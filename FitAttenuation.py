#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import argparse
import json
import numpy as np
import pandas as pd
import shutil
import sys
import warnings
warnings.filterwarnings("ignore")
from datetime import datetime
from LibGit.Attenuation import Attenuation
from LibGit.GeomSpread import GeomSpread
from LibGit.QualityFactor import QualityFactor
from LibGit.RegKappa import RegKappa
from LibGit.ConfAttFit import ConfAttFit
from LibGit.utilities import freq_range, argumentFitAttenuation,AttFittFolder,valid_true_false,FitAttenuationResDict
from LibGit.format_utils import dict_fmt

class ArgumentParserError(Exception): pass
class CustomArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ArgumentParserError(message)

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)
        
def fitattenuation(opts_str):

    synopsis = 'Return parameters to model the regional attenuation'
    usage = "example: %prog --input_file /path/to/attenuation/Attenuation.txt"
    p = argparse.ArgumentParser(description=synopsis + '\n' + usage)
    g1 = p.add_mutually_exclusive_group(required = True)
    g1.add_argument("--input", "--input_file", action="store", dest="ifile", required=False, help="TXT file containing the non parametric attenuation to be fitted")
    g1.add_argument("--config", "--configuration_file", action="store", dest="configFile",  required=False, help="configuration file in YAML format to set the non-parametric curve fitting")
    p.add_argument("--output", "--output_format", action="store", dest="output",  default="all", choices=["csv", "json", "all"], help="output file format are: 'csv' and  'json'; to save data in both formats set 'all'. Default: 'all'")
    p.add_argument("--log", "--log_file", action="store", dest="LogFile",  required=True, help="log file in TXT format.")
    p.add_argument("--v", "--verbose", action="store", dest="verb", type=valid_true_false, default='True', help="Verbose mode (prints summary and other details). Default: True")  
    opts_str_lst = opts_str.replace("'", "").replace('=',' ').split(' ')
    try: opts = p.parse_args(opts_str_lst) 
    except ArgumentParserError as ap_err: return dict_fmt(exit_status=2, exit_message=str(ap_err))    

    now = datetime.now()
    dt_string_start = now.strftime("%d/%m/%Y %H:%M:%S")
    s1 = " ---------------------- GITeps, Starting computation: {:s} ---------".format(dt_string_start)
    print(s1)
    
    message = {}
    
    plot_graph_flag = True
    save_graph_flag = True
    
    if opts.configFile != None:
        ConfigFile = ConfAttFit(opts.configFile,open(opts.LogFile,'w'),opts.verb)
        att_param_dir = ConfigFile.att_dir
        fit_att_dir = ConfigFile.out_dir
        att_file = att_param_dir + ConfigFile.att_file
        plot_graph_flag = ConfigFile.plot_graph
        save_graph_flag = ConfigFile.save_graph
        gs_model = ConfigFile.gs_model
        #f_single = ConfigFile.f_single
        f_step = ConfigFile.f_step
        freq = ConfigFile.freq
        HD = ConfigFile.hd
        HD1 = ConfigFile.hd1
        HD2 = ConfigFile.hd2
        HD3 = ConfigFile.hd3
        case = ConfigFile.case
        corr_fact_flag = ConfigFile.corr_fact_flag
        Q0_corr = ConfigFile.Q0_corr
        Vs = ConfigFile.Vs
        D1 = ConfigFile.d1
        D2 = ConfigFile.d2
        hingfreq = ConfigFile.hingfreq
        k_known = ConfigFile.k_known
        logID = ConfigFile.log
        HF1 = ConfigFile.HF1
        HF2 = ConfigFile.HF2
        Q_type = ConfigFile.Q_type
        
    if opts.ifile != None and opts.configFile != None:
        msg = " FATAL ERROR, The TXT file containing the non-parametric attenuation must be indicated in the YAML configuration file {opts.configFile}.. Aborting"
        sys.tracebacklimit = 0
        raise Exception(msg)
    if opts.configFile == None:
        if opts.verb: print("***************************************************************************")
        if opts.verb: print("......START fitting the non-parametric attenuation in interactive modality" )
        if opts.verb: print("***************************************************************************")
    elif opts.configFile != None:
        if opts.verb: print("***********************************************************************************************")
        if opts.verb: print("......START fitting the non-parametric attenuation by reading the YAML configuration file: {:s}".format(opts.configFile) )
        if opts.verb: print("***********************************************************************************************")        
    if opts.configFile == None: 
        cond_fit_att_dir = 1
        while cond_fit_att_dir == 1:
            try:
                fit_att_dir = str(input("insert folder name for fitting results: "))
                AttFittFolder(fit_att_dir)
                cond_fit_att_dir = 0
            except:
                print("ERROR!: invalid folder name for fitting results")
                cond_fit_att_dir = 1
    if opts.configFile == None: att_file = opts.ifile    
    if opts.configFile == None: logID = open(fit_att_dir + '/' + opts.LogFile,'w')
    # to read the file with the non-parametric attenuation
    msg = argumentFitAttenuation(att_file)
    s1 = "....... START reading Attenuation File: {:s} ".format(msg["file_name"])
    if opts.verb == True: print(s1)
    logID.write("\n")
    logID.write(s1)
    logID.write("\n")

    Att = Attenuation(msg["file_name"])
 
    s2 = "Attenuation file: {:s}, Nfreq: {:4d}, Ndist: {:4d}  Dref: {:5.1f} Km ".format(msg["file_name"],Att.nfreq,Att.ndist,Att.dref)
    if opts.verb == True: print(s2)
    logID.write("\n")
    logID.write(s2)
    logID.write("\n")
    AttDF = Att.getAttenuationDF() # dataframe whit non parametric attenuation obtained by GIT
    if opts.configFile == None: plot_graph_flag = True
    if opts.configFile == None: save_graph_flag = True

    val = GeomSpread(AttDF,Att.dref,Att.dist) 
    GR = val.setGRRrefSuR()
    GR_df_1_R = val.getGRRrefSuR(Att,GR['GR1']['Y']) 
    GR_df_1_R_2 = val.getGRRrefSuR2(Att,GR['GR2']['Y'])
    new_row = pd.DataFrame({'x':Att.dref, 'y':0, 'Color':'k', 'Type':'Rref/R'}, index=[0])
    df2 = pd.concat([new_row,GR_df_1_R[GR_df_1_R['x']>Att.dref]]).reset_index(drop=True)
    new_row = pd.DataFrame({'x':Att.dref, 'y':0, 'Color':'k', 'Type':'(Rref/R)^2'}, index=[0])
    df3 = pd.concat([new_row,GR_df_1_R_2[GR_df_1_R_2['x']>Att.dref]]).reset_index(drop=True)
    Att.plotAllAttenuation(df2,df3,fit_att_dir, plot_graph_flag, save_graph_flag, opts.verb)
    cond = 2
    # To define the geometrical spreading model
    while cond ==2:
        if opts.configFile == None:s3 = ".....define the geometrical speading model among Rref/R = 1, bilinear = 2, trilinear = 3 , 4-linear = 4 : "
        if opts.configFile == None:
            gs_model_flag = 1
            while gs_model_flag ==1:
                try:
                    gs_model = float(input(s3))
                    if gs_model < 1 or gs_model > 4:
                        print("*************************************")
                        print("ERROR! enter a number between 1 and 4")
                        print("*************************************")
                        gs_model_flag = 1
                    else:                     
                        gs_model_flag = 0
                except:
                    print("*************************************")
                    print("ERROR! enter a number between 1 and 4")
                    print("*************************************")
                    gs_model_flag = 1
                    
        dict_GR = {1:"Rref/R",2:"bilinear",3:"trilinear",4:"4-linear"}
        if opts.configFile != None:cond = 999
        if opts.verb: print("****************************************************")
        if opts.verb: print("Geometrical Spreading model: " + dict_GR[gs_model])
        if opts.verb: print("****************************************************")
        
        if gs_model != 1:
            s4 = "STEP 1: Fitting the Geometrical Spreading"
            logID.write("\n")
            logID.write(s4)
            logID.write("\n")
            logID.write("Geometrical Spreading model: " + dict_GR[gs_model])        
            #if opts.configFile == None:f_single = np.array([1, 3, 5, 10, 15, 20])
            if opts.configFile == None:
                s5 = "....set a frequency value (freq) to fit the geometrical spreading: "
                freq = float(input(s5))
                logID.write("\n")
                logID.write(s5)
                logID.write("\n")
            if opts.configFile == None:
                s6 = "....set the range of variability (f_step) around the frequency used to calibrate the geometrical spreading: "
                f_step = float(input(s6))
                logID.write("\n")
                logID.write(s5)
                logID.write("\n")
            #if opts.configFile == None:
            #    freq_flag = 1
            #    while freq_flag == 1:
            #        try:
            #            freq = float(input(s5))
            #            if freq in f_single:
            #                freq_flag = 0
            #            else:
            #                freq_flag = 1
            #        except:
            #            freq_flag = 1
            #if opts.configFile == None:f_step = np.array([0.2, 0.4, 1.0, 2.0, 2.0, 5.0])   
            #message.update(Att.checkFreq(freq,f_single,f_step))
            message.update({"fsingle": freq})
            message.update({"f_step": f_step})            
            f_range_index = freq_range(Att.freq,freq,message["f_step"]) # array of frequencies to be used during the geometrical spreading fitting
            message.update({"flist": str(f_range_index)})
            if opts.configFile == None:logID.write("\n")
            if opts.configFile == None:logID.write("frequency used to fit the Geometrical spreading: " + s5)
            if opts.configFile == None:logID.write("\n")
            if opts.configFile == None and gs_model !=1: s10 = 'Do you want to correct G(R) with an exponential term accounting for the anelastic attenuation at ' + str(freq) + 'Hz? YES=1 - NOT=0: '
            if opts.configFile == None:
                if gs_model !=1:
                    corr_fact_flg = 1
                    while corr_fact_flg == 1:
                        try:
                            corr_fact_flag = int(input(s10))
                            if corr_fact_flag == 1 or corr_fact_flag == 0:
                                corr_fact_flg = 0
                            else:
                                print("*************************************")
                                print("ERROR! enter 1:YES or 0:NOT")
                                print("*************************************") 
                                corr_fact_flg = 1                         
                        except:
                            print("*************************************")
                            print("ERROR! enter 1:YES or 0:NOT")
                            print("*************************************")
                            corr_fact_flg = 1                                                
                else:
                    corr_fact_flag = 0
            if corr_fact_flag == 1:
                s11 = 'G(R) corrected for the anelastic attenuation at ' + str(freq) + 'Hz'
            else:
                s11 = 'G(R) NOT corrected for the anelastic attenuation at ' + str(freq) + 'Hz'
            Anelastic_Att_dict = {}
            Anelastic_Att_dict.update({"corr_fact_flag": corr_fact_flag})
            logID.write(s11)
            logID.write("\n")

            if corr_fact_flag == 1:
                if opts.configFile == None:
                    Q0_corr_flag = 1
                    while Q0_corr_flag == 1:                    
                        try:
                            Q0_corr = float(input('Please digit a value to fix the Quality Factor at ' + str(freq) + 'Hz (Q0_corr):  '))
                            if Q0_corr > 0:                            
                                Q0_corr_flag = 0
                            else:
                                print("*************************************")
                                print("ERROR! enter a number > 0")
                                print("*************************************") 
                                Q0_corr_flag = 1                          
                        except:
                            print("*************************************")
                            print("ERROR! enter a number > 0")
                            print("*************************************")
                            Q0_corr_flag = 1                       
            else:
                Q0_corr = None

            logID.write("\n")
            logID.write("Q0_corr: " + str(Q0_corr))
            Anelastic_Att_dict.update({"Q0_corr": Q0_corr})    
            logID.write("\n")    

            if opts.configFile == None: 
                Vs_flag = 1
                while Vs_flag == 1:
                    try:
                        Vs = float(input("Please digit a value for the shear wave velocity Vs [km/s]: "))
                        if Vs <= 0:
                            print("*************************************")
                            print("ERROR! enter a number > 0")
                            print("*************************************")
                            Vs_flag = 1                        
                        else:
                            Vs_flag = 0
                    except:
                        print("*************************************")
                        print("ERROR! enter a number > 0")
                        print("*************************************")
                        Vs_flag = 1                 
            logID.write("\n")
            logID.write("Vs: " + str(Vs))
            Anelastic_Att_dict.update({"Vs": Vs}) 
            logID.write("\n")

            Att_single_f = Att.AnelasticAttDFredF(AttDF,f_range_index) # dataframe with non parametric attenuation around an User defined frequency
            if corr_fact_flag == 1:
                if type(Q0_corr) != str and Q0_corr != None and Q0_corr > 0 and type(Vs) != str and Vs != None and Vs > 0:
                    Att_single_f['y'] = Att_single_f['y'] - np.log(np.exp(-np.pi*Att_single_f['FR']*(Att_single_f['x']-Att.dref)/(Q0_corr*Vs)))
                else:
                    message = "ERROR: check the configuration file: Q0_corr and Vs must be > 0"
                    return dict_fmt(exit_status=2,exit_message=message)
            elif corr_fact_flag == 0:
                if opts.verb: print("")
            else:
                message = "ERROR: check the configuration file: corr_fact_flag must be 0 or 1"
                return dict_fmt(exit_status=2,exit_message=message)
            val = GeomSpread(Att_single_f,Att.dref,Att.dist)
            Att.plotSingleAttenuation(Att,freq,Att_single_f,fit_att_dir + '/' + "SingleAttenuation",plot_graph_flag, False, opts.verb)

        else:
            Att_single_f = AttDF
            if corr_fact_flag == 1:
                Att_single_f['y'] = Att_single_f['y'] - np.log(np.exp(-np.pi*Att_single_f['FR']*(Att_single_f['x']-Att.dref)/(Q0_corr*Vs)))
            val = GeomSpread(Att_single_f,Att.dref,Att.dist)

        if "f_step" in message: message.pop("f_step")
        if "error" in message: message.pop("error")
        if "fsingle" in message: message.pop("fsingle")
        if "flist" in message: message.pop("flist")

        Geometrical_Spreading_dict = {}

        if gs_model ==1:        

            GR = val.setGRRrefSuR()
            GR_df_1_R = val.getGRRrefSuR(Att,GR['GR1']['Y']) 
            #dataframe of the selected G(R) model
            GR_df_1_R_2 = val.getGRRrefSuR2(Att,GR['GR2']['Y'])        
            Geometrical_Spreading_dict.update({"Hinge Distance": None})
            GR_df = GR_df_1_R
            logID.write("Geometrical Spreading model: 1/R")
            logID.write("\n")
            cond = 999

        else:

            if gs_model ==2:

                if opts.configFile == None:
                    cond = 1
                    while cond == 1:
                        hd_cond = 1
                        while hd_cond == 1:
                            try:
                                HD = float(input('set the HD hinge distance [km] for the Bilinear G(R) model: '))
                                if HD > 0:
                                    hd_cond = 0
                                else:
                                    print("WARNING!!! HD must be a number > 0")
                                    hd_cond = 1
                            except Exception as e:
                                #print('Bilinear GR: ' + str(e))
                                print("WARNING!!! HD must be a number > 0")                        
                        Geometrical_Spreading_dict.update({"Hinge Distance": HD})
                        try:
                            GR = val.setGRbilinear(float(HD))
                        except Exception as e:
                            #print('Bilinear GR: ' + str(e))
                            error_message = 'ERROR!!! the hinge distance must be set between ' + str(min(Att.dist)) + ' and ' + str(max(Att.dist)) + ' km'
                            print(error_message)
                            hd_cond = 1
                            #return dict_fmt(exit_status=2, exit_message=error_message)
                        GR_df = val.getGRdf(GR,freq,'BLN_GR') #dataframe of the selected G(R) model                    
                        print("\n")
                        print("***********************************************************************")
                        print('Please check the hinge distance [km] for the Bilinear G(R) model: \n - If the HD is OK close the figure and digit: 0; \n - if you want to change the HD close the figure and digit: 1; \n - if you want to change the G(R) close the figure and digit: 2;')
                        print("***********************************************************************")
                        print("\n")  

                        GR_df['X'] = np.log10(GR_df['x']/Att.dref)
                        Att_single_f['X'] = np.log10(Att_single_f['x']/Att.dref)

                        val.plotGRmodel(GR_df,GR,Att,Att_single_f,freq,fit_att_dir + '/' + 'GeometricalSpreading', plot_graph_flag, save_graph_flag, opts)
                        #cond = int(input('make your choice: '))
                        cond2 = 1
                        while cond2 == 1:
                            try:
                                cond = int(input('make your choice: '))
                                if cond < 0 or cond > 2:
                                    print('WARNING!!! please digit 0 to go on, 1 to change the HDs, 2 to change the G(R) model')
                                    cond2 = 1
                                else:
                                    cond2 = 0
                            except:
                                print('WARNING!!! please digit 0 to go on, 1 to change the HDs, 2 to change the G(R) model')
                                cond2 = 1
                                
                else:
                    Geometrical_Spreading_dict.update({"Hinge Distance": HD})
                    try:
                        GR = val.setGRbilinear(float(HD))
                    except Exception as e:
                        print('Bilinear GR: ' + str(e))
                        error_message = 'ERROR!!! the hinge distance must be set between ' + str(min(Att.dist)) + ' and ' + str(max(Att.dist)) + ' km'
                        return dict_fmt(exit_status=2, exit_message=error_message)
                    GR_df = val.getGRdf(GR,freq,'BLN_GR') #dataframe of the selected G(R) model 
                    logID.write("\n")
                    logID.write("Hinge Distance: " + str(HD))
                    logID.write("\n")
                    val.plotGRmodel(GR_df,GR,Att,Att_single_f,freq,fit_att_dir + '/' + 'GeometricalSpreading', plot_graph_flag, save_graph_flag, opts)

                logID.write('Rref: ' + str(GR['coeff']["Rref"]))
                logID.write("\n")
                logID.write('n1: ' + "{0:.2f}".format(GR['coeff']["n1"]))
                logID.write("\n")           
                logID.write('n2: ' + "{0:.2f}".format(GR['coeff']["n2"]))
                logID.write("\n")

                if opts.verb:print("\n")
                if opts.verb:print('Rref: ' + str(GR['coeff']["Rref"]))
                if opts.verb:print('n1: ' + "{0:.2f}".format(GR['coeff']["n1"]))
                if opts.verb:print('n2: ' + "{0:.2f}".format(GR['coeff']["n2"]))

            elif gs_model ==3:
                if opts.configFile == None:
                    cond = 1
                    while cond == 1: 
                        hd1_cond = 1
                        while hd1_cond == 1:
                            try:
                                HD1 = float(input('set the HD1 hinge distance [km] for the Trilinear G(R) model: '))
                                if HD1 <=0:
                                    warning_message = "WARNING!!! HD1 must be a number > 0"
                                else:
                                    sys.exit()
                                hd1_cond = 0
                            except Exception as e:
                                print('Trilinear GR: ' + str(e))
                                warning_message = "WARNING!!! HD1 must be a number > 0"
                                print(warning_message)        
                        Geometrical_Spreading_dict.update({"h1": HD1})
                        hd2_cond = 1
                        while hd2_cond == 1:                            
                            try:
                                HD2 = float(input('set the HD2 hinge distance [km] for the Trilinear G(R) model: '))
                                hd2_cond = 0
                            except Exception as e:
                                print('Trilinear GR: ' + str(e))
                                warning_message = "WARNING!!! HD2 must be a number"
                                print(warning_message)                     
                        Geometrical_Spreading_dict.update({"h2": HD2})          
                        try:
                            GR = val.setGRtrilinear(float(HD1),float(HD2))                            
                        except Exception as e:                             
                            print('Trilinear GR: ' + str(e))
                            error_message = 'ERROR!!! the hinge distances must be set between ' + str(min(Att.dist)) + ' and ' + str(max(Att.dist)) + ' km with HD1 < HD2'
                            return dict_fmt(exit_status=2, exit_message=error_message)
                        GR_df = val.getGRdf(GR,freq,'TLN_GR') #dataframe of the selected G(R) model
                        print("\n")
                        print("********************************************************************")
                        print('Please check the hinge distances [km] for the Trilinear G(R) model: \n - if the HDs are OK close the figure and digit: 0; \n - if you want to keep the G(R) model but change the HDs close the figure and digit: 1; \n - if you want to change the G(R) close the figure and digit: 2')
                        print("********************************************************************")
                        print("\n")                     
                        val.plotGRmodel(GR_df,GR,Att,Att_single_f,freq,fit_att_dir + '/' + 'GeometricalSpreading', True, True, opts)
                        cond = int(input('make your choice: '))
                        cond2 = 1
                        while cond2 == 1:
                            if cond > 2 or cond <0:
                                print('WARNING!!! please digit 0 to go on, 1 to change the HDs, 2 to change the G(R) model')
                                cond = int(input('make your choice: '))
                            else:
                                cond2 = 0
                else:
                    Geometrical_Spreading_dict.update({"1st Hinge Distance": HD1,"2nd Hinge Distance": HD2})
                    try:
                        GR = val.setGRtrilinear(float(HD1),float(HD2))
                    except Exception as e:
                        print('Trilinear GR: ' + str(e))
                        error_message = 'ERROR!!! the hinge distances must be set between ' + str(min(Att.dist)) + ' and ' + str(max(Att.dist)) + ' km with HD1 < HD2'
                        return dict_fmt(exit_status=2, exit_message=error_message)
                    
                    GR_df = val.getGRdf(GR,freq,'TLN_GR') #dataframe of the selected G(R) model 
                    val.plotGRmodel(GR_df,GR,Att,Att_single_f,freq,fit_att_dir + '/' + 'GeometricalSpreading', plot_graph_flag, save_graph_flag, opts)

                logID.write("\n")
                logID.write("h1: " + str(HD1))
                logID.write("\n")
                logID.write("h2: " + str(HD2))
                logID.write("\n")
                logID.write('Rref: ' + str(GR['coeff']["Rref"]))
                logID.write("\n")
                logID.write('n1: ' + "{0:.2f}".format(GR['coeff']["n1"]))
                logID.write("\n")
                logID.write('n2: ' + "{0:.2f}".format(GR['coeff']["n2"]))
                logID.write("\n")
                logID.write('n3: ' + "{0:.2f}".format(GR['coeff']["n3"]))            
                logID.write("\n")  

                if opts.verb:print('Rref: ' + str(GR['coeff']["Rref"]))
                if opts.verb:print('n1: ' + "{0:.2f}".format(GR['coeff']["n1"]))
                if opts.verb:print('n2: ' + "{0:.2f}".format(GR['coeff']["n2"]))
                if opts.verb:print('n3: ' + "{0:.2f}".format(GR['coeff']["n3"]))

            elif gs_model == 4:
                if opts.configFile == None:
                    cond = 1
                    while cond == 1: 
                        hd1_cond = 1
                        while hd1_cond == 1:
                            try:
                                HD1 = float(input('set the HD1 hinge distance [km] for the 4-linear G(R) model: '))
                                hd1_cond = 0
                            except Exception as e:
                                print('4-linear GR: ' + str(e))
                                warning_message = "WARNING!!! HD1 must be a number"
                                print(warning_message)
                        Geometrical_Spreading_dict.update({"h1": HD1})
                        hd2_cond = 1
                        while hd2_cond == 1:
                            try:
                                HD2 = float(input('set the HD2 hinge distance [km] for the 4-linear G(R) model: '))
                                hd2_cond = 0
                            except Exception as e:
                                print('4-linear GR: ' + str(e))
                                warning_message = "WARNING!!! HD2 must be a number"
                                print(warning_message)                        
                        Geometrical_Spreading_dict.update({"h2": HD2})
                        hd3_cond = 1
                        while hd3_cond == 1:
                            try:
                                HD3 = float(input('set the HD3 hinge distance [km] for the 4-linear G(R) model: '))
                                hd3_cond = 0
                            except Exception as e:
                                print('4-linear GR: ' + str(e))
                                warning_message = "WARNING!!! HD3 must be a number"
                                print(warning_message)                           
                        Geometrical_Spreading_dict.update({"h3": HD3})
                        try:
                            GR = val.setGR4linear(float(HD1),float(HD2),float(HD3))
                        except Exception as e:
                            print('4-linear GR: ' + str(e))
                            error_message = 'ERROR!!! the hinge distances must be set between ' + str(min(Att.dist)) + ' and ' + str(max(Att.dist)) + ' km with HD1 < HD2 and HD2 < HD3'
                            return dict_fmt(exit_status=2, exit_message=error_message)
                        GR_df = val.getGRdf(GR,freq,'4LN_GR') #dataframe of the selected G(R) model
                        print("\n")
                        print('Rref: ' + str(GR['coeff']['Rref']) + ' Km')

                        print("********************************************************************")
                        print('Please check the hinge distances [km] for the Trilinear G(R) model: \n - if the HDs are OK close the figure and digit: 0; \n - if you want to keep the G(R) model but change the HDs close the figure and digit: 1; \n 3) - if you want to change the G(R) model close the figure and digit: 2')
                        print("********************************************************************")
                        print("\n")                     
                        val.plotGRmodel(GR_df,GR,Att,Att_single_f,freq,fit_att_dir + '/' + 'GeometricalSpreading', True, True, opts)
                        cond = int(input('make your choice: '))
                        cond2 = 1
                        while cond2 == 1:
                            if cond > 2 or cond <0:
                                print('WARNING!!! invalid choice, pay attention to the option list... only, 0,1,2 are admissible ')
                                cond = int(input('make your choice: '))
                            else:
                                cond2 = 0
                else:
                    try:
                        GR = val.setGR4linear(float(HD1),float(HD2),float(HD3))
                    except Exception as e:
                        print('4-linear GR: ' + str(e))
                        error_message = 'ERROR!!! the hinge distances must be set between ' + str(min(Att.dist)) + ' and ' + str(max(Att.dist)) + ' Km with HD1 < HD2 and HD2 < HD3'
                        return dict_fmt(exit_status=2, exit_message=error_message)
                    GR_df = val.getGRdf(GR,freq,'4LN_GR') #dataframe of the selected G(R) model
                    val.plotGRmodel(GR_df,GR,Att,Att_single_f,freq,fit_att_dir + '/' + 'GeometricalSpreading', plot_graph_flag, save_graph_flag, opts)

                logID.write("\n")                
                logID.write("h1: " + str(HD1))
                logID.write("\n")
                logID.write("h2: " + str(HD2))
                logID.write("\n")
                logID.write("h3: " + str(HD3))
                logID.write("\n")            
                logID.write('Rref: ' + str(GR['coeff']["Rref"]))
                logID.write("\n")
                logID.write('n1: ' + "{0:.2f}".format(GR['coeff']["n1"])) 
                logID.write("\n")
                logID.write('n2: ' + "{0:.2f}".format(GR['coeff']["n2"]))
                logID.write("\n")
                logID.write('n3: ' + "{0:.2f}".format(GR['coeff']["n3"]))
                logID.write("\n")
                logID.write('n4: ' + "{0:.2f}".format(GR['coeff']["n4"]))
                logID.write("\n")            
                if opts.verb:print("\n")
                if opts.verb:print('Rref: ' + str(GR['coeff']["Rref"]))
                if opts.verb:print('n1: ' + "{0:.2f}".format(GR['coeff']["n1"]))
                if opts.verb:print('n2: ' + "{0:.2f}".format(GR['coeff']["n2"]))
                if opts.verb:print('n3: ' + "{0:.2f}".format(GR['coeff']["n3"]))
                if opts.verb:print('n4: ' + "{0:.2f}".format(GR['coeff']["n4"]))
                if opts.verb:print("\n")            
                if opts.configFile != None: val.plotGRmodel(GR_df,GR,Att,Att_single_f,freq,fit_att_dir + '/' + 'GeometricalSpreading', plot_graph_flag, save_graph_flag, opts) 

    # linear fitting of the Anelastic attenuation term        
    s9 = "STEP 2: Fitting the Anelastic Attenuation term"
    
    logID.write("\n")
    logID.write(s9)
    logID.write("\n")
    logID.write("\n")
    if opts.verb: print("****************")
    if opts.verb: print(s9)
    if opts.verb: print("****************")      
    
    Geometrical_Spreading_dict.update({"model":dict_GR[gs_model]})
    
    dict_case = {1: "fit by considering a  '" +  dict_GR[gs_model]   + "' term for the Geometrical Spreading multiplied by an exponential term accounting for the anelastic attenuation", 2: "fit by considering a  '" +  dict_GR[gs_model]   + "' term for the Geometrical Spreading multiplied by an exponential term accounting for the anelastic attenuation and an exponential term accounting for kappa",3: "fit by considering a  '" +  dict_GR[gs_model]   + "' term for the Geometrical Spreading multiplied by an exponential term accounting for the anelastic attenuation and an exponential term accounting for known kappa"}
    
    if opts.configFile == None: 
        if opts.verb: print(str(1) + " : " + dict_case[1]) 
        if opts.verb: print(str(2) + " : " + dict_case[2])
        if opts.verb: print(str(3) + " : " + dict_case[3])                 
        cond2 = 1
        while cond2 == 1:
            try:
                case = int(input('Please...digit 1, 2, or 3: '))                
                if case > 3 or case <=0:
                    print('WARNING!!! pay attention to the options list, the only admissible values are 1,2,3...')
                    case = int(input('make your choice: '))
                else:
                    cond2 = 0
            except:
                print('WARNING!!! pay attention to the options list, the only admissible values are 1,2,3...')                
                cond2 = 1    
    logID.write(dict_case[case])
    logID.write("\n")        
 
    AnelasticAttDF = Att.getAnelasticAttenuation(AttDF,GR_df,0,Att.dref,None,Vs)
                
    if opts.configFile == None:
        cond = 1
        while cond == 1:
            cond_d1 =1
            while cond_d1 ==1:
                try:
                    D1 = float(input('minimum distance in km to fit the Q parameter (dist_min_Q): '))
                    if D1 < 0: 
                        print("ERROR!: enter a number > 0")
                        cond_d1 = 1
                    else:
                        cond_d1 = 0
                except:
                    cond_d1 = 1
                    print("ERROR!: enter a number > 0")
            cond_d2 =1
            while cond_d2 ==1:
                try:
                    D2 = float(input('maximum distance in km to fit the Q parameter (dist_max_Q): '))
                    if D2 < 0: 
                        print("ERROR!: enter a number > 0")
                        cond_d2 = 1
                        print("ERROR!: enter a number > 0")
                    else:
                        cond_d2 = 0
                except:
                    cond_d2 = 1
            
                      
            AnelasticAttDF_red_D = Att.AnelasticAttDFredD(AnelasticAttDF,D1,D2)
            quality_factor = QualityFactor(D1,D2,Vs,Att.dist)
            check = quality_factor.checkQF()
           
            if len(check)==0:
                pass
            else:
                return dict_fmt(exit_status=2, exit_message=check[0])
            print("\n")
            print("*********************************")
            print('Check the distance range to fit the Q parameter:\n - if you want to change the distance range close the figure and digit: 1; \n - if the distance range is OK close the figure and digit: 0')
            print("*********************************")
            print("\n") 
            try:
                Att.plotAnelasticAttenuation(Att,fit_att_dir + '/' + 'AnelasticAttenuationRedD',AnelasticAttDF_red_D,plot_graph_flag,False, opts)
            except Exception as e:
                print('Corrected Attenuation: ' + str(e))
                error_message = 'ERROR!!! pay attention to properly set the distance range to fit the quality factor'
                return dict_fmt(exit_status=2, exit_message=error_message)                
            cnd = 99
            while cnd == 99:
                cond = int(input('make your choice: '))            
                if cond == 1 or cond ==0:                    
                    cnd = 0
                else:
                    print("ERROR!: enter 0 or 1")
                    cond = int(input('make your choice: '))

    quality_factor = QualityFactor(D1,D2,Vs,Att.dist)
    check = quality_factor.checkQF()
    if len(check)==0:
        pass
    else:
        return dict_fmt(exit_status=2, exit_message=check[0])
    AnelasticAttDF_red_D = Att.AnelasticAttDFredD(AnelasticAttDF,D1,D2)

    if opts.configFile != None:Att.plotAnelasticAttenuation(Att,fit_att_dir + '/' + 'AnelasticAttenuationRedD',AnelasticAttDF_red_D,plot_graph_flag,False, opts)            
    logID.write("\n")

        
    Anelastic_Att_dict.update({"dist_min_Q": D1})       
    Anelastic_Att_dict.update({"dist_max_Q": D2})    
    logID.write("dist_min_Q: " + str(D1) + "\n") 
    logID.write("dist_max_Q: " + str(D2) + "\n")
    logID.write("\n")    

    dict_case = {}
    index = 0
    for freq in Att.freq:
        df = Att.AnelasticAttDFsingleF(AnelasticAttDF_red_D,freq)
        dist = df['x'].to_numpy()        
                  
        if case == 1:
            amp = df['AminusGR'].to_numpy()
            dict_case.update(quality_factor.Case1(dist,Att.dref,amp,freq,index,Vs))
            s12 = "Q(" + str(dict_case[index]['Frequency [Hz]']) + " [Hz]) = {:5.1f}".format(dict_case[index]['Q(f)']) + " +/- " + "{:5.3f}".format(dict_case[index]['yerr']) + "\n"
            if opts.verb: print(s12)
            logID.write(s12)
        if case ==2:
            if opts.configFile == None and index == 0: hingfreq = float(input("Please digit a hinge frequency [Hz] to fit the kappa value (hingfreq): "))
            if opts.configFile == None and index == 0: logID.write("Hinge frequency: " + str(hingfreq))
            if opts.configFile == None and index == 0: logID.write("\n")
            kappa = RegKappa(Att.freq,freq,hingfreq)
            amp = df['AminusGR'].to_numpy()
            dict_case.update(kappa.Case2(dist - Att.dref,amp,Vs,index))
            df = kappa.AttCorrGR_k_DF(df,dict_case[index]['kappa'])
            amp = df['AcorrGR_k'].to_numpy()
            dict_case.update(kappa.Case3(dist - Att.dref,amp,Vs,index,dict_case[index]['kappa'])) 
            s13 = "Q(" + str(dict_case[index]['Frequency [Hz]']) + " [Hz]) = {:5.1f}".format(dict_case[index]['Q(f)']) + " +/- " + "{:5.3f}".format(dict_case[index]['yerr'])
            s14 = "kappa(" + str(dict_case[index]['Frequency [Hz]']) + " [Hz]) = {:6.3f}".format(dict_case[index]['kappa'])
            if opts.verb: print (s13 + "  " + s14)
            logID.write(s13 + "  " + s14 + "\n")
        if case ==3:
            if opts.configFile == None and index == 0: 
                cond_hingfreq = 1
                while cond_hingfreq == 1:
                    try:
                        hingfreq = float(input("Please digit a hinge frequency [Hz] to correct for the kappa value (hingfreq): "))
                        if hingfreq > 0:
                            cond_hingfreq = 0
                        else:
                            print("ERROR!: enter a number > 0")
                            cond_hingfreq = 1
                    except:
                        cond_hingfreq == 1
            if opts.configFile == None and index == 0: logID.write("Hinge frequency: " + str(hingfreq))
            if opts.configFile == None and index == 0: logID.write("\n")
            kappa = RegKappa(Att.freq,freq,hingfreq)
            if opts.configFile == None and index == 0: 
                cond_k_known = 1
                while cond_k_known == 1:
                    try:
                        k_known = float(input("Please digit a value for the kappa [s] operator (k_known): "))
                        if k_known > 0:
                            cond_k_known = 0
                        else:
                            print("ERROR!: enter a number > 0")
                            cond_k_known = 1
                    except:
                        cond_k_known == 1                                
            if opts.configFile == None and index == 0: logID.write("Kappa value: " + str(k_known))
            if opts.configFile == None and index == 0: logID.write("\n")
            try:
                df = kappa.AttCorrGR_k_DF(df,k_known)
            except Exception as e:
                message = "ERROR! check the configuration file: hingfreq and k_known > 0"
                return dict_fmt(exit_status=2, exit_message=message)
            amp = df['AcorrGR_k'].to_numpy()
            dict_case.update(kappa.Case3(dist - Att.dref,amp,Vs,index,k_known)) 
            s13 = "Q(" + str(dict_case[index]['Frequency [Hz]']) + " [Hz]) = {:5.1f}".format(dict_case[index]['Q(f)'])
            s14 = "kappa(" + str(dict_case[index]['Frequency [Hz]']) + " [Hz]) = {:6.3f}".format(dict_case[index]['kappa'])
            if opts.verb: print (s13 + "  " + s14)
            logID.write(s13 + "  " + s14 + "\n")            
        index = index +1
    
    d1 = pd.DataFrame(dict_case)
    d1 = d1.transpose()
    
    d1 = d1.loc[d1['Q(f)']>0] # to avoid negative values
    d1["Model"] = 'Q_linear'
    d1["Type"] = 'solid'    
    if opts.verb: print()  
    if opts.verb: print('Now perform linear regression over Q(f) vs. log(f) to obtain Q0 and N ...')
    if opts.verb: print()
    dictQf = quality_factor.fitCase1(d1)
    if opts.verb: print("Q0 = {:4.0f}".format(dictQf["Q0"]) + "   " + "N = {:4.2f}".format(dictQf["N"]))
    logID.write("\n")
    logID.write("Q0 = {:4.0f}".format(dictQf["Q0"]) + "   " + "N = {:4.2f}".format(dictQf["N"]) + "\n")

    # plot Q(f)
    quality_factor.plotQf(d1,None,dictQf["Q0"],dictQf["N"],dictQf['err_Q0'],dictQf['err_N'],None,None,None,None,None,None,fit_att_dir + '/' + 'QualityFactor',plot_graph_flag,save_graph_flag,opts.verb)
    
    if opts.configFile == None:
        cond_Q_type = 1
        while cond_Q_type == 1:
            try:
                Q_type = float(input("Do you want apply a tri-linear fitting for Q(f)? \n - to go on: 0; \n - to fit Q(f) with a tri-linear trend: 1 \n make your choice: "))
                cond_Q_type = 0
            except:
                print("ERROR!: enter 0 or 1")
                cond_Q_type = 1
    if Q_type == 1:
        cond = 1
        while cond == 1:
            #double hinge frequency for Q(f)
            if opts.configFile == None: 
                try:
                    HF1 = float(input("First Hinge Frequency (HF1) for Q(f): "))
                    if HF1 >0:
                        pass
                    else:
                        print('ERROR! HF1 must be a number > 0')
                        sys.exit()
                except Exception as e:
                    print(str(e))
            logID.write("\n")
            logID.write("First Hinge Frequency for Q(f): " + str(HF1))
            if opts.configFile == None:
                try:
                    HF2 = float(input("Second Hinge Frequency (HF2) for Q(f): "))
                    if HF2 > 0:
                        pass
                    else:
                        print('ERROR! HF2 must be a number > 0')
                        sys.exit()               
                except Exception as e:
                    print(str(e))
            logID.write("\n")
            logID.write("Second Hinge Frequency for Q(f): " + str(HF2))  
            if HF1 != None:
                dfQF2 = d1.loc[d1['Frequency [Hz]'] >=HF1]
            else:
                message = "ERROR! check the configuration file: HF1 > 0"
                return dict_fmt(exit_status=2,exit_message=message)
            if HF2 != None:
                dfQF2 = dfQF2.loc[d1['Frequency [Hz]'] <=HF2]
            else:
                message = "ERROR! check the configuration file: HF2 > 0"
                return dict_fmt(exit_status=2,exit_message=message)
            dfQF2["Model"] = 'Q2_trilinear'
            dfQF2["Type"] = 'solid'
            try:
                dict_Qf2 = quality_factor.fitCase1(dfQF2)
            except:
                message = "ERROR! check the configuration file: HF1 and HF2 must be within the range [" + str(min(Att.freq)) + "Hz and " + str(max(Att.freq)) + "Hz] and HF1 < HF2"
                return dict_fmt(exit_status=2,exit_message=message)
            dict_Qf2['Q02'] = dict_Qf2.pop('Q0')
            dict_Qf2['err_Q02'] = dict_Qf2.pop('err_Q0')       
            dict_Qf2['N2'] = dict_Qf2.pop('N')                
            dict_Qf2['err_N2'] = dict_Qf2.pop('err_N')
            
            dfQF1 = d1.loc[d1['Frequency [Hz]'] <=float(HF1)] 
            dfQF1["Model"] = 'Q1_trilinear'
            dfQF1["Type"] = 'bla'
            dict_Qf1 = quality_factor.fitCase1(dfQF1)
            dict_Qf1['Q01'] = dict_Qf1.pop('Q0')
            dict_Qf1['err_Q01'] = dict_Qf1.pop('err_Q0')       
            dict_Qf1['N1'] = dict_Qf1.pop('N')                
            dict_Qf1['err_N1'] = dict_Qf1.pop('err_N')
                    
            dfQF3 = d1.loc[d1['Frequency [Hz]'] >=float(HF2)]    
            dict_Qf3 = quality_factor.fitCase1(dfQF3)    
            dfQF3["Model"] = 'Q3_trilinear'
            dfQF3["Type"] = 'solid'    
            dict_Qf3['Q03'] = dict_Qf3.pop('Q0')
            dict_Qf3['err_Q03'] = dict_Qf3.pop('err_Q0')       
            dict_Qf3['N3'] = dict_Qf3.pop('N')                
            dict_Qf3['err_N3'] = dict_Qf3.pop('err_N')
                
            dfQF1.loc[:,'fit'] = dict_Qf1['Q01']*pow(dfQF1['Frequency [Hz]'],dict_Qf1['N1'])
            dfQF2.loc[:,'fit'] = dict_Qf2['Q02']*pow(dfQF2['Frequency [Hz]'],dict_Qf2['N2'])
            dfQF3.loc[:,'fit'] = dict_Qf3['Q03']*pow(dfQF3['Frequency [Hz]'],dict_Qf3['N3'])
            dfQF_ = pd.concat([dfQF2,dfQF3])
            dfQF = pd.concat([dfQF1,dfQF_])
            if opts.configFile == None:print("\n")            
            if opts.configFile == None:print("********************************************************************")
            if opts.configFile == None:print('Please check the hinge frequencies [Hz] for the Trilinear Q(f) model: \n - if you want to change the HFs close the figure and digit: 1; \n - if the HFs is OK close the figure and digit: 0')
            print("********************************************************************")
            print("\n")                     
            
            quality_factor.plotQf(d1,dfQF,dictQf["Q0"],dictQf["N"],dictQf['err_Q0'],dictQf['err_N'],dict_Qf1["Q01"],dict_Qf1["N1"],dict_Qf2["Q02"],dict_Qf2["N2"],dict_Qf3["Q03"],dict_Qf3["N3"],fit_att_dir + '/' + 'QualityFactor',plot_graph_flag,save_graph_flag,opts.verb)
            
            if opts.configFile == None:
                try:
                    cond = int(input('make your choice: '))
                except:
                    cond = int(0)
            if opts.configFile != None:cond = int(0)
    elif Q_type == 0:
        if opts.verb: print("")
    else:
        message = "ERROR: check the configuration file: Q_type must be 0 or 1"
        return dict_fmt(exit_status=2,exit_message=message)
    try:
        d2 = d1.loc[d1["Frequency [Hz]"]>= hingfreq]
        kappa_vec = d2["kappa"].to_numpy()
        dictKappa = {"kappa_mean": np.mean(kappa_vec), "kappa_std": np.std(kappa_vec),"HFkappa" : hingfreq}
        s15 = "kappa = {:6.3f}".format(dictKappa["kappa_mean"]) + ' ±' + "{:6.3f}".format(dictKappa["kappa_std"]) + " [s]"
        if opts.verb: print(s15)
        logID.write(s15)
    except Exception as e:
        dictKappa = {"kappa_mean": 0, "kappa_std": 0,"HFkappa" : 0}

    dictionary = FitAttenuationResDict()
    message.update({"Anelastic Attenuation": dictQf, 'Geometrical Spreading':GR['coeff']})
    if Q_type !=0: message["Anelastic Attenuation"].update(dict_Qf1)
    if Q_type !=0: message["Anelastic Attenuation"].update(dict_Qf2)
    if Q_type !=0: message["Anelastic Attenuation"].update(dict_Qf3)
    if Q_type !=0: message["Anelastic Attenuation"].update({"HF1": HF1,"HF2": HF2 })
    message["Anelastic Attenuation"].update(dictKappa)
    message["Geometrical Spreading"].update(Geometrical_Spreading_dict)
    message["Anelastic Attenuation"].update(Anelastic_Att_dict)
    message["Dictionary"] = dictionary
    if opts.output == 'json' or opts.output == 'all':
        with open(fit_att_dir + '/out.json', 'w') as fp:
            json.dump(message, fp, indent=4,sort_keys=False, cls=NpEncoder)    
    
    out_json = json.dumps(message, cls=NpEncoder) 
    pd_object = pd.read_json(out_json, typ='series')      

    if opts.verb:print('---------------------------')
    if opts.verb: print("Anelastic Attenuation")
    if opts.verb:print('---------------------------')
    
    for key in message["Anelastic Attenuation"].keys():
        if opts.verb:print(key, ":", message["Anelastic Attenuation"][key])

    if opts.output == 'csv' or opts.output == 'all':
        with open(fit_att_dir + '/Anelastic_Attenuation.csv', 'w') as fp:
            for key in message["Anelastic Attenuation"].keys():
            
                fp.write(key + " : " + str(message["Anelastic Attenuation"][key]) + '\n')
                                
    if opts.verb:print('---------------------------')

    if opts.verb: print('---------------------------')
    if opts.verb:print("Geometrical Spreading")
    if opts.verb:print('---------------------------')
    for key in message["Geometrical Spreading"].keys():
        if opts.verb:print(key, ":", message["Geometrical Spreading"][key])

    if opts.output == 'csv' or opts.output == 'all':
        with open(fit_att_dir + '/Geometrical_Spreading.csv', 'w') as fp:
            for key in message["Geometrical Spreading"].keys():
            
                fp.write(key + " : " + str(message["Geometrical Spreading"][key]) + '\n')
                
    if opts.verb:print('---------------------------')
    
    if opts.output == 'csv' or opts.output == 'all':
        with open(fit_att_dir + '/dictionary.csv', 'w') as fp:
            for key in message["Dictionary"].keys():
            
                fp.write(key + " : " + str(message["Dictionary"][key]) + '\n')
    logID.close()
    if opts.configFile != None: shutil.move(opts.LogFile, fit_att_dir + '/' + opts.LogFile)
    if opts.configFile != None: shutil.copyfile(opts.configFile, fit_att_dir + '/' + os.path.basename(opts.configFile))
    dt_string_end = now.strftime("%d/%m/%Y %H:%M:%S")
    s1 = " ---------------------- FitAttenuation, End computation: {:s} ---------".format(dt_string_end)
    print('')
    print(s1)
    return dict_fmt(exit_status=0, exit_message='',out_string=out_json, out_object=json.loads(out_json), out_format='json')            

def main():
    
    ret_code = fitattenuation(' '.join(sys.argv[1:]))
    
    if ret_code['exit_status'] == 0 and ret_code['out_string'] : print('DONE!!') #print(ret_code['out_string'])
    if ret_code['exit_message']: print(ret_code['exit_message'])
    sys.exit(ret_code['exit_status'])


if __name__ == "__main__": main()
