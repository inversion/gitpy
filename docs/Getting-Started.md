# **Getting Started**

GITpy is structured in 4 main modules ([Table 2](\#tab\_module\_sum)) contained in the code folder named “gitpy-main” and a python library named LibGit ([Table 3](\#tab\_lib\_sum)). 

| Python modules | Description |
| :---- | :---- |
| **GITeps.py** | Separates the source, propagation and site contributions from the Fourier Amplitude Spectra (FAS) of the recorded S-waves; the equation system is solved without imposing any a-priori parametric models on the different terms. |
| **CorrectFas.py** | Corrects the recorded spectra (FAS) for the attenuation and site effects, using the GIT spectral decomposition outputs, providing the apparent source spectra. |
| **FitAttenuation.py** | Performs fitting of non-parametric spectral attenuation curves using standard seismological models in which the geometrical spreading and the anelastic attenuation contributions are isolated and described in terms of parametric spectral models depending on distance, quality factor, and kappa. |
| **FitSource.py** | Performs fitting of source spectra in terms of seismic moment and corner frequency, and allows to derive other seismological parameters (e.g. stress drop, radiated energy). |

[Table 2](\#table\_module\_sum) Module summary

| Python modules | Description |
| :---- | :---- |
| **Attenuation.py** | Python class to manage input/output related to the attenuation |
| **AttParametric.py** | Python class to set the spectral model to fit the non-parametric attenuation curves |
| **Brune.py** | Python class to calculate source spectra using the 𝜔2 Brune’s models |
| **ConfGITeps.py** | Python class to read the configuration file with parameters and paths to optional input files required to run the seismological data inversion (GITeps.py) |
| **ConfAttFit.py** | Python class to read the configuration file with parameters and paths to optional input files required to run the fitting of non-parametric spectral attenuation curves (FitAttenuation.py) |
| **ConfCorrectFas.py** | Python class to read the configuration file with paths to GIT spectral decomposition outputs required to run the apparent source spectra (CorrectFas.py) |
| **ConfSourceFit.py** | Python class to read the configuration file with parameters and paths to optional input files required to run the fitting of the source term (FitSource.py) |
| **EvtGit.py** | Python class to read/write event metadata related to source spectra |
| **FASInv.py** | Python class to to separates source, propagation, and site contributions from the Fourier Amplitude Spectra (FAS) of recorded S-waves; the equation system is solved without imposing any a-priori parametric models on the different terms |
| **format\_utils.py** | Utility to manage the GITpy outputs |
| **GeomSpread.py** | Python class to fit the geometrical spreading |
| **GitResults.py** | Python class to manage outputs of the seismological data inversion |
| **InputFas.py** | Python class to read the FAS  |
| **QualityFactor.py** | Python class to parametrize the frequency-dependent quality factor model |
| **RegKappa.py** | Python class to parametrize the exponential terms with kappa related to the anelastic attenuation model |
| **Source.py** | Python class to fit the non-parametric source spectra |
| **Sources.py** | Python class to manage input/output related to source spectra |
| **StatGit.py** | Python class to read/write station metadata |
| **Stations.py** | Python class to manage the site term |
| **utilities.py** | GITpy utilities  |

[Table 3](\#table\_lib\_sum) GITpy Library

![Figure 6a](images/Figure6a.jpg) 
![Figure 6b](images/Figure6b.jpg)
![Figure 6c](images/Figure6c.jpg)
![Figure 6d](images/Figure6d.jpg)

[Figure 6](images/Figure6a.jpg) Workflows of the four main python modules of GITpy: a) GITeps.py; b) CorrectFas.py; c) FitAttenuation.py; d) FitSource.py.

## **Configuration files format**

The User generally interacts with the main GITpy modules (GITeps.py, FitAttenuation.py, FitSource.py and CorrectFas.py) by editing the configuration files. The structure of these files is based on the YAML format and contains all the configuration settings that are needed to run the different modules and optional input files (Figure 8). The user must not change the structure of these files or the names of the key fields in the first column, only the values in the second column can be modified.  
The following sections describe each configuration file to help the user set the correct values for each field.

### **Configuration files for module GITeps.py**

The main fields of the YAML configuration file are subdivided into blocks: GLOBAL contains the general settings; DIST BIN includes the information about the distance discretization; SUBSET provides information in case the User needs to remove seismic events and stations from the original input dataset; CONSTRAINT DREF sets the reference distance characteristics; CONSTRAINTS SITES comprises details on reference sites constraints; CONSTRAINT SMOOTH DIST describes the smoothing condition on attenuation functions; LSQR INVERSION contains inversion related parameters. Detailed descriptions are given in [Table 4](\#tab\_conf\_giteps) and an example in [Figure 7](images/Figure7.jpg). The YAML configuration file includes four fields (dist\_file, event\_remove, and station\_remove) that require a text file name. In [Figure 8](images/Figure8.jpg) an example of the structure of each text file is shown.

Note that the options included in SUBSET should be handled with care by the Users, because they were conceived only to remove possible outliers, not to produce different working datasets from the original one. The compilation of FAS dataset useful for the application of the GIT is a crucial task that cannot be delegated to the inversion module. 

| Block | Field | Description |
| ----- | ----- | ----- |
| GLOBAL | job\_name | The User can associate an arbitrary name to his analysis |
|  | fas\_dir | Path to Directory containing the Fourier Amplitude Spectra (FAS) files |
|  | out\_dir | Output directory |
|  | gui\_env | List of interactive backend that can be used for displaying Matplotlib figures. Only \['Qt5Agg','TkAgg','nbAgg','WebAgg'\] have been tested in GITpy. To run GITeps.py in Jupyter Notebook set gui\_env: Na |
|  | cmp | Component used for the inversion : "H": horizontal components ; "Z": Vertical component |
|  | plot\_graph | "True": shows figures during the code execution; "False": avoids the figures visualization |
|  | save\_graph | "True" saves figures in the output directory; "False" does not save figures |
|  | mean | Option "VECTORIAL" calculates the vectorial component on the two H components (sqrt(EW2 \+ NS2)) ; option "GEOM" calculates the geometrical mean of the two H components |
|  | fmin | Min frequency to consider for the analysis |
|  | fmax | Max frequency to consider for the analysis |
|  | mmin | Min magnitude to consider for the analysis |
|  | mmax | Max magnitude to consider for the analysis |
| DIST BIN | type\_bin | Option "LIN" : Creates a distance vector composed by a linear distribution of values between "dist\_min" and "dist\_max" with a step as given in the variable "dist\_step";Option "LOG" : Creates a distance vector composed by a logarithmic distribution of values between "dist\_min" and "dist\_max" for a given number of bins defined by the variable "nbin\_dist";Option "FILE" : Uses an existing file with user defined distance vector whose name is set in the variable "dist\_file" |
|  | dist\_min | Min distance for type\_bin \= LIN or LOG |
|  | dist\_max | Max distance for type\_bin \= LIN or LOG |
|  | dist\_step | Step in km between values (used for type\_bin \= LIN) |
|  | nbin\_dist | Number of bins (used for the case type\_bin \= LOG) |
|  | dist\_file | Distance filename including path with a user defined distance vector  (see [figure 8](\#fig\_filesConf\_GITeps)  C). used only when type\_bin \= FILE |
| SUBSET | type\_sel | Option “FILE”: to remove from inversion a list of events from a file indicated in “event\_remove” and/or a list of stations from a file indicated in “station\_remove”. Option “NONE” to keep all data for the inversion (no files are required, “event\_remove” and “station\_remove” are ignored). |
|  | event\_remove | File name (including path) with a list of events to be excluded from the analysis (see [figure 8](images/Figure8.jpg)  A); set to "NONE" to keep all events when "type\_sel" is set to "FILE". |
|  |     station\_remove | File name (including path) with a list of stations to be excluded from the analysis (see [figure 8](images/Figure8.jpg)  B); set to "NONE" to keep all stations when "type\_sel" is set to "FILE". |
| CONSTRAINT DREF | dref | Reference distance \[km\] for which attenuation functions at all frequencies is set to *vdref* |
|  | weight\_dref | Weight of attenuation constraint at reference distance *dref* |
|  | vdref | Attenuation amplitude at all frequencies in correspondence of *dref* |
| CONSTRAINTS SITES | SITES | List of site names to be used as reference ground motion. Set this variable “ALL” to consider the average over all stations \= 0 |
|  | weight\_site | Weight for site condition |
|  | Knot | High\_freq decay kappa value \[s\]; set to *Na* to skip *Knot* |
|  | Fk | Lower frequency bound \[Hz\] to use above which to estimate kappa; set to *Na* to skip *Fk* |
| CONSTRAINTS SMOOTH DIST | weight\_smo | Weight for smoothing condition on attenuation functions, if dynamic smoothing, max. weight will be WSMOOTH, min can be set in functions themselves, currently on WSMOOTH/4 |
| LSQR INVERSION | nrow\_min | Minimum number of lines for each FAS file |
|  | damp | Damping coefficient. Default is 0\. |
|  | atol | Stopping tolerances. *lsqr* continues iterations until a certain backward error estimate is smaller than some quantity depending on atol and *btol*. |
|  | btol | Stopping tolerances. *lsqr* continues iterations until a certain backward error estimate is smaller than some quantity depending on atol and *btol*. |
|  | iter\_lim | Explicit limitation on number of iterations (for safety). |

[Table 4](\#table\_conf\_giteps): Yaml configuration file example for module GITeps.py; for further details about the lsqr inversion see the scipy documentation (https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.lsqr.html).

![Figure 7](images/Figure7.jpg)  
[Figure 7](images/Figure7.jpg) Example of configuration file for module GITeps.py

![Figure 8](images/Figure8.jpg)

[Figure 8](images/Figure8.jpg) A) file “remove\_events.txt” requires the first line including the variable names (NUM and EVENTS separated by “;”. The first column represents a progressive number, the second column shows the event ID. The end line should not be empty. B) file “remove\_stations.txt” requires the first line including the variable names (NUM and STATIONS separated by “;”. The first column represents a progressive number, the second column shows the station ID (network\_code.location\_code.station\_code) . The end line should not be empty. C) The distance file is composed by a single column reporting the User defined distances in increasing order. All txt file names can be arbitrarily modified by the User. 

### **Configuration file for module CorrectFas.py**

The YAML configuration file for module CorrectFas.py has only one block: GLOBAL that includes the general settings. Detailed descriptions are given in [Table 5](\#tab\_conf\_corrFas) and configuration file examples in [Figure 9](images/Figure9.jpg).

| Block | Field | Description |
| ----- | ----- | ----- |
| GLOBAL | job\_name | The User can associate an arbitrary name to his analysis |
|  | att\_file | File txt containing the non-parametric attenuation curves  (output of GITeps.py) |
|  | fas\_dir | Directory containing the Fourier Amplitude Spectra (FAS) files |
|  | site\_dir | Directory containing  the site amplification curves (output of GITeps.py) |
|  | out\_dir | Directory to save the  CorrectFas.py outputs |

[Table 5](\#table\_conf\_corrFas) Yaml configuration file example for module CorrectFas.py

![Figure 9](images/Figure9.jpg)

[Figure 9](images/Figure9.jpg) Example of configuration file for module CorrectFas.py

### **Configuration file for module FitAttenuation.py**

The YAML configuration file for module FitAttenuation.py is composed by the following blocks: GLOBAL contains the general settings; PLOT allows to set the display and save options for the figures; FREQUENCY GS includes options to set the frequency to fit the geometrical spreading; GEOMETRICAL\_SPREADING offers alternatives to define a fitting model for the geometrical spreading; ANELASTIC\_ATTENUATION gives the User the opportunity to define the characteristics of the anelastic attenuation model; KAPPA provides information about the high frequency attenuation. Detailed descriptions are given in [Table 6](\#tab\_fitAtt) and an example in [Figure 10](images/Figure10.jpg).

 

| Block | Field | Description |
| ----- | ----- | ----- |
| GLOBAL | att\_dir | Directory of output results from GITeps.py |
|  | att\_file | File containing the non parametric attenuation curves (output of GITeps.py) |
|  | out\_dir | Directory to save the  FitAttenuation.py outputs |
| PLOT | plot\_graph | "True": shows figures during the code execution; "False": avoids the figures visualization |
|  | save\_graph | "True" saves figures in the output directory; "False" does not save figures |
| FREQUENCY GS | f\_step | The range of variability around the frequency used to calibrate the geometrical spreading (freq) |
|  | freq | Reference frequency to calibrate the geometrical spreading. Commonly 1 Hz is assumed \[Aki and Chouet, 1975\]  |
| GEOMETRICAL SPREADING | gs\_model | Set the geometrical spreading model G(R) to use: Rref/R \= 1, bilinear \= 2, trilinear \= 3, quadrilinear \= 4 |
|  | HD1 | First hinge distance  (only for gs\_model 2, 3 or 4\) |
|  | HD2 | Second hinge distance (only for gs\_model 3 or 4\) |
|  | HD3 | Third hinge distance (only for gs\_model 4\) |
| ANELASTIC ATTENUATION | case | The User can select one of the following  attenuation models by setting this variable as follows:        	1 : fit by considering a G(R) term \+ Q(f) term and NO kappa        	2 : fit by considering a G(R) term \+ Q(f) term and an UNKNOWN kappa        	3 : fit by considering a G(R) term \+ Q(f) term and a KNOWN kappa (being G(R) the geometrical spreading, Q(f) the frequency dependent quality factor and kappa the high-frequency attenuation term) |
|  | corr\_fact\_flag | This flag allows the User to decide if correct G(R) with an exponential term accounting for the anelastic attenuation at 1Hz or not.       	1 : yes (default value)       	0 : no |
|  | Q\_type | Set how to fit the frequency-dependent Q(f):       	0 : linear fitting       	1 : tri-linear fitting |
|  | Q0\_corr | Quality factor at 1 Hz (used if corr\_fact\_flag \= 1\) |
|  | dist\_min\_Q | Minimum distance in km to fit  Q(f) |
|  | dist\_max\_Q | Maximum distance in km to fit  Q(f) |
|  | Vs | S-wave velocity \[Km/s\] |
|  | HF1 | First hinge frequency (only for case Q\_type \= 1\) |
|  | HF2 | Second hinge frequency (only for case Q\_type \= 1\) |
| KAPPA | hingfreq | Hinge frequency \[Hz\] to fit/correct the kappa value (used only if case \= 2 or 3\) |
|  | k\_known | Kappa value if known (used only if case \= 3\) | 

[Table 6](\#table\_fitAtt) Yaml configuration file example for module FitAttenuation.py

![Figure 10](images/Figure10.jpg)

[Figure 10](images/Figure10.jpg) Example of configuration file for module FitAttenuation.py

### **Configuration file for module FitSource.py**

The YAML configuration file for module FitSource.py has the following blocks: GLOBAL contains the general settings; CONSTRAINT: includes constant values for the calculations and information to manage the reference moment magnitudes; CRUSTAL MODEL: provides details on the crustal model to be applied for the source model estimation; SOURCEFITTING: comprises the required information for the source spectral fitting; KAPPA: includes settings for kappa source fitting;  PLOT: allows to set options for the figures; SAVE: provides information to save the figures. Detailed descriptions are given in [Table 7](\#tab\_fitSrc) and configuration file examples in [Figure 11](images/Figure11.jpg). [Figure 12](images/Figure11.jpg) and [Figure 13](images/Figure13.jpg) show example files required in the yaml configuration file for variables  “magnitude\_file”, “f\_range\_file”,  and “crustal\_model”.

| Block | Field | Description |
| ----- | ----- | ----- |
| GLOBAL | job\_name | Title for the analysis  (not used at the moment but will be added to the plots) |
|  | source\_dir | Directory containing  the non parametric source spectra (output of GITeps.py) or directory containing the apparent spectra (output of CorrectFas.py) |
|  | out\_dir | Directory to save the  FitSource.py outputs |
| CONSTRAINT | dref | Reference distance \[Km\] for which attenuation function is set to *vdref* (Table 4). For coherence the User should set this value as in the inversion configuration file (Table 4\) |
|  | Rp | S-wave average radiation pattern |
|  | Fs | Free-surface coefficient |
|  | En | Coefficient that accounts for the separations of S-wave energy onto two horizontal components |
|  | magnitude\_file | File including a list of events with independent Mw. This file has 3 columns: the first is the event ID, the second the Mw value, the third is a flag: 1 means that this Mw is fixed for the event; 0 means that the event is one of those used to calculate the difference between the independent Mw and the  calculated Mw. More details are in [Figure 12](images/Figure12.jpg)|
|  | AnchorageMw | The User can decide to anchorage the estimated Mws to the average of the difference with respect to an independent Mw catalog or not   	0: no   	1: yes |
| CRUSTALMODEL | type\_model | Possible settings: HOMOGENEOUS : in this case variables "Vs" and "rho"  are used for the source model. HETEROGENEOUS: in this case Vs and rho are set reading the file indicated in the variable "crustal\_model" |
|  | crustal\_model | File name (including path) containing the used crustal model |
|  | Vs | S-wave velocity  \[Km/s\] in the source region. For coherence the User should set this value as in the inversion configuration file (Table 4\) |
|  | rho | Density in the source region \[g/cm3\] |
| SOURCEFITTING | type | It is possible to fit the source spectrum in two ways:      	1 : simultaneous fit for M0 and fc;      	2: three steps fit following Bindi and Kota, 2020 (implemented in the future release) |
|  | sourcemodel | Option to choose the source model for future implementations. At the moment this parameter can be set as follows:     	1: Brune’s model |
|  | f\_range | Option to manage the range of frequency for spectrum fit:   1: fmin and fmax from file;   2: fmin is automatically calculated while fmax\=30 Hz  |
|  | f\_range\_file | File name (with path) used when variable "f\_range" is 1 ([Figure 13](images/Figure13.jpg)) |
|  | appflag | As FitSpectra.py can be used to fit both output of GITeps.py and CorrectFas.py it is important to set on which outputs to perform the fitting:   	0:working on median source spectra (\*.source);   	1:working on apparent source spectra (\*.app\_source) |
|  | list\_eve | List of events to fit (used only for the case appflag=1) |
| KAPPA | hingfreq | Lower frequency bound above which to estimate kappa source |
| PLOT | param | Option to decide how create figures: \#ACC : in acceleration (y-axis: m/s) \#VEL : in velocity (y-axis: m) \#DISP: in displacement (y-axis: m\*s) |
|  | plot\_graph | Option to show figures on  the screen : TRUE or FALSE |
|  | list\_eve | List of events for which to show plots on the screen |
| SAVE | save\_graph | Option to save the figures: TRUE or FALSE |
|  | list\_eve | List of events for which to save plots |

[Table 7](\#table\_fitSrc) Yaml configuration file example for module FitSource.py

![Figure 11](images/Figure11.jpg)

[Figure 11](images/Figure11.jpg) Example of configuration file for module FitSource.py

![Figure 12](images/Figure12.jpg)

[Figure 12](images/Figure12.jpg) File required by the yaml configuration file for FitSource.py (variable “magnitude\_file”). It is a text file (example “fixed\_magnitude.txt”) containing, when available, independent information of moment magnitude (Mw) for some events of the dataset. The independent Mw can be derived for example from a reference catalog. The file is composed of 3 columns: column “Event\_ID” includes the event ID; column “MW” shows the independent moment magnitude value; column “fixflag” represents a flag to manage the Mw value within FitSource.py. Events with a flag equal to 1 are used to constrain the source fit, fixing a priori the seismic moment of each individual event to the value corresponding to Mw (generally for large events to mitigate the limited bandwidth effects); events with a flag equal to 0 are used to anchor the seismic moments retrieved from the source spectral fit to a reference moment magnitude catalogue. In the latter case, the average difference between the seismic moments retrieved for the events flagged as 0 and the values in the reference catalogue is calculated and used to remove the bias between the two populations by shifting all source spectra by an amount equal to the calculated bias.

![Figure 13](images/Figure13.jpg)

[Figure 13](images/Figure13.jpg) Example file “f\_range.txt” for the variable “f\_range\_file” of the configuration file (Table 4).

## **Execution**

### **Run Inversion module**

The User can launch the inversion by executing python from the command line as:

`python $code\_path/GITeps.py  \--config $your\_path/conf\_file\_inversion.yaml   \--log logfile`

Example:  
`python   ./GITeps.py   \--config  Conf\_GITeps.yaml  \--log log\_inversion.log`

| Options | Description | Default | Required |
| ----- | ----- | ----- | ----- |
| config | configuration file in YAML format to set the Generalized Inversion of seismological records  | na | True |
| log | log file in TXT format  | na  | True |
| v | Verbose mode (prints summary and other details) | True |  |

[Table 8](\#table\_run\_inv) List of options to run GITeps.py

### **Run CorrectFas.py**

The module CorrectFas.py correcting the recorded spectra (FAS) for the attenuation and site effects, using the GIT spectral decomposition outputs, provides the apparent source spectra. The command to execute this module is:

`python $code\_path/CorrectFas.py  \--config  $your\_path/Conf\_CorrectFas.yaml  \--log $your\_path/logfile.log`

Example:  
`python ./CorrectFas.py \--config Conf\_CorrectFas.yaml   \--log logCorrFAS.log`

| Options | Description | Default | Required |
| ----- | ----- | ----- | ----- |
| config | configuration file in YAML format to execute the CorrectFas.py module  | na | True |
| log | log file in TXT format. | na | True |
| v | Verbose mode (prints summary and other details) | True |  |

[Table 9](\#table\_run\_CorrFas) List of options to run CorrectFas.py

### **Run attenuation modeling module**

The User has the option to run FitAttenuation.py using an interactive mode or an automatic mode. The interactive mode is useful to find the best model by testing various options and immediately visualize the test results with the possibility to make many tests in a short time. The automatic procedure uses the information from the configuration file in case the User already knows the parameters he wants to use to model the non-parametric attenuation curves.

Interactive mode is entered by executing the python code with \--input followed by the non-parametric attenuation file (Attenuation.txt)  without configuration file argument:  
`python $code\_path/FitAttenuation.py    \--input  $your\_path/Attenuation.txt \--log  $your\_path/FitAttenuation.log` 

Automatic mode requires the configuration file:  
`python $code\_path/FitAttenuation.py     \--config  $your\_path/ConfAttFIT.yaml \--log  $your\_path/FitAttenuation.log`

Example:  
`python FitAttenuation.py \--input GITOUT/Attenuation.txt \--log FitAttenuation.log`    (interactive mode)  
`python FitAttenuation.py \--config Conf\_FitAttenuation.yaml \--log FitAttenuation.log` (automatic mode)

| Options | Description | Default | Required |
| :---- | :---- | :---- | :---- |
| input | TXT file containing the non parametric attenuation to be fitted | na | True (only for interactive mode) |
| config | Configuration file in YAML format to set the non-parametric curve fitting. | na | True (only for automatic mode) |
| output | Output file format are: ‘csv’ or  ‘json’; to save data in both formats set ‘all’ | all |  |
| log | Log file in TXT format. | na | True |
| v | Verbose mode (prints summary and other details) | True |  |

[Table 10](\#table\_run\_fitAtt) List of options to run FitAttenuation.py both in interactive and in automatic way

### **Run source modeling module**

FitSource.py can be executed to fit the median source spectra (GITeps.py output source files) or the apparent source spectra (output of CorrectFas.py). In both cases the code is launched in the same way as the configuration file has the information on the spectra to be fitted (see table 4 variable “appflag” and “sourc\_dir”):

`python $code\_path/FitSource.py   \--config $your\_path/Conf\_FitSource.yaml  \--log $your\_path/logsource.log`

Example:  
`python FitSource.py  \--config Conf\_FitSource.yaml \--log logsource.log`

| Options | Description | Default | Required |
| ----- | ----- | ----- | ----- |
| config | Configuration file in YAML format to set the source fitting. | na | True |
| output | Output file format are: ‘csv’ and  ‘json’; to save data in both formats set ‘all’ | all |  |
| log | Log file in TXT format. | na | True |
| v | Verbose mode (prints summary and other details) | True |  |

[Table 11](\#table\_run\_fitSrc) List of option to run the FitSource.py

## **Output files format**

### **Output GITeps.py**

The output files are stored in “out\_dir” as indicated in the configuration file (section 3.4.1). This module creates a file named “Attenuation.txt”, a log file, and three sub-directories: GRAPH, SITES and SOURCES ([Figure 14](images/Figure14.jpg) A). An example of  “Attenuation.txt” is in [Figure 15](images/Figure15.jpg) A. This file includes a table representing the non-parametric attenuation functions. The header line has some general information: the name of the module that created this file, the component used for the analysis (H horizontal, Z vertical), the reference distance selected in the configuration file and the project name. The second line shows the frequencies \[Hz\], and the first column the distances \[Km\]. The other elements of the table are the attenuation values (log10 of the amplitude in cm/s2) corresponding to that combination of frequency-distance. Folder GRAPH contains three plots (.jpg) summarizing the main results of the inversion for source, attenuation and site terms ([Figure 14](images/Figure14.jpg) B). Folder SITES gather the amplification functions for each station. The file extension is “.site”and the name corresponds to the station ID (network\_code.location\_code.station\_code), plus the instrument and component information  ([Figure 14](images/Figure14.jpg) D). As observed in the example ([figure 15](images/Figure15.jpg) C), the header of each file  is composed of the module name that produced it, station ID, band information, component, latitude, longitude, elevation and the true/false operator (true if the station is used as reference station, false otherwise). The first column of this file is the frequency \[Hz\] the second the amplification factor. In folder SOURCES the non-parametric source curves, one for each event, are stored. The extension of these files is “.source” and the name corresponds to the event ID ([Figure 14](images/Figure14.jpg) C). An example of “.source” file is in [figure 15](images/Figure15.jpg) B. Also in this case the header line shows some general information (module name, component, type of composition for horizontal component, event ID, event magnitude from FAS files, latitude, longitude and depth). This line, in some cases, can include an extra parameter that is added after the execution of module FitSource.py (only in case AnchorageMw \= 1). This value is the multiplier factor to be applied to the source spectra obtained by GIT to scale the spectral ordinates coherently with an Mw catalog (see magnitude\_file in Table 7). This option allows the User to compare your results with other studies which used different Mw estimates. Note that starting from line 2 this file is characterized by two columns: the first one is the frequency \[Hz\] and the second the non-parametric source amplitude (log10 scale of the amplitude in cm/s2).  

![Figure 14](images/Figure14.jpg)  

[Figure 14](images/Figure14.jpg): Output files and directories of GITeps.py

![Figure 15](images/Figure15.jpg)

[Figure 15](images/Figure15.jpg): Structure of the output files.

### **Output  CorrectFas.py**

CorrectFas.py produces two files for each event: “.app\_source” and “.source” ([figure 16](images/Figure16.jpg)). They are located in the directory named by the user in the configuration file (see variable “out\_dir” in [table 5](\#tab\_conf\_corrFas) and the example in [figure 9](images/Figure9.jpg)). These files have the same header line composed of general information such as the name of the module that created the file (CorrectFas.py), the component used for the analysis, the event ID, the event magnitude from FAS files, latitude, longitude and depth. File with extension “.app\_source”  ([figure 17](images/Figure17.jpg) A) includes all the apparent source spectra, one for each station. It is represented in matrix form starting from line 2 where the column names are indicated. The first column, named “FDSN”, shows the station ID (network\_code.location\_code.station\_code), the second one (“Dist”) reports the hypocentral distance, the third (“Az”) and fourth (“Bz”) are azimuth and back-azimuth respectively. From the 5th column on all frequencies are reported. If the user wants to extract a particular station spectrum for the event, he should extract the line corresponding to the station ID of interest. The amplitudes are represented in cm/s2. File with extension “.source”  ([figure 17](images/Figure17.jpg) B) is median spectrum for the event. It is characterized by 4 columns: frequency \[Hz\], log10 of median amplitude, the standard deviation and the number of amplitudes used to calculate the median value. Files “.source” can be differentiated from those produced by GITeps.py by reading the first field in the first line. Also the number of columns is different (see [figure 15](images/Figure15.jpg) B and [figure 17](images/Figure17.jpg) B).

![Figure 16](images/Figure16.jpg)

[Figure 16](images/Figure16.jpg) The output directory (in this example SOURCE\_APP) contains two files for each event. The one with extension “.app\_source” and the other one with extension “.source”.

![Figure 17](images/Figure17.jpg)

[Figure 17](images/Figure17.jpg) Example of CorrectFas.py output files for one event. A) File “.app\_source” includes the station source spectra for the event; B) File “.source” is the median spectrum obtained from the apparent spectra.

### **Output FitAttenuation.py**

The outputs of module FitAttenuation.py are saved in the directory indicated in the configuration file (field “out\_dir” in  [table 6](\#tab\_fitAtt)), or, if the interactive execution is chosen, in the output directory indicated by the user.  The figures are saved only in the case field “plot\_graph” in  [table 6](\#tab\_fitAtt) is set to “true”. In this case 3 figures are produced (example in [figure 18](images/Figure18.jpg)). The numerical values of the attenuation model can be saved in two formats: “json” and/or “csv” ([figure 18](images/Figure18.jpg) and [figure 19](images/Figure19.jpg)). For the csv format, anelastic attenuation and geometrical spreading models are saved in two separated files ([figure 19](images/Figure19.jpg) A and B) and the parameters described in a third file named “dictionary.csv” ([figure 19](images/Figure19.jpg) D). If the “json” format is selected the same information are saved in a unique file ([figure 19](images/Figure19.jpg) C). Finally a log-file is saved in the same directory as a “.txt” file. 

![Figure 18](images/Figure18.jpg)

[Figure 18](\#figur\_outFitAtt) Output files of FitAttenuation.py

![Figure 19](images/Figure19.jpg)

[Figure 19](images/Figure19.jpg) Structure of the FitAttenuation.py output files. A) Anelastic attenuation parameters; B) Geometrical spreading parameters; C) Anelastic attenuation and geometrical spreading parameters; D) glossary of each key field of the output files. 

### **Output  FitSource.py**

The FitSource.py outputs are different depending on whether the module is applied to median source spectra (files “.source”, output of GITeps.py) or to the apparent source spectra (files “.app\_source”, output of CorrectFas.py). 

**Fit median source spectra derived from GITeps.py**  

When FitSource.py is applied to the non-parametric source spectra produced by GITeps.py, the module fits the source spectra following the indications given in the configuration file (section 4.1.4). An example of output files is shown in [figure 20](images/Figure20.jpg). Depending on the setting of the configuration file, it is possible to select some events to save the corresponding source spectra and model in acceleration, velocity or displacement (see blocks PLOT and SAVE in [table 7](\#tab\_fitSrc)). Furthermore, three figures summarizing the results on the whole dataset are created ([figure 20](images/Figure20.jpg)). In case the source fitting is not able to find a solution for an event, the corresponding ID is saved in a file named “source\_spectra\_err” so that the user can easily identify and check the event. Another output of this module is a table of source parameters in “.csv” and/or “.json” formats (see [table 11](\#tab\_run\_fitSrc)). The list of the parameters provided in the table for each event is described in [table 12](\#tab\_out\_src\_param) and an example is shown in [figure 21](images/Figure21.jpg). Finally a log file is also provided at the end of the execution of module FitSource.py.

![Figure 20](images/Figure20.jpg)

[Figure 20](images/Figure20.jpg) Output files of FitSource.py when applied to the median non-parametric source spectra derived from GITeps.py (\*.source files).

![Figure 21](images/Figure21.jpg)

[Figure 21](images/Figure21.jpg) Example of source\_parameters.csv. MWcat \= \-9 means that the seismic event is not included in a reference moment magnitude catalogue (see “magnitude\_file” in Table 7).

| Parameter | Description |
| :---- | :---- |
| EveId | Event Identification Number |
| M | Magnitude from FAS input files (the User should know what kind of magnitude he included in his input data files) |
| MWcat | Mw derived from reference catalog (see [figure 12](images/Figure12.jpg)) |
| fmin \[Hz\] | Min frequency used for the source spectra fitting |
| fmax \[Hz\] | Max frequency used for the source spectra fitting |
| M0 \[Nm\] | Seismic moment \[Nm\] |
| M0\_err | Standard error on seismic moment \[Nm\] |
| fc \[Hz\] | Corner frequency \[Hz\] |
| fc\_error | Standard error on corner frequency \[Hz\] |
| M0\_fc\_corr | Correlation coefficient between M0 and fc |
| kappa \[s\] | Source kappa \[s\] |
| k\_err | Standard deviation of the source kappa \[s\] |
| stress\_drop \[MPa\] | Stress drop \[MPa\] |
| stress\_drop\_err | Error propagation on stress drop |
| log10\_Er\_mod \[J\] | log10 of radiated energy (from Brune’s model) |
| AppStress \[Pa\] | Apparent stress by Wyss 1970 (using Er\_mod) |
| log\_10\_Er\_oss \[J\] | log10 of radiated energy \[J\] obtained from non-parametric spectrum |
| log\_10\_Er\_oss\_fit \[J\] | log10 of radiated energy \[J\] obtained from fitted spectrum in the range fmin and fmax |
| AppStress\_oss \[Pa\] | Observed apparent stress by Wyss 1970 (using Er\_oss) |
| log\_10\_Er\_tot \[J\] | Total energy: Er\_oss and extrapolated for low and high frequency \[Ide and Beroza, 2001\] |
| log\_10\_Er\_tot\_fmin\_fmax\_fit \[J\] | Total Energy: Er\_oss\_fit and extrapolated for low and high frequency (Ide and Beroza 2001\)  |
| ηSW | Savage-Wood seismic efficiency	ηSW (Apparent stress / stress drop) |
| κv | Ratio between the estimated energy and the total energy considering the entire frequency range \[Ide and Beroza, 2001\] |
| κv\_fmin\_fmax\_fit | Ratio between the estimated energy and the total energy considering the frequency range \[fmin,fmax\] \[Ide and Beroza, 2001\] |
| Mw | Moment magnitude |

[Table 12](\#table\_out\_src\_param) Source parameters saved in the output file “source\_parameters.csv”

**Fit apparent source spectra derived from CorrectFas.py**

When FitSource.py is applied to the apparent source spectra produced by CorrectFas.py, the module fits each apparent source spectra (files “.app\_source”) following the indications given in the configuration file (section 4.1.4). In particular the source fitting and the source parameters are calculated for the events listed in the configuration file (block SOURCEFITTING, field “list\_eve” in [table 7](\#tab\_fitSrc)). For each event a file “ \*\_appsource\_param.csv” (where \* is the event ID) is created ([figure 22](images/Figure22.jpg)) including the source parameters obtained for each available station. An example is shown in [figure 22](images/Figure22.jpg) B, where a table of values is shown for an example event. In column A the user can read the list of the parameters, and from column B onwards, the corresponding values for each station whose name is indicated on line 1\.

![Figure 22](images/Figure22.jpg)

[Figure 22](images/Figure22.jpg) Output files of FitSource.py when applied to the apparent source spectra derived from CorrectFas.py (\*.app\_source files)