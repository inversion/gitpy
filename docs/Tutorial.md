# **Tutorial Guide for New Users**

The following is a step-by-step guide for a specific example taken from the Central Italy region.

### **Example Dataset**

From the GitLab README page it is possible to download a small dataset ([GITpy\_db](https://drive.google.com/file/d/1RbGhe6y2QTUb479qmCute7fc3I0NLu5c/view?usp=sharing)) to be used as an example to test the GITpy code and to better understand the typical structure of an input dataset for this code. The training dataset of GITpy is composed of \~7000 accelerometric three-component waveforms related to 209 seismic events (3.0 \<= Ml \<= 6.14) recorded in Central Italy by 53 stations (Figure 23). The dataset includes the mainshocks of the latest seismic sequences that occurred in the study area (L’Aquila, 2009; Amatrice-Visso-Norcia, 2016). **GITpy\_db** is well sampled in the near-field region Repi \[20-40 Km\] and the hypocentral depth is on average lower than 10 Km (Figure 24). The azimuthal coverage is not optimal because most of the stations are located in the NNW-SSE direction (Figure 23,24).   
Notice that **GITpy\_db** does not fully reflect the ground motion features of Central Italy. It can be used just to test the functionalities of GITpy before attempting to derive region-specific source, attenuation, and site spectral parameters from more accurate datasets.

![Figure 23](images/Figure23.jpg)

[Figure 23](images/Figure23.jpg) Left: Spatial distribution of events and stations included in the GITpy training dataset and right: magnitude-distance distribution in function of the number of waveforms.

![Figure 24](images/Figure24.jpg)

[Figure 24](images/Figure24.jpg) Top panel: Number of records according to events magnitude (left) and depth (right): Bottom panel: Number of records in function of recording station azimut (left) and hypocentral distance (right).

**GITpy\_db** is composed of a set of flat files which contain three component Fourier Amplitude Spectra (FAS) of and related events and station metadata, one for each spectral amplitude frequency to analyze in the frequency range \[0.5 \- 25.06 Hz\]. The FAS have been calculated on S-wave windows selected on the base of a frequency-dependent threshold on the signal-to-noise ratio (SNR, e.g. SNR \= 10 for 0.2–0.4 Hz, SNR \= 5 for 0.4–15 Hz and SNR \= 10 for f \> 15 Hz). The time windows used to calculate the spectral amplitudes were selected considering the fraction of cumulated energy (i.e. 90% for data recorded within an hypocentral distance of 25 km; 80% for distances between 25 and 50 km and 70% for larger distances), imposing a minimum duration of 4 s. The FAS are smoothed using the Konno and Ohmachi (1998) algorithm (b \= 40).

![Figure 25](images/Figure25.jpg)

[Figure 25](images/Figure25.jpg) Graphical example of output by using the **GITpy\_db** dataset.

An example of graphical output for source spectra, attenuation curves, and site amplification is shown in Figure 25\. Each file in GITpy\_d has the extension “.fas”.  Below is an example (file FAS\_001.00.fas):

![Figure 26](images/Figure26.jpg)

[Figure 26](images/Figure26.jpg) Example of FAS file (file with extension “.fas”).

The User can create its own input files with an arbitrary number of columns to include various information (as in the example file above). GITpy will read only mandatory fields, the other columns will be ignored. The table below shows only the mandatory columns:

| Field | Description | Format | Mandatory |
| ----- | ----- | ----- | ----- |
| ID | Identification number of the event | string  | yes |
| Ml | Local Magnitude | float  | yes |
| EvLat | Event latitude \[°\] | float  | yes |
| EvLon | Event longitude \[°\] | float  | yes |
| EvtDpt | Event depth \[Km\] | float  | yes |
| Ty |  |  |  |
| Stat |  |  |  |
| FDSN | Code to identify the seismic station. The identifier is composed of the FDSN network\_code, the location\_code, and the station\_code separated by a “.” | char | yes |
| CMP | Identifier code composed of two letters: the first indicates the general sampling rate and response band of the data source; the second one specifies the family to which the sensor belongs. | char |  |
| MSta |  |  |  |
| StLat | Station latitude | float | yes |
| StLon | Station longitude | float | yes |
| StElev | Station elevation \[m\] | float | yes |
| Depi | Epicentral distance \[Km\] | float | yes |
| Dipo | Hypocentral distance \[Km\] | float | yes |
| Az | Event to station azimuth \[°\] | float | yes |
| FAS\_Z | Fourier amplitude on vertical component \[cm/s\] | float | yes |
| FAS\_NS | Fourier amplitude on NS component \[cm/s\] | float | yes |
| FAS\_EW | Fourier amplitude on EW component \[cm/s\] | float | yes |
| LenZ |  |  |  |
| LenNS |  |  |  |
| LenEW |  |  |  |
| Freq | Frequency of reference for the FAS measurements \[Hz\] | float | yes |
| TP |  |  |  |
| TS |  |  |  |
| ZE |  |  |  |
| ZS |  |  |  |

[Table 13](\#table\_dataset): Description of the fields included in the **GITpy\_db** FAS files (\*.fas). The mandatory fields are required to run GITpy, while the metadata related to the optional ones (in gray) contribute to qualify the dataset. Note that in **GITpy\_db** we provided the local magnitude Ml. In a further release of GITpy we will implement the possibility to consider different types of magnitude. 

## **Example Jupyter Python Notebooks**

In this section we provide some Jupyter Notebooks tutorials to guide the User on how to run the four main python modules of GITpy (Table 2). Make sure you have Jupyter installed on your laptop. If not, you can install Jupyter in the *gitpy* python environment as follows if you are a LINUX User:

`*pip3 install notebook*`

To run the notebook:

`*jupyter notebook*`

On Windows, you can run Jupyter via the shortcut Anaconda adds to your start menu. In both cases it will open a new tab in your default web browser that should look something like the screenshot in Figure 27\. The Jupyter Notebooks (.ipynbs files) of the GITpy project can be used as tutorials to help new Users to run codes, display the results, and read explanations of each block of the source codes. Figure 27-30 give examples of Notebooks for the four main python modules of GITpy (Table 2).

![Figure 27](images/Figure27.jpg) 

[Figure 27](images/Figure27.jpg) Example of dashboard, specifically designed for managing Jupyter Notebooks (.ipynb files).

![Figure 28](images/Figure28.jpg)

[Figure 28](images/Figure28.jpg) Example of Jupyter Notebook to run the GITpy inversion module (_GITeps.py_) to separate the FAS into the following three terms: A) path attenuation; B) Source spectra; C) site amplification. The blue lines in the plot of the frequency dependent site amplification factors indicate the reference ground motion used to constraint the solution of the overdetermined linear system (eq. 1), whereas the red lines indicate the median reference site seismic response. 

![Figure 29](images/Figure29.jpg)

[Figure 29](images/Figure29.jpg)  Example of Jupyter Notebook to run the GITpy module (_GITeps.py_) developed for the physical parametrization of the non-parametric attenuation curves (A) in terms of: geometrical spreading (B) and frequency dependent quality factor (C).

![Figure 30](images/Figure30.jpg)

[Figure 30](images/Figure30.jpg) Example of Jupyter Notebook to run the GITpy module (_FitSource.py_) developed for the physical parametrization of the source spectra in terms of: seismic moment M0 and corner frequency fc  (A) and frequency ksource (B). Other source parameters are provided by the code as already documented in the 3.5.4 section.