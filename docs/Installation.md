# **Installation**

This tutorial will guide you through the installation of the GITpy software, its basic requirements and the management of its specific dependencies using a Python3 virtual environment for both Linux and Windows users.  
The first step for both operating systems is to download GITpy from GitLab repository at: [https://gitlab.rm.ingv.it/inversion/gitpy/-/tree/GITpy](https://gitlab.rm.ingv.it/inversion/gitpy/-/tree/GITpy).

The User may also download the source code of a specific release of GITpy following the same instruction of the development software for its installation:

[v.1.0.0-gitpy](https://gitlab.rm.ingv.it/inversion/gitpy/-/releases/v1.0.0)

[Table 1](\#tab\_requirements) shows all the requirements.

| Element | Version |
| :---: | :---: |
| Python | 3.9 |
| numpy | 1.24.4 |
| scipy | 1.13.0 |
| pandas | 2.2.2 |
| plotnine | 0.13.6 |
| setuptools | 69.5.1 |
| pyyaml | 6.0.1 |
| obspy | 1.4.1 |
| openpyxl | 3.1.2 |
| lmfit | 1.2.2 |

[Table 1](\#table\_requirements) GITpy requirements.

### **Installing Python3 and required Python3 libraries (pip and venv)**

* Python3

Before you go any further, make sure you have *Python3* installed and that it is available from your command line. As it is usually already included by default on many GNU/Linux distributions, you can check this by running the following command at a shell prompt:

*python3 \--version*

You should get some output with the exact version installed (e.g. 3.9). If you do not have Python3, you can install it using the following commands:

1. Update the package list using the following command:

   `*sudo apt update*`

2. Use the following command to install Python3:

   `*sudo apt install python3*`

* pip3

  Next, make sure you have *pip3* available. Chances are it is already installed, you can check this by running the following command at a shell prompt:

  `*pip3 \--version*`


  and you should get an output text with the exact version of the pip3 package installed on your system. If not, you can install pip3 as follows:

1. Update the package list using the following command (not needed if you just did it in the previous step):

   `*sudo apt update*`

2. Use the following command to install pip3:

   `*sudo apt install python3-pip*`

* venv

  Now, make sure you have the Python3 *venv* module available, you can check this by running the following command at a shell prompt:

  `*python3 \-m venv*`

  and you should get an output text with a brief usage synopsis. If not, you can install the *venv* module as follows:

1. Update the package list using the following command (not needed if you just did it in one of the previous steps):

   `*sudo apt update*`

2. Use the following command to install the *venv* module for Python3:

   *`sudo apt install python3-venv*`

### **Download the Ground Motion Processing repository**

You can create a local copy of the latest development version of GITpy by cloning the *git* repository or download the package from the project main web page and extract it to any directory you like. If you choose to use *git*, you can check if it is already installed:

`*git \--version*`

or install it the usual way:

`*sudo apt install git*`

From a terminal window, change to the local directory where you want to clone your repository and run:

`*git clone git@gitlab.rm.ingv.it:inversion/gitpy.git*`

or

`*git clone https://gitlab.rm.ingv.it/inversion/gitpy.git*`

If the clone was successful, a new sub-directory appears on your local drive in the directory where you cloned your repository. This directory has the same name as the GitLab repository that you cloned. To check how many branches of the GITpy development are available in the GitLab repository run:

`*git branch*`

a list like this will appear:

*(gitpy) user@MyComputer:\~/gitpy$ git branch*  
*GITpy*  
 *\* main*

to switch to another branch use:

 `*git checkout GITpy*`

followed by

`*git status*`

to verify that you are on the desired branch of GITpy development.

### **Creating a Virtual Environment**

GITpy was developed using python libraries that are not included in the standard installation of Python 3.9 such as *numpy*, *scipy*, or *pandas* (see Table 1). To manage dependencies avoiding conflict due to different version installed on own Operating System you can create a "Python virtual environment" specifying the target directory (absolute or relative to the current directory) with the ground motion processing repository:

`*python3 \-m venv /path/to/new/virtual/environment*`

For example:

`*python3 \-m venv /path/to/gitpy/*`

To activate the Virtual Environment

`*cd /path/to/gitpy/*`

and run

`*source bin/activate*`

You should get a command line like this:

`*(gitpy) user@MyComputer:/gitpy$*`

At this point you are ready to install the GITpy requirements with pip3 only in the virtual environment:

`*pip3 install \--upgrade pip*`

`*pip3 install obspy*` (this will also install numpy, maplotlib and scipy)

`*pip3 install pandas plotnine setuptools pyyaml openpyxl lmfit PyQt5 tornado*`

`*sudo apt install python3-tk*`

You should be now ready to use the GITpy software\!

You can "exit" the virtual environment by deactivating it wit the following command:

`*deactivate*`

## **Instructions for Windows Users**

There are several ways to run Python scripts like GITpy on Windows 11, but the recommended procedure is to use Conda or Anaconda Navigator, to easily manage dependencies and create an isolated environment to install and run GITpy. In the following we describe the procedure for using Anaconda Navigator (Navigator enables you to use Conda to manage environments and packages, but in a graphical application instead of a command line interface).

1. Install Anaconda by following the instructions provided here:         [https://docs.anaconda.com/free/anaconda/install/windows/](https://docs.anaconda.com/free/anaconda/install/windows/)  
     
3. On the Environment page (on the left), click **Create** (Figure 1\)**.** 

<img src="images/Figure1.jpg" alt="Figure 1" width="1000"/> 

[Figure 1](images/Figure1.jpg) Screenshot of the Anaconda navigator 

4. Give a name to the new environment (for example “gitpy”), click on Python and select version 3.9. Now you have a new environment “gitpy” with the correct Python version installed to run GITpy.  
     
<img src="images/Figure2.jpg" alt="Figure 1" width="400"/>

[Figure 2](images/Figure2.jpg) Example on how create a new environment

5. From the home page, switch to the new “gitpy” environment by clicking on its name  (Figure 3).  
    

![Figure 3](images/Figure3.jpg)

[Figure 3](images/Figure3.jpg) Example of selection of the virtual environment created to run GITpy

6. Go back to the **Environments** page and check that you have the “conda-forge” channel, if not you can add it:   
* click on the **Channels** button   
* then click the **Add** button   
* enter the channel URL: [https://conda.anaconda.org/conda-forge/](https://conda.anaconda.org/conda-forge/)   
* Press  **Enter** on your keyboard  
* Click on the **Update channels** button.  
    
7. From the Environments page check the installed packages versions (Figure 4). Make sure you have all the required packages and the correct versions, as listed in Requirements table 1\.  
   

![Figure 4](images/Figure4.jpg)

[Figure 4](images/Figure4.jpg) Example of Environment page of Anaconda navigator 

If some packages are not the correct version or they are missing at all, follow the instructions below:  
[https://docs.anaconda.com/free/navigator/tutorials/manage-packages/](https://docs.anaconda.com/free/navigator/tutorials/manage-packages/)

8. Go back to the Home page, you should now be in the new “gitpy” environment (check). Among the default applications there is “PowerShell Prompt “. Launch it and you are ready to run GITpy from the command line.  

![Figure 5](images/Figure5.jpg)  
   
[Figure 5](images/Figure5.jpg) Example of the home page of the Anaconda navigator to select the Powershell Prompt

9. Download GITpy from Gitlab repository at: [https://gitlab.rm.ingv.it/inversion/gitpy-trial](https://gitlab.rm.ingv.it/inversion/gitpy-trial) save and unzip it in your work directory. Then from the PowerShell Prompt window go into your work directory to run the python codes (Figure 5). 