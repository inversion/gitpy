#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy.optimize import curve_fit
from LibGit.utilities import linfunc1,linfunc2

class RegKappa:
    
    def __init__(self,freq_vec,freq_single,HF):

        self.freq_vec =  freq_vec
        self.hingfreq = HF
        self.freq = freq_single
        self.kappa = None

    def Case2(self,X,Y,Vs,index):

        if self.freq >= self.hingfreq:
            
            popt, pcov = curve_fit(linfunc1, X, Y)
            Qf = -(np.pi*self.freq)/(Vs*popt[1])
            #perr = np.sqrt(np.diag(pcov))
            #print(perr)
            #Q = (-np.pi*self.freq)/(popt[0]*Vs)
            perr_Q = np.sqrt(np.diag(pcov))[1]
            #std_BETA = np.sqrt(np.diagonal(pcov))
            Q_plus = np.pi*self.freq/(Vs*abs(popt[1]+perr_Q*2))
            Q_minus = np.pi*self.freq/(Vs*abs(popt[1]-perr_Q*2))
            std_Q = Q_plus - Qf          
            #std_Q = np.pi*self.freq/Vs*popt[1]*popt[1]*np.abs(popt[1]-float(perr_Q))
            kappa = -popt[0]/(np.pi*self.freq)
            perr_kappa = np.sqrt(np.diag(pcov))[1]            
                        
            #dict = {int(index):{"Frequency [Hz]":self.freq, "acoeff": float(popt[0]), "bcoeff": float(popt[1]), "std_acoeff": None, "std_bcoeff": None, "Q(f)": Qf, "kappa": float(kappa)}}
            dict = {int(index):{"Frequency [Hz]":self.freq, "bcoeff": float(popt[1]), "std_bcoeff": perr_Q, "Q(f)": Qf, "Q": Qf,"yerr": std_Q,"err_high": Q_plus, "err_low": Q_minus, "kappa": float(kappa)}}
        else:
            popt, pcov = curve_fit(linfunc2, X, Y)
            Qf = -(np.pi*self.freq)/(Vs*popt[0])
            perr_Q = np.sqrt(np.diag(pcov))
            #print(perr)
            Q_plus = np.pi*self.freq/(Vs*abs(popt[0]+perr_Q*2))
            Q_minus = np.pi*self.freq/(Vs*abs(popt[0]-perr_Q*2))
            std_Q = Q_plus - Qf
            #Q = (-np.pi*self.freq)/(popt[0]*Vs)
            #std_Q = np.pi*self.freq/Vs*popt[0]*popt[0]*np.abs(popt[0]-float(perr_Q))
            #dict = {int(index):{"Frequency [Hz]":self.freq, "acoeff": float(popt[0]), "bcoeff": float(popt[1]), "std_acoeff": None, "std_bcoeff": None, "Q(f)": Qf, "kappa": 0}}
            dict = {int(index):{"Frequency [Hz]":self.freq, "bcoeff": float(popt[0]), "std_bcoeff": float(perr_Q), "Q(f)": Qf, "Q": Qf,"yerr": std_Q,"err_high": Q_plus[0], "err_low": Q_minus[0], "kappa": 0}}
                    
        return dict  

    def Case3(self,X,Y,Vs,index,kval):
        
        popt, pcov = curve_fit(linfunc2, X, Y)
        Qf = -(np.pi*self.freq)/(Vs*popt[0])
        perr = np.sqrt(np.diag(pcov))[0]
        #std_Q = np.pi*self.freq/Vs*popt[0]*popt[0]*np.abs(popt[0]-float(perr))
        Q_plus = np.pi*self.freq/(Vs*abs(popt[0]+perr*2))
        Q_minus = np.pi*self.freq/(Vs*abs(popt[0]-perr*2))
        std_Q = Q_plus - Qf
        #perr_Q = np.sqrt(np.diag(pcov[1]))

        if self.freq >= self.hingfreq:
            
            #dict = {int(index):{"Frequency [Hz]":self.freq, "bcoeff": float(popt[0]), "std_bcoeff": None, "Q(f)": Qf, "kappa": float(kval)}}
            dict = {int(index):{"Frequency [Hz]":self.freq, "bcoeff": float(popt[0]), "std_bcoeff": float(perr), "Q(f)": Qf, "Q": Qf,"yerr": std_Q,"err_high": Qf + std_Q, "err_low": Qf - std_Q, "kappa": float(kval)}}

        else:
            #dict = {int(index):{"Frequency [Hz]":self.freq, "bcoeff": float(popt[0]), "std_bcoeff": None, "Q(f)": Qf, "kappa": 0}}
            dict = {int(index):{"Frequency [Hz]":self.freq, "bcoeff": float(popt[0]), "std_bcoeff": float(perr), "Q(f)": Qf, "Q": Qf,"yerr": std_Q,"err_high": Qf + std_Q, "err_low": Qf - std_Q, "kappa": 0}}
            
        
        return dict    
    
    def AttCorrGR_k_DF(self,df,k_known):
        
        df['AcorrGR_k'] = df["AminusGR"]
        for ind in range(len(df['AcorrGR_k'])):
            freq = df['FR'].iloc[ind]
            if freq >= self.hingfreq:
                df['AcorrGR_k'].iloc[ind] = df["AminusGR"].iloc[ind] + np.pi*(freq - self.hingfreq)*k_known 
        return df
      