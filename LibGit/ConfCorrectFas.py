#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
import glob
import os
import sys
import gc
import pandas as pd
import numpy as np
from yaml.loader import SafeLoader
from scipy import interpolate
from pathlib import Path
from LibGit.Stations import Stations
from LibGit.Attenuation import Attenuation

class FasName:
    def __init__(self, nm, f0):
        self.freq = float(f0)
        self.name = nm
        
class StaFas:
    def __init__(self, nm,nfr,dist,az,baz,value, nf0):
        self.FDSN = nm
        self.Dipo=dist
        self.Azim=az
        self.Baz=baz
        self.val = np.zeros(shape=(nfr))
        self.val[:] = np.NaN
        self.val[nf0]=value      
        
class MapEvtSta:
    def __init__(self, Id0):
        self.Id = Id0
        self.nsta=0
        self.lista_sta = []
        self.listaStaz = []
        self.mat=None
        
    def addSta(self,staz,nf,dist,az, baz,val, nf0):
        p = staz in self.lista_sta
        if (p == False):
            self.nsta=self.nsta+1
            self.lista_sta.append(staz)
            self.nfreq=nf
            self.listaStaz.append(StaFas(staz,nf,dist,az,baz,val, nf0))
        else:
            index_sta=self.lista_sta.index(staz)
            sta=self.listaStaz[index_sta]
            sta.val[nf0]=val
            #print(index_sta)        
        
class ConfCorrectFas:
    
    def CreateMeshAtt(self):
        x = list()
        y = list()
        z = list()
        for j in range(self.Att.ndist):
            d0=self.Att.dist[j]
            for i in range(self.Att.nfreq):
                f1=self.Att.freq[i]
                val=self.Att.att[j][i]
                x.append(d0)
                y.append(f1)
                z.append(val)
        x = np.array(x)
        y = np.array(y)
        z = np.array(z)
        self.mesh_att=interpolate.interp2d(x, y, z, kind='linear')
        
                        
    def ReadFas(self):
        self.fas = list()
        s1 = "  Reading Fas files fom dir: {:s} ".format(self.fas_dir)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        self.Tables = list()
        os.chdir(self.fas_dir)
        fil = "*.fas"
        s1 = " TOTAL NUMBER OF FAS files (*.fas):  {:10d}".format(len(glob.glob(fil)))
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        nskip=0
        for file in glob.glob(fil):
            gc.collect()
            name = os.path.join(self.fas_dir, file)
            data = pd.read_table(name, header=0, delim_whitespace=True, nrows=1)
            f1=0
            fmin=self.Att.freq[0]-0.01
            fmax=self.Att.freq[self.Att.nfreq-1]+0.01
            if len(data) >= 1:
                f1 = data.iloc[0].Freq
                pp=(f1 >= fmin and f1 <= fmax)
                nr=len(data)
            else:
                pp=False
                nr=0
            if pp == True:
                self.fas.append(FasName(name, f1))
            else:
                s1 = "Warning FAS File: {:s}, Freq {:6.3f} Hz, Ndat: {:d},   Skipped !".format(name,f1,nr)
                self.log.write(s1)
                self.log.write("\n")
                print(s1)
                nskip = nskip + 1
        self.nf = len(self.fas)
        if self.nf == 0:
            msg = f" ERROR SELECTED FAS files = 0 (*.fas), FAS Dir {self.fas_dir} Aborting"
            self.log.write(s1)
            self.log.write("\n")
            print(msg)
            sys.exit()
        self.fas.sort(key=lambda x: x.freq, reverse=False)
        nread = 0
        for i in range(len(self.fas)):
            name = self.fas[i].name
            f1 = self.fas[i].freq
            self.freq.append(f1)
            data = pd.read_table(name, header=0, delim_whitespace=True)
            ndat = len(data)
            s1 = "FAS: {:5d} {:s} {:6.3f} Hz Ndat: {:10d}".format(nread + 1, name, f1, ndat)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            self.Tables.append(data)
            if nread == 0:
                self.evt = data[['Id', 'Ml', 'EvLat', 'EvLon', 'EvtDpt']]
                self.sta = data[['FDSN', 'CMP','StLat', 'StLon', 'StElev']] # modificato 
            else:
                self.evt=pd.concat([self.evt,data[['Id', 'Ml', 'EvLat', 'EvLon', 'EvtDpt']]])
                self.sta=pd.concat([self.sta,data[['FDSN', 'CMP','StLat', 'StLon', 'StElev']]])
            nread = nread + 1
        self.evt = self.evt.drop_duplicates('Id', keep='last')
        self.evt = self.evt[["Id", "Ml", "EvLat", "EvLon", "EvtDpt"]].astype(str, float)
        self.evt = self.evt.sort_values(by=['Id'], ascending=False)
        self.evt = self.evt.reset_index()
        for i in range(len(self.evt)):
             id0 = self.evt.iloc[i].Id
             if len(id0) != 11:
                pass
             else:
                id0 = "0" + id0
                self.evt.iloc[i].Id=id0
      

        s1 = "....... TOTAL NUMBER OF EVENTS: {:d}".format(len(self.evt))
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        for i in range(len(self.evt)):
            self.Map_Evt_Sta.append(MapEvtSta(self.evt.iloc[i].Id))
        self.sta = self.sta.drop_duplicates('FDSN', keep='last')
        self.sta = self.sta.drop_duplicates()
        self.sta = self.sta[['FDSN', 'CMP','StLat', 'StLon', 'StElev']].astype(str, float)
        s1 = "....... TOTAL NUMBER OF STATIONS: {:d} -----------".format(len(self.sta))
        self.log.write(s1)
        self.log.write("\n")
        self.FDSN=self.sta['FDSN'].values.tolist()
        self.FDSN.sort(reverse=False)
        
        for i in range(len(self.Sites.staGit)):
            staz=self.Sites.staGit[i].FDSN
            val=(staz in self.FDSN)
            if val == True:
             self.GoodSta.append(True)
             #s1 = "  Warning Station: {:s}, site function from dir: {:s} does not exist !".format(staz,self.site_dir)
             #print(s1)
            else:  
              s1 = "  Warning Station: {:s}, site from dir: {:s} is not included in the input FAS data set !".format(staz,self.site_dir)
              self.log.write(s1)
              self.log.write("\n")
              self.GoodSta.append(False)
              print(s1)
        self.goodSite=np.zeros(len(self.FDSN))
        for i in range(len(self.FDSN)):
             self.goodSite[i]=0
             val=False
             staz=self.FDSN[i]
             for j in range(len(self.Sites.staGit)):
                 if staz == self.Sites.staGit[j].FDSN:
                     val=True
                     self.goodSite[i]=1
             if val == False:
                  s1 = "  Warning Station: {:s}, station from input FAS data set: {:s} is not included in site functions data set !".format(staz,self.site_dir)
                  self.log.write(s1)
                  self.log.write("\n")
                  print(s1)        
        
    def __init__(self,fname,logf):
        self.ConfName = fname
        self.log = logf
        self.fas=None
        self.job_name=None
        self.att_file=None
        self.site_dir=None
        self.fas_dir=None
        self.Att=None
        self.Sites=None
        self.FDSN =None
        self.cmp=None
        self.GoodSta = [] 
        self.freq = []
        self.goodSite = []
        self.scale=1
        self.mesh_att=None
        self.Map_Evt_Sta = []
        self.Map_Evt_Stat=None
        if Path(fname).suffix != '.yaml':
            msg = f" Config File {fname} not yaml extension\n Aborting"
            print(msg)
            sys.exit(1)
        try:
            with open(fname) as f:
                   data = yaml.load(f, Loader=SafeLoader)
        except FileNotFoundError:
            print(f"File {fname} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {fname}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {fname} is", repr(err))
            sys.exit(1)
        s1 = "....... START reading Ymal Configuration File: {:s} ".format(fname)
        #self.log = open(self.log, "w")
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        s1 = "-- Section GLOBAL --"
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        self.job_name = data["GLOBAL"]["job_name"]
        s1 = "> Job Name: {:s} ".format(self.job_name)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        self.att_file = data["GLOBAL"]["att_file"]
        s1 = "> Attenuation file: {:s} ".format(self.att_file)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        p1 = os.access(self.att_file, os.R_OK)
        if p1:
            pass
        else:
            msg = f" FATAL ERROR, CANNOT READ FILE: {self.att_file}.. Aborting"
            self.log.write(s1)
            self.log.write("\n")
            print(msg)
            sys.exit()
        self.Att = Attenuation(self.att_file)
        s1 = "  Reading Attenuation file: {:s} ".format(self.att_file)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        self.cmp=self.Att.cmp
        if self.cmp == 'H':
           self.mean = 'VECTORIAL'
        s1 = "  Attenuation Job Name:  {:s},  Ndist {:d},   Nfreq {:d}, CMP: {:s}, Dref: {:f}".format(self.Att.job_name,self.Att.ndist,len(self.Att.freq),self.Att.cmp,self.Att.dref)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        self.CreateMeshAtt()
        self.site_dir = data["GLOBAL"]["site_dir"]
        s1 = "> Site dir: {:s} ".format(self.site_dir)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        p1 = os.access(self.site_dir, os.W_OK)
        if p1:
            s1 = "  Reading site functions form dir: {:s} ".format(self.site_dir)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            self.Sites = Stations(self.site_dir)
            s1 = "  Num of sites:  {:d}".format(self.Sites.nsta)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            self.Sites.staGit.sort(key=lambda x: x.FDSN, reverse=False)
            if self.Sites.nsta == 0:
                msg = " FATAL ERROR Num of sites = 0 .. Aborting"
                self.log.write(s1)
                self.log.write("\n")
                print(msg)
                sys.exit()
        else:
            msg = f" FATAL ERROR, CANNOT READ DIR: {self.site_dir}.. Aborting"
            self.log.write(s1)
            self.log.write("\n")
            print(msg)
            sys.exit()
        self.fas_dir = data["GLOBAL"]["fas_dir"]
        s1 = "> Fas dir: {:s} ".format(self.fas_dir)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        p1 = os.access(self.fas_dir, os.W_OK)
        if p1:
            self.ReadFas()
        else:
            msg = f" FATAL ERROR, CANNOT READ DIR: {self.fas_dir}.. Aborting"
            self.log.write(s1)
            self.log.write("\n")
            print(msg)
            sys.exit()
        self.out_dir = data["GLOBAL"]["out_dir"]
        s1 = "> OUT  dir: {:s} ".format(self.out_dir)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        val = os.path.isdir(self.out_dir)
        if val:
             s1 = "> WARNING Out Dir:  {:s} Exist! ".format(self.out_dir)
             self.log.write(s1)
             self.log.write("\n")
             print(s1)
             write = os.access(self.out_dir, os.W_OK)
             if write == False:
                 msg = f" Cannot write Out Dir {self.out_dir} \n Aborting"
                 self.log.write(msg)
                 self.log.write("\n")
                 print(msg)
                 sys.exit()
        else:
             os.mkdir(self.out_dir)
             flag = os.access(self.out_dir, os.W_OK)
             if not flag:
                 msg = f" Cannot create Out Dir {self.out_dir} \n Aborting"
                 self.log.write(msg)
                 self.log.write("\n")
                 print(msg)
                 sys.exit()
             else:
                 s1 = " Succesfully created Out Dir:  {:s} ".format(self.out_dir)
                 self.log.write(s1)
                 self.log.write("\n")
                 print(s1)

       