#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import glob
import shutil
import pandas as pd
import numpy as np
import math
import argparse
from plotnine import ggplot,aes,geom_line,xlab,ylab, scale_color_distiller,\
    theme_bw, ggtitle, scale_y_log10, geom_errorbar,geom_errorbarh,scale_x_continuous, geom_histogram,\
        geom_point, geom_vline,annotate,scale_linetype_manual
from pathlib import Path
from scipy.interpolate import UnivariateSpline

def usageGit(msg):
    print(msg)
    sys.exit()
    
def linfunc1(x,a,b):
        y = a + b*x
        return y 
    
def linfunc2(x,b):
        y = b*x
        return y     
    
def plotSingleAttenuationGRmod(Att,f_single,dtot3,X,Y):

    d_fit_belove_HD = {'D_Km': X, 'GR': Y, 'TY': str(1), 'FR': 1}
    GR_fit_belove_HD = pd.DataFrame(d_fit_belove_HD)
    print(GR_fit_belove_HD)
    print("*****")
    print(dtot3)
    fmin=Att.freq[0]
    fmax=Att.freq[Att.nfreq-1]
    lm = []
    lm = [fmin,fmax]
    stit = "Git: {:s}, Job Name: {:s}, CMP: {:s} Freq: {:5.1f} ".format(Att.progname,Att.job_name,Att.cmp,1)
    ndist=len(Att.dist)

    g = ggplot()
    g=  g +  geom_line(dtot3, aes(x='x', y='y', group='TY',color='FR'),size=1.0,linetype="dotted")
    g= g + geom_line(GR_fit_belove_HD, aes(x='D_Km', y='GR', group='TY',color='FR'))
    #g=  g + scale_color_distiller(name='Freq  [Hz]',palette=9,type='div',limits=lm)
    g=g + xlab("Hypocentral [km]")  +  ylab("Log(Attenuation)")
    g = g + ggtitle(stit)
    g=g+theme_bw()
    print(g)      

def freq_range (f_tot,f_single,f_step):
    f_tot = np.array(f_tot)
    ind_f1 = np.where(f_single - f_step <= f_tot)
    ind_f2 = np.where(f_tot < f_single + f_step)
    ind_f = np.intersect1d(ind_f1[0],ind_f2[0])
    
    return f_tot[ind_f]

def getAttDF(Att,f_single,f_step):
    dtot=None
    for i in range(len(Att.freq)):
        y1 = np.empty(Att.ndist, dtype=float)
        for j in range(Att.ndist):
            y1[j] = Att.att[j][i]
        fr = round(Att.freq[i], 2)
        f1 = []
        f1 = [fr for i in range(Att.ndist)]
        frr = str(fr)
        s1 = []
        s1 = [frr for i in range(Att.ndist)]
        s2 = []
        s2 = ["GIT" for i in range(Att.ndist)]
        d1 = {'x': Att.dist, 'y': y1, 'TY': s1, 'FR': f1,"Type": s2}
        d1 = pd.DataFrame(d1)
        if i == 0:
            dtot = d1
        else:
            dtot = dtot.append(d1, ignore_index=True)
        #dtot = dtot.append(d1)
        del d1
            
    f_range_index = freq_range (Att.freq,f_single,f_step)

    cond = dtot["FR"] < f_range_index[-1]
    dtot2 = dtot.loc[cond]   
    cond2 = dtot2["FR"] > f_range_index[0]
    dtot3 = dtot2.loc[cond2]
    
    return dtot3
          
def plotSingleAttenuation(Att,f_single,dtot3):
    
    fmin=Att.freq[0]
    fmax=Att.freq[Att.nfreq-1]
    lm = []
    lm = [fmin,fmax]
    

    stit = "Git: {:s}, Job Name: {:s}, CMP: {:s} Freq: {:5.1f} ".format(Att.progname,Att.job_name,Att.cmp,f_single)
    ndist=len(Att.dist)
    xmax= Att.dist[ndist-1]
    xmax=math.log10(xmax)
    g = ggplot()
    g=  g +  geom_line(dtot3, aes(x='x', y='y', group='TY',color='FR'),size=1.0,linetype="dotted")
    g=  g + scale_color_distiller(name='Freq  [Hz]',palette=9,type='div',limits=lm)
    g=g + xlab("Hypocentral [km]")  +  ylab("Log(Attenuation)")
    g = g + ggtitle(stit)
    g=g+theme_bw()
    print(g)

def plotAnelasticAttenuation(Att,dtot3):
    
    fmin=Att.freq[0]
    fmax=Att.freq[Att.nfreq-1]
    lm = []
    lm = [fmin,fmax]
    

    stit = "Git: {:s}, Job Name: {:s}, CMP: {:s}".format(Att.progname,Att.job_name,Att.cmp)
    ndist=len(Att.dist)
    xmax= Att.dist[ndist-1]
    xmax=math.log10(xmax)
    g = ggplot()
    g=  g +  geom_line(dtot3, aes(x='x', y='AminusGR', group='TY',color='FR'),size=1.0,linetype="dotted")
    g=  g + scale_color_distiller(name='Freq  [Hz]',palette=9,type='div',limits=lm)
    g=g + xlab("Hypocentral [km]")  +  ylab("Log(Attenuation)")
    g = g + ggtitle(stit)
    g=g+theme_bw()
    print(g)   
    
def plotAttenuationAndModel(Model,Att):
    dtot=None
    dmodel=None
    stit = "Git: {:s}, Job Name: {:s}, CMP: {:s} ".format(Att.progname,Att.job_name,Att.cmp)

    for i in range(0, len(Att.freq), 1):
        y1 = np.empty(Att.ndist, dtype=float)
        for j in range(Att.ndist):
            y1[j] = Att.att[j][i]
        fr = round(Att.freq[i], 2)
        f1 = []
        f1 = [fr for i in range(Att.ndist)]
        frr = str(fr)
        s1 = []
        s1 = [frr for i in range(Att.ndist)]
        s2 = []
        s2 = ["GIT" for i in range(Att.ndist)]
        d1 = {'x': Att.dist, 'y': y1, 'TY': s1, 'FR': f1,"Type": s2}
        d1 = pd.DataFrame(d1)
        if i == 0:
            dtot = d1
        else:
            dtot = dtot.append(d1)
        dtot = dtot.append(d1)
        del d1
    if Model != None:
        for i in range(Model.nfreq):
            y1 = np.empty(Model.ndist, dtype=float)
            for j in range(Model.ndist):
                y1[j] = Model.att[j][i]
            fr = round(Model.freq[i], 2)
            f1 = []
            f1 = [fr for i in range(Model.ndist)]
            frr = str(fr)
            s1 = []
            s1 = [frr for i in range(Model.ndist)]
            s2 = []
            s2 = ["Model" for i in range(Model.ndist)]
            d1 = {'x': Model.dist, 'y': y1, 'TY': s1, 'FR': f1,"Type": s2}
            d1 = pd.DataFrame(d1)
            if i == 0:
                dmodel = d1
            else:
                    dmodel = dmodel.append(d1)
            del d1
    v1= [0.2,2.5]
    v2= ["dotted","solid"]
    ndist=len(Att.dist)
    xmax= Att.dist[ndist-1]
    xmax=math.log10(xmax)
    g = ggplot()
    if Model != None:
        g=  g +  geom_line(dtot, aes(x='x', y='y', group='TY',color='FR',linetype="Type"),size=0.5)
        g = g +  geom_line(dmodel, aes(x='x', y='y', group='TY',color='FR',linetype="Type"),size=1.2)
        g= g + scale_color_distiller(name='Freq  [Hz]',palette=9,type='div')
        g= g + scale_linetype_manual(values= v2)
    else:
        g=  g +  geom_line(dtot, aes(x='x', y='y', group='TY',color='FR'),size=0.5,linetype="dotted")
        g= g + scale_color_distiller(name='Freq  [Hz]',palette=9,type='div')
    g=g+ xlab("Hypocentral [km]")  +  ylab("Log(Attenuation)")
    g = g + ggtitle(stit)
    g=g+theme_bw()
    print(g)

def usageGit(msg):
    print(msg)
    sys.exit()
    
def AttFittFolder(foldername):  
    isExist = os.path.exists(foldername)
    if isExist: 
        query = float(input("The folder already exists!!! Do you want to overwrite it? YES = 1; NOT = 0: "))
        if query == 1:
            shutil.rmtree(foldername)
            os.mkdir(foldername)
        else:
            foldername = str(input("Please, digit a new folder name for non-parametric fitting results: "))
            os.mkdir(foldername)  
    else:         
        os.mkdir(foldername)
        
def argumentFitSource(nameFile):  
    msg = f" Source Spectrum file: {nameFile}"
    #print(msg)
    p1 = os.path.isdir(nameFile)
    if p1:
        msg = f" FATAL ERROR, FILE: {nameFile} is a dir.. Aborting"
        #print(msg)
        sys.exit()
    p1 = os.access(nameFile, os.R_OK)
    if p1:
        pass
    else:
        msg = f" FATAL ERROR, CANNOT READ FILE: {nameFile}.. Aborting"
        #print(msg)
        sys.exit()    
    
    return {"message": msg, "file_name": nameFile}

def argumentFitAttenuation(nameFile):
    #if 2 > len(sys.argv):
    #    msg: str = "Usage: FitAttenuation.py AttenuationFile "
    #    usageGit(msg)
    #else:
    #    pass
    #nameFile = sys.argv[1]
    msg = f" Attenuation file: {nameFile}"
    #print(msg)
    p1 = os.path.isdir(nameFile)
    if p1:
        msg = f" FATAL ERROR, FILE: {nameFile} is a dir.. Aborting"
        #print(msg)
        sys.exit()
    p1 = os.access(nameFile, os.R_OK)
    if p1:
        pass
    else:
        msg = f" FATAL ERROR, CANNOT READ FILE: {nameFile}.. Aborting"
        #print(msg)
        sys.exit()   
        
    return {"message": msg, "file_name": nameFile}

def valid_true_false(arg):
    """ 
    Check if the argument is true or false
    Args:
        arg(str)
    Returns:
        bool True or False
    """
    if arg.lower() == 'true': return True
    elif arg.lower() == 'false': return False
    else:
        raise argparse.ArgumentTypeError("invalid choice: '" + str(arg) + "' (choose from 'true' or 'false')")

def FitAttenuationResDict():
    dictionary = {'Q0': "Quality factor at 1 Hz related to the linear fitting",\
                'N': "Exponent value for Q(f)=Q0*f^N related to the linear fitting",\
                'err_Q0': "Standard deviation for Q0",\
                'err_N': "Standard deviation for N",\
                'Q01':"Quality factor for f[Hz]<=HF1 (Q(f) tri-linear fitting)",\
                'err_Q01': "Standard deviation for Q01",\
                'N1':"Exponent value for Q(f)=Q01*f^N1 for f[Hz]<=HF1 (Q(f) tri-linear fitting)",\
                'err_N1': "Standard deviation for N1",\
                'Q02':"Quality factor for HF1<f[Hz]<HF2 (tri-linear fitting)",\
                'err_Q02': "Standard deviation for Q02",\
                'N2':"Exponent value for Q(f)=Q02*f^N2 for HF1<f[Hz]<HF2 (Q(f) tri-linear fitting)",\
                'err_N2': "Standard deviation for N2",\
                'Q03':"Quality factor for f[Hz]>=HF2 (tri-linear fitting)",\
                'err_Q03': "Standard deviation for Q03",\
                'N3':"Exponent value for Q(f)=Q03*f^N3 for f[Hz]>=HF2 (Q(f) tri-linear fitting)",\
                'err_N3': "Standard deviation for N3",\
                'HF1':"First Hinge Frequency (Q(f) tri-linear fitting)",\
                'HF2':"Second Hinge Frequency (Q(f) tri-linear fitting)",\
                'kappa_mean': "kappa value for path term",\
                'kappa_std': "standard deviation of the kappa mean",\
                'corr_fact_flag': "flag to correct the non-parametric attenuation ",\
                'Q0_corr': "flag to fit the Q(f): 0) linear fitting; 1) trilinear fitting with hinge frequencies HF1 and HF2",\
                'Vs': "Share wave velocity [km/s]",\
                'dist_min_Q': "lower bound of the hypocentral distance interval used to fit the quality factor at each frequency",\
                'dist_max_Q': "upper bound of the hypocentral distance interval used to fit the quality factor at each frequency",\
                'Rref': "Reference Distance [km] used in the Generalized Inversion",\
                'n1': "Exponent value for (R/Rref)^n1 ",\
                'n2': "Exponent value for (R/H1)^n1*(H1/Rref)^n2 ",\
                'n3': "Exponent value for (R/H1)^n1*(H1/H2)^n2*(H2/Rref)^n3 ",\
                'err_n1':"standard deviation for n1",\
                'err_n2':"standard deviation for n2",\
                'err_n3':"standard deviation for n3",\
                'h':"Hinge distance [km] for bilinear model",\
                'Hinge Distance':"Hinge distance [km] for bilinear model",\
                'HD1':"First Hinge Distance [km]",\
                'HD2':"Second Hinge Distance [km]",\
                'HD3':"Third Hinge Distance [km]",\
                'model': "Type fitting for Geometrical Spreading es: Rref/R, bilinear, tri-linear, 4-linear"   }
    return dictionary

def get_site_val(cfg, freq):
    crust = 0
    refval = 0
    v1 = -1 * math.pi * cfg.knot / math.log(10)
    val = math.isnan(cfg.Fk)
    if not val:
        if freq > val:
            val = math.isnan(cfg.knot)
            if not val:
                slopeK = v1
            else:
                slopeK = 0
        else:
            slopeK = 0
    else:
        val = math.isnan(cfg.knot)
        if not val:
            slopeK = v1
        else:
            slopeK = 0
    if cfg.n_freq_crust > 1:
        n = cfg.n_freq_crust - 1
        if freq < cfg.freq_crust[0]:
            pp = cfg.amp_crust[0]
        elif freq > cfg.freq_crust[n]:
            pp = cfg.amp_crust[n]
        else:
            pp = cfg.spline_crust(freq)
        crust = math.log10(pp)
    refval = crust + slopeK * freq
    return refval

def check_log (logFile,configFile):
    val = os.path.isfile(logFile)
    if (val):
        write = os.access(logFile, os.W_OK)
        if write == False:
            msg = f" Cannot write Log File {logFile} \n Aborting"
            print(msg)
            sys.exit()
        else:
            msg = f" Warning Log File {logFile} exist !"
            print(msg)
    else:
        pa = os.path.dirname(logFile)
        write = os.access(pa, os.W_OK)
        if write == False:
            msg = f" Cannot write File {logFile} \n Aborting"
            print(msg)
            sys.exit()
    confF = os.path.abspath(configFile)
    confL = os.path.abspath(logFile)
    return confF,confL

def check_Mw(ConfigFile,opts,logID):
    EvIDMw = []
    MW = []
    fixflag = []
    if ConfigFile.anchorage_mw ==1:
        if ConfigFile.magnitude_file == None:
            msg = f" Magnitude File not found.  Aborting"
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")
            sys.exit(1)    
        if Path(ConfigFile.magnitude_file).suffix != '.txt':
            msg = f" Config File {ConfigFile.magnitude_file} not txt extension\n Aborting"
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")
            sys.exit(1)
        try:
            with open(ConfigFile.magnitude_file) as f:
                lines = f.readlines()[1:]
                EvIDMw = []
                MW = []
                fixflag = []
                for row in lines:
                    row = row.rstrip('\n')
                    row = row.split(';')
                    EvIDMw.append(row[0])
                    MW.append(row[1])
                    fixflag.append(row[2])
            f.close()

        except FileNotFoundError:
            msg = f"File {ConfigFile.magnitude_file} not found. Aborting!"
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")        
            sys.exit(1)
        except OSError:
            msg = f"OS error occurred trying to open {ConfigFile.magnitude_file}"
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")        
            sys.exit(1)
        except Exception as err:
            msg = f"Unexpected error opening {ConfigFile.magnitude_file} is", repr(err)
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")
            sys.exit(1)
    return EvIDMw,MW,fixflag

def check_f_range(ConfigFile,opts,logID):
    if Path(ConfigFile.f_range_file_name).suffix != '.txt':
        msg = f" Config File {ConfigFile.f_range_file_name} not txt extension\n Aborting"
        if opts.verb == True: print(msg)
        logID.write("\n")
        logID.write(msg)
        logID.write("\n")
        sys.exit(1)
    try:
        with open(ConfigFile.f_range_file_name) as f:
            lines = f.readlines()[1:]
            MMIN = []
            MMAX = []
            FMIN = []
            FMAX = []
            for row in lines:
                row = row.rstrip('\n')
                row = row.split(';')
                if len(row) < 5:
                    break
                MMIN.append(row[1])
                MMAX.append(row[2])
                FMIN.append(row[3])
                FMAX.append(row[4])
        f.close()

    except FileNotFoundError:
        msg = f"File {ConfigFile.f_range_file_name} not found.  Aborting"
        if opts.verb == True: print(msg)
        logID.write("\n")
        logID.write(msg)
        logID.write("\n")        
        sys.exit(1)
    except OSError:
        msg = f"OS error occurred trying to open {ConfigFile.f_range_file_name}"
        if opts.verb == True: print(msg)
        logID.write("\n")
        logID.write(msg)
        logID.write("\n")        
        sys.exit(1)
    except Exception as err:
        msg = f"Unexpected error opening {ConfigFile.f_range_file_name} is", repr(err)
        if opts.verb == True: print(msg)
        logID.write("\n")
        logID.write(msg)
        logID.write("\n")
        sys.exit(1)        
    
    return MMIN,MMAX,FMIN,FMAX 
   
def check_1D_model(ConfigFile,opts,logID):
    if ConfigFile.crustal_model == None:
        msg = f" Config File not found.  Aborting"
        if opts.verb == True: print(msg)
        logID.write("\n")
        logID.write(msg)
        logID.write("\n")
        sys.exit(1)        
    if Path(ConfigFile.crustal_model).suffix != '.txt':
        msg = f" Config File {ConfigFile.crustal_model} not txt extension\n Aborting"
        if opts.verb == True: print(msg)
        logID.write("\n")
        logID.write(msg)
        logID.write("\n")
        sys.exit(1)
    try:
        with open(ConfigFile.crustal_model) as f:
            lines = f.readlines()[1:]
            DEPTH = []
            VP = []
            VS = []
            RHO = []
            
            for row in lines:
                row = row.rstrip('\n')
                row = row.split(';')
                DEPTH.append(row[1])
                VP.append(row[2])
                VS.append(row[3])
                RHO.append(row[4])
                
        f.close()

    except FileNotFoundError:
        msg = f"File {ConfigFile.crustal_model} not found.  Aborting"
        if opts.verb == True: print(msg)
        logID.write("\n")
        logID.write(msg)
        logID.write("\n")        
        sys.exit(1)
    except OSError:
        msg = f"OS error occurred trying to open {ConfigFile.crustal_model}"
        if opts.verb == True: print(msg)
        logID.write("\n")
        logID.write(msg)
        logID.write("\n")        
        sys.exit(1)
    except Exception as err:
        msg = f"Unexpected error opening {ConfigFile.crustal_model} is", repr(err)
        if opts.verb == True: print(msg)
        logID.write("\n")
        logID.write(msg)
        logID.write("\n")
        sys.exit(1)
    return DEPTH,VP,VS,RHO

def convert_strings_to_floats(input_array): 
    output_array = []
    for element in input_array:
        converted_float = float(element)
        output_array.append(converted_float)
    return output_array

def fun_kappa(x,a,b):
    y = a-np.log10(np.exp(1))*np.pi*b*x
    return y

# Define the fitting function
def power_fitting_lmfit(params,x,y):
    beta1 = params['M0']
    beta2 = params['fc']
    #y_fit = a* x **b
    y_fit = np.log10(beta1) + np.log10(np.power(beta2,2)/(np.power(beta2,2)+np.power(x,2)))
    return y_fit-y

def fun_spectral_fit(x,beta1,beta2):
    y = np.log10(beta1) + np.log10(np.power(beta2,2)/(np.power(beta2,2)+np.power(x,2)))
    return y  

def linfun(x,a,b):
    y = a + b*x
    return y

def fmintofit (df,treshold):
    y_spl = UnivariateSpline(df['Frequency [Hz]'].tolist(),df['Displacement Spectrum [m*s]'].tolist(),s=0,k=4)
    spline = y_spl(df['Frequency [Hz]'])
    df['spline'] = spline
    y_spl_2d = y_spl.derivative(n=1)
    df['1dspline'] = y_spl_2d(df['Frequency [Hz]'].tolist())
    y_spl2 = UnivariateSpline(df['Frequency [Hz]'].tolist(),df['1dspline'].tolist(),s=0,k=4)
    y_spl2_2d = y_spl2.derivative(n=1)
    df['1dspline2'] = np.abs(y_spl2_2d(df['Frequency [Hz]'].tolist()))
    fmin = df.loc[df['1dspline2'].round(0) > treshold]['Frequency [Hz]'].tolist()[-1]
    return fmin  

def calc_stress_drop_line (Vs,M0_Nm,stress_drop):
    fc = np.array([])
    #stress_drop in list_dsigma:
    dsigma_bar = 10*stress_drop
    M0_dyne_cm = M0_Nm*pow(10,7)
    fc = np.concatenate((fc,4.9*pow(10,6)*Vs*pow(dsigma_bar/M0_dyne_cm,1/3)))
    #log10_M0_Nm = np.log10(M0_Nm)
    return fc

def plot_fc_vs_M0(dfresult,a,b,flag_print,flag_save,Vs,outdir,type):
    dfresult['log10(M0) [Nm]'] = np.log10(dfresult['M0 [Nm]'])
    dfresult['log10(M0 + M0_err) [Nm]'] = np.log10(dfresult['M0 [Nm]']+dfresult['M0_err'])
    M0min = np.log10(np.min(np.array(dfresult['M0 [Nm]'].tolist())))
    M0max = np.log10(np.max(np.array(dfresult['M0 [Nm]'].tolist())))
    expo = linfun(np.arange(M0min,M0max,(M0max-M0min)/len(dfresult['M0 [Nm]'])),a,b)
    if len(expo) - len(dfresult['M0 [Nm]']) == 0:
        fc_vs_M0 = pow(10,expo)
    else:
        valore = len(expo) - len(dfresult['M0 [Nm]'])
        fc_vs_M0 = pow(10,expo[0:-valore])
    dfresult['fcvsM0 [Hz]'] = fc_vs_M0
    
    M0_range = np.arange(M0min,M0max,(M0max-M0min)/len(dfresult['M0 [Nm]']))
    M0_range_ = np.rint(np.arange(M0min,M0max,1))
    if len(expo) - len(dfresult['M0 [Nm]']) == 0:
        dfresult['M0_range [Nm]'] = M0_range
    else:
        M0_range = M0_range[0:-(len(expo) - len(dfresult['M0 [Nm]']))]
        dfresult['M0_range [Nm]'] = M0_range
        
    list_dsigma = [0.1, 1.0, 10]    

    count = 1
    for sd in list_dsigma:
        fc = calc_stress_drop_line(Vs,pow(10,M0_range),sd)
        dfresult['fc_sd_' + str(count)] = fc
        count = count + 1 

    g = ggplot(dfresult, aes(x = 'log10(M0) [Nm]',y='fc [Hz]'))    
    g = g + geom_errorbar(aes(x='log10(M0) [Nm]', ymin=dfresult['fc [Hz]'] - dfresult['fc_error'],ymax=dfresult['fc [Hz]'] + dfresult['fc_error']), width=.1)
    g = g + geom_errorbarh(aes(y='fc [Hz]', xmin=np.log10(dfresult['M0 [Nm]'] - dfresult['M0_err']),xmax=np.log10(dfresult['M0 [Nm]'] + dfresult['M0_err'])), height = .1)

    g = g + geom_point(fill='grey', size=2)
    
    g = g + geom_line(dfresult, aes(x = 'M0_range [Nm]',y='fcvsM0 [Hz]'), size=2, color='r')

    if 'fc_sd_1' in dfresult.columns.values.tolist(): g = g + geom_line(dfresult, aes(x = 'M0_range [Nm]',y='fc_sd_1'), size=1, linetype='dashed',color='b') + annotate(geom="label",x=dfresult['M0_range [Nm]'].tolist()[10], y=dfresult['fc_sd_1'].tolist()[10], label=str(list_dsigma[0]) + 'MPa', fill = 'white',size=9, colour='blue')
    if 'fc_sd_2' in dfresult.columns.values.tolist(): g = g + geom_line(dfresult, aes(x = 'M0_range [Nm]',y='fc_sd_2'), size=1, linetype='dashed', color='b') + annotate(geom="label",x=dfresult['M0_range [Nm]'].tolist()[10], y=dfresult['fc_sd_2'].tolist()[10], label=str(list_dsigma[1]) + 'MPa', fill = 'white',size=9, colour='blue')
    if 'fc_sd_3' in dfresult.columns.values.tolist(): g = g + geom_line(dfresult, aes(x = 'M0_range [Nm]',y='fc_sd_3'), size=1, linetype='dashed', color='b') + annotate(geom="label",x=dfresult['M0_range [Nm]'].tolist()[10], y=dfresult['fc_sd_3'].tolist()[10], label=str(list_dsigma[2]) + 'MPa', fill = 'white',size=9, colour='blue')
    if 'fc_sd_4' in dfresult.columns.values.tolist(): g = g + geom_line(dfresult, aes(x = 'M0_range [Nm]',y='fc_sd_4'), size=1, linetype='dashed', color='b') + annotate(geom="label",x=dfresult['M0_range [Nm]'].tolist()[10], y=dfresult['fc_sd_4'].tolist()[10], label=str(list_dsigma[3]) +'MPa', fill = 'white',size=9, colour='blue')
    if 'fc_sd_5' in dfresult.columns.values.tolist(): g = g + geom_line(dfresult, aes(x = 'M0_range [Nm]',y='fc_sd_5'), size=1, linetype='dashed', color='b')+ annotate(geom="label",x=dfresult['M0_range [Nm]'].tolist()[10], y=dfresult['fc_sd_5'].tolist()[10], label=str(list_dsigma[4]) + 'MPa', fill = 'white',size=9, colour='blue')
    
    g = g + scale_x_continuous(breaks = M0_range_.tolist())
    g = g + scale_y_log10()
    g = g + ggtitle('fc = ' + str('{:.3f}'.format(a)) + str('{:.3f}'.format(b)) + '*log10(M0)')
    g = g + theme_bw()
    if flag_print: print(g)
    if not os.path.exists(outdir + '/fitmod' + str(type) + '/PLOT/'):
        os.makedirs(outdir + '/fitmod' + str(type) + '/PLOT/')
    if flag_save: g.save(filename=outdir + '/fitmod' + str(type) +  '/PLOT/' + 'fc_vs_M0.jpg', dpi=300) 
    
def plot_ksource_distrib(dfresult,k_mean,k_std,flag_print,flag_save,outdir,sourcetype):    
    g = ggplot(dfresult, aes(x='kappa [s]')) + geom_histogram(color = "black", fill = "gray")
    g = g+geom_vline(aes(xintercept=k_mean),size=2,color = "red")
    g = g + ggtitle ('ksource =' + '{:.3f}'.format(k_mean) + '±' + '{:.3f}'.format(k_std) + ' [s]')
    if flag_print: print(g)
    if not os.path.exists(outdir + '/fitmod' + str(sourcetype) + '/PLOT/'):
        os.makedirs(outdir + '/fitmod' + str(sourcetype) + '/PLOT/')
    if flag_save: g.save(filename=outdir + '/fitmod' + str(sourcetype) + '/PLOT/' + 'ksource_distribution.jpg', dpi=300)
    
def plot_KappaCorrSourceSpe (df,SAfit,eveid,flag_print,flag_save,outdir):
    g = ggplot(df, aes(x='Frequency [Hz]', y='Acceleration Spectrum [m/s]')) + geom_line(size=0.2)
    g = g + geom_line(SAfit,aes(x='Frequency [Hz]', y='AmplitudeFit'),size=1.0 ,color="red")
    g = g + geom_line(SAfit,aes(x='Frequency [Hz]', y='AmpAccCorrK'),size=1.0 ,color="blue")
    g = g + geom_line(SAfit,aes(x='Frequency [Hz]', y='AmpCorrk_offset'),size=1.0)
    g = g + geom_line(df,aes(x='Frequency [Hz]', y='AmpAccCorrK'),size=2.0)
    if flag_print: print(g)    
    if not os.path.exists(outdir + '/PLOT/'):
        os.makedirs(outdir + '/PLOT/')
    if flag_save: g.save(filename=outdir + '/PLOT/' + outdir + '/PLOT/' + eveid + '_KappaCorrSourceSpe', dpi=300)     

def plot_M_vs_Mw(dfresult,flag_print,flag_save,outdir,type):
        g = ggplot(dfresult, aes(x = 'M',y='Mw')) + geom_point(fill='grey', size=2)
        g = g + geom_line(dfresult, aes(x = 'M',y='M'), size=2, color='r')
        g = g + theme_bw()
        if flag_print: print(g)
        if not os.path.exists(outdir + '/fitmod' + str(type) + '/PLOT/'):
            os.makedirs(outdir + '/fitmod' + str(type) + '/PLOT/')
        if flag_save: g.save(filename=outdir + '/fitmod' + str(type) +  '/PLOT/' + 'Mw_vs_M.jpg', dpi=300)

      
            
def plot_fmin (df,eveid,fmin,flag_print,flag_save):
    g = ggplot(df, aes(x='Frequency [Hz]', y='1dspline')) + geom_point(size=1.0, color='r')
    g = g  + geom_point(df,aes(x='Frequency [Hz]', y='1dspline2'),size=1.0)
    g = g + geom_vline(xintercept = fmin)
    if flag_print: print(g)
    if flag_save: g.save(filename=eveid + '_fmin', dpi=300)
    
def readingfas(idir):
    list_dir = glob.glob(idir + "/*.fas")
    print('Reading FAS files in: ',str(idir), ' directory....')
    count = 0
    for nameFile in list_dir:
        print(nameFile)
        data = pd.read_table(nameFile, header=0, delim_whitespace=True)
        data = pd.DataFrame(data)
        if len(data) == 0:
            msg = f" ERROR FAS File: {nameFile} Ndat=0. Aborting"
            print(msg)
            sys.exit()
        if count==0:
            fas = data
        else:
            fas = fas._append(data)
        count = count +1      
    del (count)
    return fas

def statall(fas):   
    try:
        fas[["Id0", "Ml"]].describe()      
        stat = fas.agg({"Ml": ["min", "max", "median", "mean","std","skew"],"Dpt": ["min", "max", "median", "mean","std","skew"],
             "Depi": ["min", "max", "median", "mean","std","skew"],"Azim": ["min", "max", "median", "mean","std","skew"],"Freq": ["min", "max"]}) 
   
    except:
        fas[["Id", "Ml"]].describe()
        stat = fas.agg({"Ml": ["min", "max", "median", "mean","std","skew"],"EvtDpt": ["min", "max", "median", "mean","std","skew"],
             "Depi": ["min", "max", "median", "mean","std","skew"],"Azim": ["min", "max", "median", "mean","std","skew"],"Freq": ["min", "max"]}) 
   
    return stat

def fassta(fas):    
    fas_sta = fas.drop_duplicates(['FDSN'])
    fas_sta = fas_sta.rename(columns={"StLat": "Latitude [°]", "StLon": "Longitude [°]"})
    return fas_sta 