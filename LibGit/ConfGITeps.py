#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
import math
import os
import sys
import numpy as np
from typing import List
from yaml.loader import SafeLoader
from scipy.interpolate import UnivariateSpline
from pathlib import Path
from LibGit.utilities import readingfas,statall,fassta

class Conf:
    
    amp_crust: List[float]

    def ReadFileCrust(self):
        self.freq_crust = list()
        self.amp_crust = list()
        try:
            cru = open(self.FileCrust, 'rb')
        except FileNotFoundError:
            print(f"FileCrust {self.FileCrust} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {self.FileCrust}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {self.FileCrust} is", repr(err))
            sys.exit(1)
        self.n_freq_crust = 0
        Lines = cru.readlines()
        ncount = 0
        for line in Lines:
            ncount += 1
            line = line.rstrip().decode()
            p1 = line.split()
            self.amp_crust.append(float(p1[1]))
            self.freq_crust.append(float(p1[0]))
        freq = np.asarray(self.freq_crust)
        amp = np.asarray(self.amp_crust)
        self.n_freq_crust = ncount
        self.spline_crust = UnivariateSpline(freq, amp)
        cru.close()

    def ReadDistFile(self):
        distl = list()
        self.dist_step = 0
        try:
            dist = open(self.FileDist, 'rb')
        except FileNotFoundError:
            print(f"File {self.FileDist} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {self.FileDist}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {self.FileDist} is", repr(err))
            sys.exit(1)
        self.n_freq_crust = 0
        Lines = dist.readlines()
        ncount = 0
        for line in Lines:
            ncount += 1
            line = line.rstrip().decode()
            p1 = line.split()
            distl.append(float(p1[0]))
        self.dist_bin = np.asarray(distl)
        self.nbin_dist = len(self.dist_bin)
        self.dist_max = self.dist_bin[self.nbin_dist - 1]
        self.dist_min = self.dist_bin[0]
        s1 = "  Dist Min: {:7.1f}, Dist Max: {:7.1f}, N bin dist: {:6d} ".format(self.dist_min,self.dist_max,self.nbin_dist)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        dist.close()

    def ReadEveRedFile(self):
        evel = list()
        try:
            event = open(self.eve_red, 'rb')
        except FileNotFoundError:
            print(f"File {self.eve_red} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {self.eve_red}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {self.eve_red} is", repr(err))
            sys.exit(1)
        self.n_eve = 0
        Lines = event.readlines()[1:]
        ncount = 0
        for line in Lines:
            ncount += 1
            line = line.rstrip().decode()
            p1 = line.split(';')
            evel.append(p1)
        self.list_eve = np.asarray(evel)

        s1 = "  Number of Removed Events: {:7d} ".format(len(self.list_eve))
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        event.close()

    def ReadStaRedFile(self):
        stal = list()
        try:
            station = open(self.sta_red, 'rb')
        except FileNotFoundError:
            print(f"File {self.sta_red} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {self.sta_red}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {self.sta_red} is", repr(err))
            sys.exit(1)
        Lines = station.readlines()[1:]
        ncount = 0
        for line in Lines:
            ncount += 1
            line = line.rstrip().decode()
            p1 = line.split(';')
            #p1 = p1[1]
            stal.append(p1)
        self.list_sta = np.asarray(stal)

        s1 = "  Number of Removed Stations: {:7d} ".format(len(self.list_sta))
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        station.close()
                
    def __init__(self,fname,logf):
        self.spline_crust = None
        self.n_freq_crust = 0
        self.ConfName = fname
        self.log = logf
        self.nrow_min = 100
        self.damp = 1e-10
        self.atol = 1e-08
        self.btol = 1e-08
        self.iter_lim = 1000
        self.knot = float('nan')
        self.weight_smo = 20
        self.freq_crust = list()
        self.amp_crust = list()
        self.Fk = float('nan')
        self.scale = 1
        self.job_name = None
        self.eve_red = list()
        self.sta_red = list()
        self.GUI = 'Agg'
        
        if Path(fname).suffix != '.yaml':
            msg = f" Config File {fname} not yaml extension\n Aborting"
            print(msg)
            sys.exit(1)
        try:
            with open(fname) as f:
                   data = yaml.load(f, Loader=SafeLoader)
        except FileNotFoundError:
            print(f"File {fname} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {fname}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {fname} is", repr(err))
            sys.exit(1)
        s1 = "....... START reading Ymal Configuration File: {:s} ".format(fname)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        s1 = "-- Section GLOBAL --"
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        self.job_name = data["GLOBAL"]["job_name"]
        s1 = "> Job Name: {:s} ".format(self.job_name)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        self.fas_dir = data["GLOBAL"]["fas_dir"]
        s1 = "> FAS  dir: {:s} ".format(self.fas_dir)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        val = os.path.isdir(self.fas_dir)
        if val == False:
          s1 = "> WARNING No such file or directory: {:s} ".format(self.fas_dir)
          print(s1)  
          self.log.write(s1)
          self.log.write("\n")
          sys.exit()
        self.out_dir = data["GLOBAL"]["out_dir"]
        s1 = "> OUT  dir: {:s} ".format(self.out_dir)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        val = os.path.isdir(self.out_dir)
        if val:
            s1 = "> WARNING Out Dir:  {:s} Exist! ".format(self.out_dir)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            write = os.access(self.out_dir, os.W_OK)
            if write == False:
                msg = f" Cannot write Out Dir {self.out_dir} \n Aborting"
                self.log.write(msg)
                self.log.write("\n")
                print(msg)
                sys.exit()
        else:
            os.mkdir(self.out_dir)
            flag = os.access(self.out_dir, os.W_OK)
            if not flag:
                msg = f" Cannot create Out Dir {self.out_dir} \n Aborting"
                self.log.write(msg)
                self.log.write("\n")
                print(msg)
                sys.exit()
            else:
                s1 = " Succesfully created Out Dir:  {:s} ".format(self.out_dir)
                self.log.write(s1)
                self.log.write("\n")
                print(s1)
        
        self.GUI = data["GLOBAL"]["gui_env"]
        s1 = "> GUI: " + str(self.GUI)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        
        self.plot_graph = False
        self.plot_graph = str(data["GLOBAL"]["plot_graph"])
        self.plot_graph = self.plot_graph.upper()
        if self.plot_graph != 'TRUE' and self.plot_graph != 'FALSE':            
            s1 = "  WARNING INVALID plot_graph: {:s} (options: TRUE or FALSE') reset to default falue: FALSE ? (Y/N)".format(
                str(self.plot_graph))
            query_plot = input(s1)
            if query_plot == 'Y':
                s1 = "> Plot_graph: {:s} ".format(str(self.plot_graph))
                self.log.write(s1)
                self.log.write("\n")
                self.plot_graph = False
                print(s1)
            else:
                sys.exit()
        else:
            s1 = "> Plot_graph: {:s} ".format(str(self.plot_graph))
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        if self.plot_graph == 'FALSE':
            self.plot_graph = False
        if self.plot_graph == 'TRUE':
            self.plot_graph = True
        self.save_graph = False
        self.save_graph = str(data["GLOBAL"]["save_graph"])
        self.save_graph = self.save_graph.upper()
        if self.save_graph != 'TRUE' and self.save_graph != 'FALSE':
            s1 = "  WARNING INVALID save_graph: {:s} (options: TRUE or FALSE') reset to default falue: FALSE ? (Y/N)".format(
                str(self.save_graph))
            query_save = input(s1)
            if query_save == 'Y':
                self.save_graph = False
                s1 = "> Save_graph: {:s} ".format(str(self.save_graph))
                self.log.write(s1)
                self.log.write("\n")
                print(s1)
            else:
                sys.exit()
        else:
            s1 = "> Save_graph: {:s} ".format(str(self.save_graph))
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        if self.save_graph == 'FALSE':
            self.save_graph = False
        if self.save_graph == 'TRUE':
            self.save_graph = True
        self.cmp = str(data["GLOBAL"]["cmp"])
        if self.cmp != 'H' and self.cmp != 'NS' and self.cmp != 'EW' and self.cmp != 'Z':
            s1 = "  WARNING INVALID cmp:   {:s} (options: H,NS,EW,Z) do you want to reset to default cmp  H (Y/N) ?".format(self.cmp)
            query_comp = input(s1)
            if query_comp == 'Y':
                self.cmp = 'H'
            else:
                print("Aborting")
                sys.exit()
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        s1 = "> Cmp: {:s} ".format(self.cmp)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        self.mean =  str(data["GLOBAL"]["mean"])
        if self.mean != 'VECTORIAL' and self.mean != 'GEOM':
            s1 = "  WARNING INVALID mean:  {:s} (options: VECTORIAL or GEOM) reset to default mean  VECTORIAL ? (Y/N) ".format(
               self.cmp)
            query_compcomp = input(s1)
            if query_compcomp == 'Y':
                self.mean = 'VECTORIAL'
                s1 = "> Mean: {:s} ".format(self.mean)
                self.log.write(s1)
                self.log.write("\n")
                print(s1)
            else:
                print("Aborting")
                sys.exit()       
        s1 = "> Mean: {:s} ".format(self.mean)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        try:
            self.fmin = float(data["GLOBAL"]["fmin"])
        except Exception as e:
            print('ERROR: check fmin in the configuration file: ' + str(e) +  "\n Aborting" )
            sys.exit()
        if self.fmin >= 0:
            s1 = "> Fmin:  {:6.2f} Hz".format(self.fmin)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        else:
            s1 = "> WARNING! Fmin must be a positive number, check the configuration file  \n Aborting "
            print(s1)
            sys.exit()
        try:
            self.fmax = float(data["GLOBAL"]["fmax"])
        except Exception as e:
            print('ERROR: check fmax in the configuration file: ' + str(e) +  "\n Aborting" )
            sys.exit()
        if self.fmax >=0:
            s1 = "> Fmax:  {:6.2f} Hz".format(self.fmax)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        else:
            s1 = "> WARNING! Fmax must be a positive number, check the configuration file \n Aborting "
            print(s1)
            sys.exit()
        try:
            self.mmin = float(data["GLOBAL"]["mmin"])
        except Exception as e:
            print('ERROR: check fmax in the configuration file: ' + str(e) + "\n Aborting" )
            sys.exit()
        if self.mmin >=0:
            s1 = "> Min Mag:  {:6.2f}".format(self.mmin)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        else:
            print("> WARNING! mmin must be a positive number, check the configuration file \n Aborting ")
            sys.exit()
        try:
            self.mmax = float(data["GLOBAL"]["mmax"])
        except Exception as e:
            print('ERROR: check mmax in the configuration file: ' + str(e) + "\n Aborting" )
            sys.exit()
        if self.mmax >=0:
            s1 = "> Max Mag:  {:6.2f}".format(self.mmax)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        else:
            print("> WARNING! mmax must be a positive number, check the configuration file \n Aborting ")
            sys.exit()
        if self.mmax < self.mmin:
            print("ERROR! mmin must be < mmax, check the configuration file \n Aborting ")
            sys.exit()
        s1 = "-- Section DIST BIN --"
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        self.type_bin =  str(data["DIST BIN"]["type_bin"])
        if self.type_bin != 'LIN' and self.type_bin != 'LOG' and self.type_bin != 'FILE':
            s1 = "  Error INVALID type_bin:  {:s} (options: LIN or LOG or FILE \n Aborting ".format(self.type_bin)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            sys.exit(1)
        s1 = "> Type_bin:  {:s} ".format(self.type_bin)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        if self.type_bin == 'LIN' or self.type_bin == 'LOG':
            try:
                self.dist_min =  float(data["DIST BIN"]["dist_min"])
            except Exception as e:
                print('ERROR: check dist_min in the configuration file: ' + str(e) + "\n Aborting" )
                sys.exit()
            if self.dist_min >=0:
                s1 = "> Dist_min:  {:7.2f} km".format(self.dist_min)
                self.log.write(s1)
                self.log.write("\n")
                print(s1)
            else:
                s1 = "ERROR! Dist_min must be >=0, check the configuration file \n Aborting"
                print(s1)
                sys.exit()
            try:
                self.dist_max =  float(data["DIST BIN"]["dist_max"])
            except Exception as e:
                print('ERROR: check dist_max in the configuration file: ' + str(e) + "\n Aborting" )
                sys.exit()
            if self.dist_max >=0:
                s1 = "> Dist_max:  {:7.2f} km".format(self.dist_max)
                self.log.write(s1)
                self.log.write("\n")
                print(s1)
            else:
                s1 = "ERROR! Dist_max must be >=0, check the configuration file \n Aborting"
                sys.exit()
        if self.type_bin != 'FILE':
            if self.dist_max <= self.dist_min:
                s1 = "ERROR! Dist_max must be > Dist_min, check the configuration file \n Aborting"
                print(s1)
                sys.exit()
        if self.type_bin == 'LIN':
            self.dist_step =  float(data["DIST BIN"]["dist_step"])
            s1 = "> Dist_step: {:7.2f} km".format(self.dist_step)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            self.dist_bin = list(np.arange(self.dist_min, self.dist_max, self.dist_step))
            self.dist_bin = np.array(self.dist_bin)
            self.nbin_dist = len(self.dist_bin)
            s1 = "> N bin dist (LIN): {:d} ".format(self.nbin_dist)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        if self.type_bin == 'LOG':
            self.nbin_dist = int(data["DIST BIN"]["nbin_dist"])
            s1 = "> N bin dist: (LOG) {:d} ".format(self.nbin_dist)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            if self.dist_min==0: self.dist_min=0.01
            v1 = math.log10(self.dist_min)
            v2 = math.log10(self.dist_max)
            if float(self.nbin_dist) == 0:
                msg = "ERROR! check the configuration file: nbin_dist > 0"
                print()
                print(msg)
                print()
                sys.exit(1)
            self.dist_step = (v2 - v1) / float(self.nbin_dist)
            self.dist_bin = list(np.arange(v1, v2, self.dist_step))
            self.dist_bin = np.array(self.dist_bin)
            self.dist_bin = 10 ** self.dist_bin
        if self.type_bin == 'FILE':
            s1 = str(data["DIST BIN"]["dist_file"])
            self.FileDist = s1
            s1 = "> FileDist :  {:s} ".format(self.FileDist)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            self.ReadDistFile()
        s1 = "-- Section SUBSET --"
        self.log.write(s1)
        self.log.write("\n")
        self.type_sel =  str(data["SUBSET"]["type_sel"])
        if self.type_sel == 'FILE':
            s1 = str(data["SUBSET"]["event_remove"])
            self.eve_red = s1
            s1 = "> FileEventRed :  {:s} ".format(self.eve_red)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            if self.eve_red != 'NONE':
                self.ReadEveRedFile()        
            s1 = str(data["SUBSET"]["station_remove"])
            self.sta_red = s1
            s1 = "> FileStationRed :  {:s} ".format(self.sta_red)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)                    
            if self.sta_red != 'NONE':
                self.ReadStaRedFile()

        elif self.type_sel == 'NONE':
            s1 = "> FileEventRed :  no events excluded from the inversion "
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            s1 = "> FileStationRed : no stations excluded from the inversion "
            self.log.write(s1)
            self.log.write("\n")
            print(s1)                    

        else:
            s1 = "  Error INVALID type_sel:  {:s} \n Aborting ".format(self.type_sel)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
            sys.exit(1)        
        s1 = "> Type_sel:  {:s} ".format(self.type_sel)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
                            
        s1 = "-- Section CONSTRAINT DREF --"
        self.log.write(s1)
        self.log.write("\n")
        self.dref =  float(data["CONSTRAINT DREF"]["dref"])
        s1 = "> Reference distance: {:4.1f} km ".format(self.dref)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        if len(self.dist_bin) <= 10:
            s1 = " The number of distance bin must be > 10, check the configuration file"
            print(s1)
            sys.exit()
        if self.dref < 0 or self.dref < self.dist_bin[0] or self.dref >= self.dist_bin[self.nbin_dist - 1]:
            msg = " Dref must be > {:6.1f} km and < {:6.1f} km Aborting !".format(self.dist_bin[0],self.dist_bin[-1])
            print(msg)
            sys.exit()
        try:
            self.weight_dref = float(data["CONSTRAINT DREF"]["weight_dref"])
        except Exception as e:
            print('ERROR: check weight_dref in the configuration file: ' + str(e) + "\n Aborting" )
            sys.exit()
        if self.weight_dref > 0:
            s1 = "> Weight_dref:  {:3.0f} ".format(self.weight_dref)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        else:
            s1 = " Weight_dref must be > 10, check the configuration file"
            print(s1)
            sys.exit()
        try:
            self.vdref = float(data["CONSTRAINT DREF"]["vdref"])
        except Exception as e:
            print('ERROR: check vdref in the configuration file: ' + str(e) + "\n Aborting" )
            sys.exit()        
        if self.vdref > 0:
            self.vdref = math.log10(self.vdref)
            s1 = "> Vdref:  {:7.5f} ".format(self.vdref)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        else:
            s1 = " vdref must be > 0, check the configuration file"
            print(s1)
            sys.exit()
        self.REF_SITE = list()
        s1 = "-- Section CONSTRAINTS SITES --"
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        sites = data["CONSTRAINTS SITES"]["SITES"]

        fas = readingfas(self.fas_dir)
        stats = statall(fas) 
        if sites == 'ALL':
            self.iref = 0
            fas_sta = fassta(fas)
            print('')
            print(stats)
            print('')
            sites = fas_sta['FDSN'].tolist()
            p1 = sites
            self.REF_SITE = p1
            self.nsites = len(p1)
            print(self.REF_SITE)
            s1 = "> References Sites:  ALL "
            #for i in range(self.nsites):
            #    self.REF_SITE.append(p1[i])
            #    s1 = " {:s} ".format(p1[i])
            #    self.log.write(s1)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        else:
            self.iref = 1
            p1 = sites.split(",")
            self.nsites = len(p1)
            s1 = "> N reference site:  {:d}  ".format(len(p1))
            self.log.write(s1)
            print(s1, end='')
            for i in range(self.nsites):
                self.REF_SITE.append(p1[i])
                s1 = " {:s} ".format(p1[i])
                self.log.write(s1)
                print(s1, end='')
            self.log.write("\n")
        print("")   
        try:
            self.weight_site = float(data["CONSTRAINTS SITES"]["weight_site"])
        except Exception as e:
            print("ERROR: check weight_site in the configuration file: " + str(e))
            sys.exit()
        if self.weight_site > 0:
            s1 = "> Weight_site:  {:3.0f} ".format(self.weight_site)
            self.log.write(s1)
            self.log.write("\n")
            print(s1)
        else:
            s1 = "Weight_site must be >0:  check the configuration file"
            print(s1)
            sys.exit()
        s1 = data["CONSTRAINTS SITES"]["Knot"]
        if s1 != 'Na':
            try:
                self.knot = float(s1)
                s1 = "> Knot :  {:7.5f} ".format(self.knot)
            except Exception as e:
                print("ERROR: check Knot in the configuration file: " + str(e))
            if self.knot < 0:
                print("ERROR: Knot must be >=0, check the configuration file ")
                sys.exit()
        else:
            s1 = "> Knot :  Na "
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        #s1=str(data["CONSTRAINTS SITES"]["FileCrust"])
        #if s1 != 'Na':
        #    self.FileCrust = s1
        #    s1 = "> FileCrust :  {:s} ".format(self.FileCrust)
        #    self.ReadFileCrust()
        #else:
        #    s1 = "> FileCrust :  Na "
        #self.log.write(s1)
        #self.log.write("\n")
        #print(s1)
        s1=data["CONSTRAINTS SITES"]["Fk"]
        if s1 != 'Na':
            try:
                self.Fk = float(s1)
                s1 = "> Threshold Freq (K):  {:7.5f} ".format(self.Fk)
            except Exception as e:
                print("ERROR: check Fk in the configuration file: " + str(e))
                sys.exit()
            if self.Fk < 0:
                print("ERROR: Fk must be >=0, check the configuration file ")
                sys.exit()            
        else:
            s1 = "> Threshold Freq (K): Na "
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        s1 = "-- Section CONSTRAINTS SMOOTH DIST  --"
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        try:
            self.weight_smo = float(data["CONSTRAINTS SMOOTH DIST"]["weight_smo"])
            s1 = "> Weight_smo:  {:3.0f} ".format(self.weight_smo)
        except Exception as e:
            print("ERROR: check Weight_smo in the configuration file: " + str(e))
            sys.exit()
        if self.weight_smo <= 0:
            print("ERROR: Weight_smo must be > 0, check the configuration file ")
            sys.exit()        
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        s1 = "-- Section LSQR INVERSION   --"
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        try:
            self.nrow_min = int(data["LSQR INVERSION"]["nrow_min"])
            s1 = "> Nrow_min:  {:d} ".format(self.nrow_min)
        except Exception as e:
            print("ERROR: check Nrow_min in the configuration file: " + str(e))
            sys.exit()
        if self.nrow_min <= 0:
            print("ERROR: Nrow_min must be > 0, check the configuration file ")
            sys.exit()
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        try:
            self.damp = float(data["LSQR INVERSION"]["damp"])
            s1 = "> Damp:  {:f} ".format(self.damp)
        except Exception as e:
            print("ERROR: check damp in the configuration file: " + str(e))
            sys.exit()
        if self.damp <=0:
            print("ERROR: damp must be > 0, check the configuration file ")
            sys.exit()
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        try:
            self.atol = float(data["LSQR INVERSION"]["atol"])
            s1 = "> Atol:  {:e} ".format(self.atol)
        except Exception as e:
            print("ERROR: check atol in the configuration file: " + str(e))
            sys.exit()
        if self.atol <=0:
            print("ERROR: atol must be > 0, check the configuration file ")
            sys.exit()
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        try:
            self.btol = float(data["LSQR INVERSION"]["btol"])
            s1 = "> Btol:  {:e} ".format(self.btol)
        except Exception as e:
            print("ERROR: check Btol in the configuration file: " + str(e))
            sys.exit()
        if self.btol <=0:
            print("ERROR: Btol must be > 0, check the configuration file ")
            sys.exit()
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        try:
            self.iter_lim = int(data["LSQR INVERSION"]["iter_lim"])
            s1 = "> Iter_lim:  {:d} ".format(self.iter_lim)
        except Exception as e:
            print("ERROR: check Iter_lim in the configuration file: " + str(e))
            sys.exit()
        if self.iter_lim <=0:
            print("ERROR: Iter_lim must be > 0, check the configuration file ")
            sys.exit()
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
        s1 = "....... END reading Yaml Configuration File: {:s} ".format(fname)
        self.log.write(s1)
        self.log.write("\n")
        print(s1)
