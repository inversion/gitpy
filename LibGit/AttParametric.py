#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math

class AttParametric:
    
    def __init__(self):
        self.ty=-1
        self.rref=-1
        self.vs=-1
        self.h1=-1
        self.h2=-1
        self.n1=-1
        self.n2=-1
        self.n3=-1
        self.Q0=-1
        self.n=-1
        self.nfreq=-1
        self.ndist=-1
        self.freq = list()
        self.dist = list()

    def setUnoSuR(self,dref,Q,n0,vss,dist,freq):
        self.ty=0
        self.rref=dref
        self.vs=vss
        self.n1=1
        self.n2=0
        self.n3=0
        self.h1=self.h2=-1
        self.n=n0
        self.Q0=Q
        self.ndist = len(dist)
        self.nfreq = len(freq)
        initial_val = 0.0
        self.att = [[initial_val] * self.nfreq for _ in range(self.ndist)]
        for i in range(self.nfreq):
            self.freq.append(freq[i])
        for j in range(self.ndist):
            self.dist.append(dist[j])
            for i in range(self.nfreq):
                self.att[j][i] = self.getAttuation(dist[j],freq[i])
        return self.att
        
    def setTrilinear(self,dref,Q,n0,vss,dist,freq,n1,n2,n3,h1,h2):
        self.ty=2
        self.rref=dref
        self.vs=vss
        self.n1=n1
        self.n2=n2
        self.n3=n3   
        self.h1=h1
        self.h2=h2
        self.n=n0
        self.Q0=Q
        self.ndist = len(dist)
        self.nfreq = len(freq)
        initial_val = 0.0
        self.att = [[initial_val] * self.nfreq for _ in range(self.ndist)]
        for i in range(self.nfreq):
            self.freq.append(freq[i])
        for j in range(self.ndist):
            self.dist.append(dist[j])
            for i in range(self.nfreq):
                self.att[j][i] = self.getAttuation(dist[j],freq[i])
        return self.att

    def setRef(self,dref):
        self.rref=dref
        
    def setBilinear(self,dref,Q,n0,vss,dist,freq,n1,n2,h1):
        self.ty=1
        self.rref=dref
        self.vs=vss
        self.n1=n1
        self.n2=n2
        self.h1=h1
        self.n=n0
        self.Q0=Q
        self.ndist = len(dist)
        self.nfreq = len(freq)
        initial_val = 0.0
        self.att = [[initial_val] * self.nfreq for _ in range(self.ndist)]
        for i in range(self.nfreq):
            self.freq.append(freq[i])
        for j in range(self.ndist):
            self.dist.append(dist[j])
            for i in range(self.nfreq):
                self.att[j][i] = self.getAttuation(dist[j],freq[i])        
        return self.att
        
    def getAttuation(self,d0,fr):
        val=1
        if self.ty == 0:
            gr=(self.rref/d0)
            gr=pow(gr,self.n1)
            num=-math.pi*fr*(d0-self.rref)
            den=(self.Q0*pow(fr,self.n))*self.vs
            v1=num/den
            v2=math.exp(v1)
            val=gr*v2
            val=math.log10(val)
        elif self.ty == 1:
            if d0 <= self.h1:
                gr = pow(self.rref/d0,self.n1)
            else:
                gr1 = pow(self.rref/self.h1,self.n1)
                gr2 = pow(self.h1/d0,self.n2)
                gr = gr1*gr2
            num=-math.pi*fr*(d0-self.rref)
            den=(self.Q0*pow(fr,self.n))*self.vs
            v1=num/den
            v2=math.exp(v1)
            val=gr*v2
            val=math.log10(val)  
        elif self.ty == 2:
            if d0 <= self.h1:
                gr = pow(self.rref/d0,self.n1)
            elif d0 >= self.h2:
                gr1 = pow(self.rref/self.h1,self.n1)
                gr2 = pow(self.h1/self.h2,self.n2)
                gr3 = pow(self.h2/d0,self.n3)
                gr = gr1*gr2*gr3
            else:
                gr1 = pow(self.rref/self.h1,self.n1)
                gr2 = pow(self.h1/d0,self.n2)
                gr = gr1*gr2
            num=-math.pi*fr*(d0-self.rref)
            den=(self.Q0*pow(fr,self.n))*self.vs
            v1=num/den
            v2=math.exp(v1)
            val=gr*v2
            val=math.log10(val)        
        return val
    
    def getAnelasticAtt (self,Att,GRmod):
        AnelasticAtt = Att - GRmod
        return AnelasticAtt