#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
import sys
import os
import shutil
from pathlib import Path
from yaml.loader import SafeLoader

class ConfSourceFit:
    
    def __init__(self,fname,verbose):
        
        self.verbose = verbose
        self.job_name = None
        self.gmparam = None
        self.plot_graph = None
        self.save_graph = None
        self.Vs = None
        self.dref = None
        self.rho = None
        self.Rp = None
        self.Fs = None
        self.En = None
        self.hingfreqkappa = None
        self.type_model = None
        self.magnitude_file = None
        self.sourcemodel = None
        self.f_range_file = None
        self.outdir = None
        self.anchorage_mw = None
        self.sourcetype = None
        self.fit_apps_list = None
        if Path(fname).suffix != '.yaml':
            msg = f" Config File {fname} not yaml extension\n Aborting"
            print(msg)
            sys.exit(1)
        try:
            with open(fname) as f:
                   data = yaml.load(f, Loader=SafeLoader)
        except FileNotFoundError:
            print(f"File {fname} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {fname}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {fname} is", repr(err))
            sys.exit(1)
            
        s1 = "....... START reading Ymal Configuration File: {:s} ".format(fname) 
        s1 = "-- Section GLOBAL --"
        if verbose: print(s1)
        self.type = data["SOURCEFITTING"]["type"]
        self.source_dir = data["GLOBAL"]["source_dir"]
        if os.path.exists(self.source_dir) == False:
            print('Warning!! the folder ' + self.source_dir + ' does not exists: check source_dir in the configuration file')
            sys.exit()
        else:
            s = "> Source Dir: {:s} ".format(self.source_dir)
            print(s)
        self.outdir = data["GLOBAL"]["out_dir"]
        s1 = "> Out Dir: {:s} ".format(self.outdir)

        if os.path.exists(self.outdir + '/fitmod' + str(self.type)) == False:
            os.makedirs(self.outdir + '/fitmod' + str(self.type))
        else:
            print('Warning!! the folder ' + self.outdir + '/fitmod' + str(self.type) + ' already exists, do you want to refresh it?')
            query = float(input('digit 1 to refresh the files in the folder, otherwise 0: '))
            if query == 1:
                path = self.outdir + '/fitmod' + str(self.type)
                try:
                    shutil.rmtree(path)
                    print("Directory removed successfully")
                except OSError as o:
                    print(f"Error, {o.strerror}: {path}")
                os.makedirs(self.outdir + '/fitmod' + str(self.type))

        logID = open(self.outdir + '/fitmod' + str(self.type) + '/logsource.txt','w') # to open a log file
        logID.write(s)
        logID.write("\n")        
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)
        self.job_name = data["GLOBAL"]["job_name"]
        s1 = "> Job Name: {:s} ".format(self.job_name)
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)
        self.out_dir = data["GLOBAL"]["out_dir"]
        s1 = "> Output Dir: {:s} ".format(self.out_dir)
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)  
                     
        s1 = "-- Section  CONSTRAINT--"
        try:
            self.dref = float(data["CONSTRAINT"]["dref"])
            s1 = "> Reference Distance [km]: {:s} ".format(str(self.dref))
        except Exception as e:
            s1 = "ERROR: check Dref in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        if self.dref <=0:
            s1 = "ERROR: Dref must be > 0, check the configuration file "
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        if verbose: print(s1)      
            
        try:
            self.Rp = float(data["CONSTRAINT"]["Rp"])
            s1 = "> Radiation Pattern: {:s} ".format(str(self.Rp))
        except Exception as e:
            s1 = "ERROR: check Rp in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        if self.Rp <=0:
            s1 = "ERROR: Rp must be > 0, check the configuration file "
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1) 
        
        try:
            self.Fs = float(data["CONSTRAINT"]["Fs"])
            s1 = "> Free surface coefficient: {:s} ".format(str(self.Fs))
        except Exception as e:
            s1 = "ERROR: check Fs in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()        
        if self.Rp <=0:
            s1 = "ERROR: Fs must be > 0, check the configuration file "
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)
                
        try:
            self.En = float(data["CONSTRAINT"]["En"])
            s1 = "> Splitting component: {:s} ".format(str(self.Fs))
        except Exception as e:
            s1 = "ERROR: check En in the configuration file " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        if self.Fs <=0:
            s1 = "ERROR: En must be > 0, check the configuration file "
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)
                
        try:
            self.anchorage_mw = int(data["CONSTRAINT"]["AnchorageMw"])
            s1 = "> Flag for Magnitude Catalog : {:s} ".format(str(self.anchorage_mw))
            if self.anchorage_mw == 1 or self.anchorage_mw == 0:
                pass
            else:
                s1 = "> ERROR: check AnchorageMw the configuration file. Set 1: to associate the events magnitudes to a reference catalog; 0: to use the magnitudes values in FAS files "               
                print(s1)
                logID.write(s1)
                logID.write("\n")
                sys.exit()
        except Exception as e:
            s1 = "> ERROR: check AnchorageMw the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        
        if self.anchorage_mw == 1:
            try:
                self.magnitude_file = data["CONSTRAINT"]["magnitude_file"]
                val = os.path.isfile(self.magnitude_file)
                if val == False:
                    s1 = "> WARNING No such file or directory: {:s} ".format(self.magnitude_file)
                    print(s1)  
                    self.log.write(s1)
                    self.log.write("\n")
                    sys.exit()
                else:
                    s1 = "> Magnitude file: {:s} ".format(str(self.magnitude_file))
                    logID.write(s1)
                    logID.write("\n")
                    if verbose: print(s1)
            except Exception as e:
                s1 = "> ERROR: No Magnitude file, check the configuration file: " + str(e)
                print(s1)      
                logID.write(s1)
                logID.write("\n")
                sys.exit()
            
        s1 = "> To anchorate spectral amplitude to a reference Mw catalog: {:s} ".format(str(self.En))
        logID.write(s1)
        logID.write("\n")
            
        s1 = "-- Section CRUSTALMODEL --"
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)

        try:
            self.type_model = str(data["CRUSTALMODEL"]["type_model"])
            if self.type_model == "HOMOGENEOUS" or self.type_model == "HETEROGENEOUS":
                s1 = "> crustal_model: {:s} ".format(str(self.type_model))
                pass
            else:
                s1 = "ERROR! type_model in the configuration file must be set to HOMOGENEOUS or HETEROGENEOUS " 
                print(s1)
                logID.write(s1)
                logID.write("\n")
                sys.exit()
        except Exception as e:
            s1 = "ERROR! check type_model in the configuration file: " + str(e) 
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)        
        
        
        if self.type_model == "HETEROGENEOUS":
            try:
                self.crustal_model = data["CRUSTALMODEL"]["crustal_model"]
                val = os.path.isfile(self.crustal_model)
                if val == False:
                  s1 = "> WARNING No such file or directory: {:s} ".format(self.crustal_model)
                  print(s1)  
                  logID.write(s1)
                  logID.write("\n")
                  sys.exit()
                else:
                    s1 = "> Crustal model file: {:s} ".format(str(self.crustal_model))
                    logID.write(s1)
                    logID.write("\n")
                    if verbose: print(s1)
            except Exception as e:
                s1 = "> ERROR: No Crustal Model file, check the configuration file: " + str(e)
                print(s1)
                logID.write(s1)
                logID.write("\n")
                sys.exit()               
        
        try:
            self.Vs = float(data["CRUSTALMODEL"]["Vs"])
            s1 = "> S wave velocity [km/s]: {:s} ".format(str(self.Vs))
            logID.write(s1)
            logID.write("\n")
            if verbose: print(s1)
        except Exception as e:
            s1 = "ERROR: check Vs in the configuration file " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        if self.Fs <=0:
            s1 = "ERROR: Vs must be > 0, check the configuration file "
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()

        try:
            self.rho = float(data["CRUSTALMODEL"]["rho"])
            s1 = "> Crustal medium density [g/cm^3]: {:s} ".format(str(self.rho))
            logID.write(s1)
            logID.write("\n")
            if verbose: print(s1)
            logID.write("\n")  
        except Exception as e:
            s1 = "ERROR: check rho in the configuration file " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        if self.Fs <=0:
            s1 = "ERROR: rho must be > 0, check the configuration file "
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()

        s1 = "-- Section SOURCEFITTING --"
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)
        
        try:
            self.type = int(data["SOURCEFITTING"]["type"])
            if self.type == 1: 
                s2 = "simultaneous fit for M0 and fc"
            #if self.type == 2: s2 = "three steps from Bindi and Kota 2020" next release
                s1 = "> type of fitting: {:s} ".format(s2)
                logID.write(s1)
                logID.write("\n")
                if verbose: print(s1)
            else:
                s1 = "ERROR! the type of fitting in the configuration file must be 1"
                print(s1)
                logID.write(s1)
                logID.write("\n")
                sys.exit()
        except Exception as e:
            s1 = "ERROR: check type in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()                  

        try:
            self.sourcemodel = int(data["SOURCEFITTING"]["sourcemodel"])
            if self.sourcemodel == 1: 
                s2 = "Brune's model"
                s1 = "> type of source of model: {:s} ".format(s2)
                logID.write(s1)
                logID.write("\n")
                if verbose: print(s1)
            else:
                s1 = "ERROR! sourcemodel in the configuration file must be 1"
                print(s1)
                logID.write(s1)
                logID.write("\n")
                sys.exit()
        except Exception as e:
            s1 = "ERROR: check sourcemodel in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()       
        
        try:
            self.f_range = int(data["SOURCEFITTING"]["f_range"])
            if self.f_range == 1: 
                s1 = "Source Fitting between User Defined fmin and fmax"
                logID.write(s1)
                logID.write("\n")
                if verbose: print(s1)
            elif self.f_range == 2:
                s1 = "Source Fitting between an automatically calculated fmin and fmax = 30 Hz"
                if verbose: print(s1)
                logID.write(s1)
                logID.write("\n")
            else:
                s1 = "ERROR: f_range in the configuration file must be 1 or 2 "
                print(s1)
                logID.write(s1)
                logID.write("\n")
                sys.exit()                
        except Exception as e:
            s1 = "ERROR: check f_range in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()        
        
        if self.f_range == 1:                   
            try:
                self.f_range_file_name = data["SOURCEFITTING"]["f_range_file"]
                s2 = "...reading fmin and fmax from " + self.f_range_file_name
            except:
                msg = "ERROR: check the configuration file, f_range_file not found"
                print(msg)
                logID.write(msg)
                logID.write("\n")
                sys.exit(1)                
        if self.f_range == 2: s2 = "....calculating fmin and fmax"
        logID.write(s2)
        logID.write("\n")
        if verbose: print(s2)
        
        try:
            self.sourcetype =  int(data["SOURCEFITTING"]["appflag"] )
            if self.sourcetype == 1: s2 = "...working on apparent source spectra (*.app_source) " 
            elif self.sourcetype == 0: s2 = "...working on source spectra (*.source) " 
            else: 
                s1 = "ERROR! appflag in the configuration file must be 1 or 0 "
                print(s1)
                logID.write(s1)
                logID.write("\n")
                sys.exit()
            logID.write(s2) 
            logID.write("\n")
            if verbose: print(s2)
        except Exception as e:
            s1 = "ERROR! check appflag in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        
        if  self.sourcetype == 1:  
            try:
                self.fit_apps_list =  data["SOURCEFITTING"]["list_eve"] 
                if self.sourcetype == 1: s2 = "...source parameters estimation for a specific list of apparent source spectra (*.app_source): " + str(self.fit_apps_list)
                logID.write(s2) 
                logID.write("\n")
                if verbose: print(s2)
            except Exception as e:
                s1 = "ERROR! check list_eve in the configuration file: " + str(e)
                print(s1)
                logID.write(s1)
                logID.write("\n") 
                sys.exit()        
                         
        s1 = "-- Section KAPPA --"
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)

        try:
            self.hingfreqkappa = float(data["KAPPA"]["hingfreq"])
            s1 = "> Hinge Frequency for Kappa [Hz]: {:s} ".format(str(self.hingfreqkappa))
            logID.write(s1)
            logID.write("\n")
            if verbose: print(s1)
            logID.write("\n")  
        except Exception as e:
            s1 = "ERROR: check hingfreq in the configuration file " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
        if self.Fs <=0:
            s1 = "ERROR: hingfreq must be > 0, check the configuration file "
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()  
        
        logID.write("\n")        
        s1 = "-- Section PLOT --"
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)   
                
        try:
            self.gmparam = str(data["PLOT"]["param"])
            if self.gmparam == "DISP" or self.gmparam == "VEL" or self.gmparam == "ACC":
                s1 = "> Ground Motion Parameter: {:s} ".format(str(self.gmparam))
                if verbose: print(s1)
                logID.write(s1)
                logID.write("\n")
            else:
                s1 = "ERROR! param in the configuration file must be DISP, VEL, or ACC"
                print(s1)
                logID.write(s1)
                logID.write("\n")
                sys.exit()
        except Exception as e:
            s1 = "ERROR! check param in the configuration file: " + str(e)  
            print(s1)                       
            logID.write(s1)
            logID.write("\n")
            sys.exit()
            
        try:
            self.plot_graph = data["PLOT"]["plot_graph"]
            if self.plot_graph == True or self.plot_graph == False:
                s1 = "> Plot Graph: {:s} ".format(str(self.plot_graph))
                if verbose: print(s1)
                logID.write(s1)
                logID.write("\n")
            else:
                s1 = "ERROR! plot_graph in the configuration file must be True or False"
                print(s1)
                logID.write(s1)
                logID.write("\n")
                sys.exit()
        except Exception as e:
            s1 = "ERROR! check plot_graph in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
            
        try:
            self.plot_eve_list = data["PLOT"]["list_eve"]  
            s1 = "> List of Events to plot: {:s} ".format(str(self.plot_eve_list))
            logID.write(s1)
            logID.write("\n")
            if verbose: print(s1)
        except Exception as e:
            s1 = "ERROR! check list_eve to plot source spectra in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()
            
        logID.write("\n")
        s1 = "-- Section SAVE --"
        logID.write(s1)
        logID.write("\n")
        if verbose: print(s1)

        try:
            self.save_graph = data["SAVE"]["save_graph"]
            if self.save_graph == True or self.save_graph == False:
                s1 = "> Save Graph: {:s} ".format(str(self.save_graph))
                if verbose: print(s1)
                logID.write(s1)
                logID.write("\n")
            else:
                s1 = "ERROR! save_graph in the configuration file must be True or False"
                print(s1)
                logID.write(s1)
                logID.write("\n")
                sys.exit()
        except Exception as e:
            s1 = "ERROR! check save_graph in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()

        try:
            self.save_eve_list = data["PLOT"]["list_eve"]  
            s1 = "> List of Events to save: {:s} ".format(str(self.save_eve_list))
            logID.write(s1)
            logID.write("\n")
            if verbose: print(s1)
        except Exception as e:
            s1 = "ERROR! check list_eve to save source spectra in the configuration file: " + str(e)
            print(s1)
            logID.write(s1)
            logID.write("\n")
            sys.exit()        
        
        logID.close()