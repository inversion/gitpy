#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import math
import os
import numpy as np
import pandas as pd
from lmfit import Parameters,minimize, fit_report
from plotnine import aes, ggplot, scale_x_log10,ggtitle,geom_line, geom_point, theme_bw,geom_vline,annotate, ggtitle,theme_bw, xlab, ylab, scale_color_distiller, annotation_logticks
from scipy.optimize import curve_fit
from LibGit.utilities import fun_kappa,power_fitting_lmfit,fun_spectral_fit
from LibGit.EvtGit import EvtGit

class Source:
    
    def __init__(self, *args):
        self.evtGit = list()
        self.freq = list()
        self.progName = None
        self.comp = None
        self.comptype = None
        self.eveid = None
        self.Mag = None
        self.lat = None
        self.lon = None
        self.depth = None
        self.offset = None
        self.fname = str(args[0])
        if os.path.isfile(args[0]):
            self.FromFile(args[0])

    def FromFile(self, fn):
        self.fname = fn
        self.freq = list()
        self.Amp = list()
        try:
            f = open(fn, "r")
        except FileNotFoundError:
            print(f"File {fn} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {fn}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {fn} is", repr(err))
            sys.exit(1)
        line = f.readline()
        line = line.rstrip('\n')
        p1 = line.split()        
        self.progName = p1[0]
        self.cmp = p1[1]
        self.evtGit.append(EvtGit(fn))
        self.nevt = len(self.evtGit)
        self.comptype = (p1[2])
        self.eveid = p1[3]
        self.Mag = float(p1[4])
        self.lat = float(p1[5])
        self.lon = float(p1[6])
        self.depth = float(p1[7])
        if len(p1) > 8:
            self.offset = float(p1[8])
       
        if self.nevt > 0:
            self.progName = self.evtGit[0].programName
            self.nfreq = self.evtGit[0].nfreq
            self.freq = list()
            self.Amp = list()
            #for i in range(self.nfreq):
            #    self.nfreq
            #    self.freq.append(self.evtGit[0].freq[i])
                
        lines = f.readlines()
        for row in lines:
            row = row.rstrip('\n')
            row = row.split()
            self.freq.append(row[0])
            self.Amp.append(row[1])
        f.close()

    def plotSources(self, name_out, v1, v2, typ):
        dtot = None
        
        if typ == "ACC":
            st = "Inv: {:s}, Cmp: {:s}, Nevt: {:d} (Acc)".format(self.progName, self.evtGit[0].cmp, self.nevt)
        elif typ == "VEL":
            st = "Inv: {:s}, Cmp: {:s}, Nevt: {:d} (Vel)".format(self.progName, self.evtGit[0].cmp, self.nevt)
        else:
            st = "Inv: {:s}, Cmp: {:s}, Nevt: {:d} (Dsp)".format(self.progName, self.evtGit[0].cmp, self.nevt)
        for i in range(self.nevt):
            #      print(i, " ",self.evtGit[i].nfreq)
            f1 = self.evtGit[i].freq
            id0 = self.evtGit[i].Id0
            id1 = [id0 for i in range(self.nfreq)]
            m1 = round(self.evtGit[i].Ml, 2)
            #   m00=str(m1)
            s1 = []
            s1 = [m1 for i in range(self.nfreq)]
            if typ == "VEL":
                for ifr in range(self.nfreq):
                    v1 = math.pow(10., self.evtGit[i].val[ifr])
                    v1 = v1 / (2 * math.pi * f1[ifr])
                    self.evtGit[i].val[ifr] = math.log10(v1)
            elif typ == "DSP":
                for ifr in range(self.nfreq):
                    v1 = math.pow(10., self.evtGit[i].val[ifr])
                    v1 = v1 / ((2 * math.pi * f1[ifr]) * (2 * math.pi * f1[ifr]))
                    self.evtGit[i].val[ifr] = math.log10(v1)
            #
            #       v1=math.log10(v1)
            #  self.evtGit[i].val[ifr]=v1
            #
            #     #v1 = v1 / ((2 * math.pi * f1[ifr]))
            #     v1=math.log10(v1)
            #     self.evtGit[i].val[ifr]=v1
            d1 = {'x': self.evtGit[i].freq, 'y': self.evtGit[i].val, 'TY': id1, 'FR': s1}
            d1 = pd.DataFrame(d1)
            if i == 0:
                dtot = d1
            else:
                dtot = dtot.append(d1)
        del f1
        del m1
        del s1
        if v1 != False or v2 != False:
            dtot = dtot.reset_index()
            g = ggplot(dtot, aes(x='x', y='y', group='TY', color='FR')) + geom_line(size=0.2)
            g = g + scale_color_distiller(name='Ml ', palette=9, type='div')
            g = g + scale_x_log10(breaks=[0.3, 1.0, 3.0, 10.0, 30.0])
            g = g + annotation_logticks(sides="b")
            g = g + xlab("Frequency [Hz]") + ylab("Log(Source)")
            g = g + ggtitle(st)
            g = g + theme_bw()
            if v1:
                print(g)
            if v2 != False:
                if name_out:
                    g.save(filename=name_out, dpi=300)

    def calculate_M0(self):
        M0_dyne_cm = pow(10,1.5*self.Mag+16.1) # stima momento sismico da Ml nei FAS in dyne*cm
        M0_N_m = pow(10,1.5*self.Mag+9.1)    
        return [M0_dyne_cm,M0_N_m]                    

    def calculate_fc(self,Vs,stress_drop,M0_dyne_cm):
        f_c = 4.9*pow(10,6)*Vs*pow(stress_drop/M0_dyne_cm,1/3)
        return f_c

    def method1(self,d1,ConfigFile,Evt,const,M0_N_m,f_c,opts,errors,warning,fmin,fmax,VS,mag_flag,Mcat,MWflag,indsta):
        #fmin = fmintofit (d1,3)

        if ConfigFile.sourcetype == 1:
            d3 = d1.iloc[indsta][0:4].reset_index()        
            FDSN = d3[indsta][0]
            Dist = d3[indsta][1]
            Az = d3[indsta][2]
            Bz = d3[indsta][3]
            d1 = d1.iloc[indsta][4:-1].reset_index().astype(float)
            d1.columns = ['Frequency [Hz]','Acceleration Spectrum [m/s]']
            d1 = d1.dropna()
        if ConfigFile.hingfreqkappa < f_c:
            SAfit = d1.loc[d1['Frequency [Hz]'] > f_c]
        else:
            SAfit = d1.loc[d1['Frequency [Hz]'] > ConfigFile.hingfreqkappa]      
        if len(SAfit['Acceleration Spectrum [m/s]'].tolist()) > 3:

            popt_kappa, pcov_kappa = curve_fit(fun_kappa, (SAfit['Frequency [Hz]']).tolist(), SAfit['Acceleration Spectrum [m/s]'].tolist())
            #dfmi.loc[:, ('one', 'second')] = value
            SAfit.loc[:,'AmplitudeFit'] = fun_kappa(SAfit['Frequency [Hz]'],popt_kappa[0],popt_kappa[1]).tolist()
            #Correct high frequency part for kappa before fitting source spectra
            SAfit.loc[:,'AmpAccCorrK'] = SAfit['Acceleration Spectrum [m/s]'] - SAfit['AmplitudeFit']
            offset = np.abs(SAfit['Acceleration Spectrum [m/s]'].tolist()[0] - SAfit['AmpAccCorrK'].tolist()[0])
            if SAfit['Acceleration Spectrum [m/s]'].tolist()[0] - SAfit['AmpAccCorrK'].tolist()[0] > 0:
                SAfit.loc[:,'AmpCorrk_offset'] = SAfit['AmpAccCorrK'] + offset
            else:           
                SAfit.loc[:,'AmpCorrk_offset'] = SAfit['AmpAccCorrK'] - offset
            d1.loc[:,'AmpAccCorrK'] = d1['Acceleration Spectrum [m/s]']
            for i in range(len(SAfit['Frequency [Hz]'])):
                if ConfigFile.hingfreqkappa < f_c:
                    d1.loc[:,'AmpAccCorrK']= d1['AmpAccCorrK'].replace(d1['AmpAccCorrK'].loc[d1['Frequency [Hz]'] > f_c].tolist()[i],SAfit['AmpCorrk_offset'].tolist()[i]) 
                else:    
                    d1.loc[:,'AmpAccCorrK']= d1['AmpAccCorrK'].replace(d1['AmpAccCorrK'].loc[d1['Frequency [Hz]'] > ConfigFile.hingfreqkappa].tolist()[i],SAfit['AmpCorrk_offset'].tolist()[i]) 
            #plot_KappaCorrSourceSpe (d1,SAfit,Evt.Id0,True,False,ConfigFile.out_dir)

            d1.loc[:,'AmpDispCorrK'] = d1['AmpAccCorrK'].tolist() - 2*np.log10(2*math.pi*d1['Frequency [Hz]']) - 2 # Spettro in spostamento in metri
            d1.loc[:,'AmpVelCorrK'] = d1['AmpAccCorrK'].tolist() - np.log10(2*math.pi*d1['Frequency [Hz]']) - 2 # Spettro in velocità    
            #fmin = fmintofit (d1,3)

            #plot_fmin (d1,Evt.Id0,fmin,False,False)

            d2 = d1.loc[d1['Frequency [Hz]']>=fmin]
            d2_ = d2.loc[d2['Frequency [Hz]']<=fmax]
            d2 = d2_
            try:
                if mag_flag == 0:
                    #popt_M0_fc, pcov_M0_fc = curve_fit(fun_spectral_fit,d2['Frequency [Hz]'].tolist(), d2['Displacement Spectrum [m*s]'].tolist() - const,p0=[M0_N_m,f_c],bounds=((0,0),(np.inf,100)))

                    # Defining the various parameters
                    params = Parameters()
                    # M0 is constrained to be >=0
                    params.add('M0', value=M0_N_m,min= 0, vary = True)
                    # fc is constrained in the [0,100] Hz range
                    params.add('fc', value=f_c, min= 0, max= 100)
                    # Calling the minimize function. Args contains the x and y data.
                    fitted_params = minimize(power_fitting_lmfit, params, args=(d2['Frequency [Hz]'].tolist(),d2['AmpDispCorrK'].tolist() - const,), method='least_squares')
                    ndata = len(d2['AmpDispCorrK'].tolist())
                    # Getting the fitted values
                    beta1 = fitted_params.params['M0'].value
                    beta2 = fitted_params.params['fc'].value    

                    # Printing the fitted values
                    try:
                        if opts.verb:print('FDSN = ' + FDSN)
                    except:
                        print()
                    if opts.verb:print('log10(M0) = ', str('{:.1f}'.format(np.log10(beta1))) + ' Nm')
                    if opts.verb:print('fc = ', str('{:.1f}'.format(beta2)) + ' Hz')
                    if opts.verb:print('Correlation (M0,fc)', fit_report(fitted_params).split('\n')[-1].split('=')[-1])
                    if opts.verb:print('ks = ', str('{:.3f}'.format(popt_kappa[1])) + ' s')
                    if opts.verb:print('###########################')
                    if opts.verb:print('')
                    # Pretty printing all the statistical data
                    #if opts.verb: print(fit_report(fitted_params))
                    popt_M0_fc = np.array([beta1,beta2])
                    #pcov_M0_fc = fitted_params.covar            

                else:
                
                    # Defining the various parameters
                    params = Parameters()
                    # M0 is fixed
                    params.add('M0', value=M0_N_m,vary = False)
                    # fc is constrained in the [0, 100] Hz range
                    params.add('fc', value=f_c, min= 0, max= 100)
                    # Calling the minimize function. Args contains the x and y data.


                    fitted_params = minimize(power_fitting_lmfit, params, args=(d2['Frequency [Hz]'].tolist(),d2['AmpDispCorrK'].tolist() - const,), method='least_squares')
                    ndata = len(d2['AmpDispCorrK'].tolist())
                    # Getting the fitted values
                    beta1 = fitted_params.params['M0'].value
                    beta2 = fitted_params.params['fc'].value 

                    # Printing the fitted values
                    try:
                        if opts.verb: print('FDSN = ' + FDSN)
                    except:
                        print()
                    if opts.verb:print('log10(M0) = ', str('{:.1f}'.format(np.log10(beta1))) + ' Nm')
                    if opts.verb:print('fc = ', str('{:.1f}'.format(beta2)) + ' Hz')
                    if opts.verb:print('ks = ', str('{:.3f}'.format(popt_kappa[1])) + ' s')
                    if opts.verb:print('###########################')
                    if opts.verb:print('')

                    # Pretty printing all the statistical data
                    popt_M0_fc = np.array([beta1,beta2])


            except Exception as e:
                if opts.verb: print('spectral fit failure',str(e))
                errors['message'].append(str(e))
                errors['ideve'].append(Evt.Id0)
                popt_M0_fc = None
            if popt_M0_fc is not None:    
                d1.loc[:,'AmpDispMod'] = const + fun_spectral_fit(d1['Frequency [Hz]'].tolist(),popt_M0_fc[0],popt_M0_fc[1])

                SMom = popt_M0_fc[0]
                M0_error = fitted_params.params['M0'].stderr
                fc = popt_M0_fc[1]
                fc_error = fitted_params.params['fc'].stderr
                if mag_flag==0:
                    M0_fc_corr = float(fit_report(fitted_params).split('\n')[-1].split('=')[-1].replace(' ','').replace(')',''))
                else:
                    M0_fc_corr = 9999
                kappa = popt_kappa[1]
                perr_kappa = np.sqrt(np.diag(pcov_kappa))
                kappa_error = perr_kappa[1]
                ideve = Evt.Id0
                Mag = Evt.Ml
                fminl = fmin
                fmaxh = fmax 
                stressdrop1 = 7/16*np.power(2*math.pi,3)*1/np.power(2.34,3)
                stressdrop2 = np.array(SMom)*np.power(np.array(fc)/(VS*1000),3)
                stressdrop = stressdrop1*stressdrop2*(1/np.power(10,6))            
                stressd = stressdrop
                dsdM0 = (7*np.power(2*math.pi,3)*np.power(fc,3))/(16*np.power(2.34*VS*1000,3))*(1/np.power(10,6))            
                dsdfc = (21*np.power(2*math.pi,3)*SMom*np.power(fc,2))/(16*np.power(2.34*VS*1000,3))*(1/np.power(10,6))
                try:
                    stressd_error = np.sqrt(np.power(dsdM0*M0_error,2) + np.power(dsdfc*kappa_error,2))
                except:
                    stressd_error = np.nan
            else:
                kappa = np.nan
                kappa_error = np.nan
                SMom = np.nan
                M0_error = np.nan
                fc = np.nan
                fc_error = np.nan
                M0_fc_corr = 9999
                ideve = Evt.Id0
                Mag = Evt.Ml
                fminl = np.nan
                fmaxh = np.nan
                stressd = np.nan
                stressd_error = np.nan            
        else:
            kappa = np.nan
            kappa_error = np.nan
            SMom = np.nan
            M0_error = np.nan
            fc = np.nan
            fc_error = np.nan
            M0_fc_corr = 9999
            ideve = Evt.Id0
            Mag = Evt.Ml
            fminl = np.nan
            fmaxh = np.nan
            stressd = np.nan
            stressd_error = np.nan
            warning['ideve'].append(Evt.Id0) 
            warning['M'].append(Evt.Ml)
            warning['f_c'].append(f_c)
            warning['#k_sample'].append(len(SAfit['Acceleration Spectrum [m/s]'].tolist()))
        if ConfigFile.sourcetype == 1:
            return {'FDSN':FDSN,'Dist':Dist,'Az':Az,'Bz':Bz,'ideve': ideve,'M':Mag,'Mcat': Mcat,'MWflag':MWflag,'fminl': fminl, 'fmaxh': fmaxh,'M0': SMom, 'M0_error': M0_error, 'fc': fc,'fc_error':fc_error, 'M0_fc_corr': M0_fc_corr, 'kappa': kappa, 'kappa_error':kappa_error,'stressd': stressd, 'stressd_error': stressd_error,'error_message':errors, 'warnings':warning}
        else:
            return {'ideve': ideve,'M':Mag,'Mcat': Mcat,'MWflag':MWflag,'fminl': fminl, 'fmaxh': fmaxh,'M0': SMom, 'M0_error': M0_error, 'fc': fc,'fc_error':fc_error, 'M0_fc_corr': M0_fc_corr, 'kappa': kappa, 'kappa_error':kappa_error,'stressd': stressd, 'stressd_error': stressd_error,'error_message':errors, 'warnings':warning}

    def plot_FittedSourceSpe (self,df,pd_m,eveid,mag,logM0,fc,fmin,fmax,config,flag):
        if config.gmparam == 'DISP': g = ggplot(df, aes(x='Frequency [Hz]', y='Displacement Spectrum [m*s]')) + geom_line(size=1,linetype='dashed')
        if config.gmparam == 'DISP' and flag == True:g = g + geom_line(df,aes(x='Frequency [Hz]', y='AmpDispCorrK'),size=1.0,color='g')    
        if config.gmparam == 'DISP' and flag == True:g = g + geom_line(pd_m,aes(x='Frequency [Hz]', y='DispModel'),size=2.0,color='b')
        if config.gmparam == 'ACC': g = ggplot(df, aes(x='Frequency [Hz]', y='Acceleration Spectrum [m/s]')) + geom_line(size=1,linetype='dashed')
        if config.gmparam == 'ACC' and flag == True:g = g + geom_line(df,aes(x='Frequency [Hz]', y='AmpAccCorrK'),size=1.0,color='g')
        if config.gmparam == 'ACC'  and flag == True:g = g + geom_line(pd_m,aes(x='Frequency [Hz]', y='AccModel'),size=2.0,color='b')
        if config.gmparam == 'VEL': g = ggplot(df, aes(x='Frequency [Hz]', y='Velocity Spectrum [m]')) + geom_line(size=1,linetype='dashed')
        if config.gmparam == 'VEL' and flag == True:g = g + geom_line(df,aes(x='Frequency [Hz]', y='AmpVelCorrK'),size=1.0,color='g')
        if config.gmparam == 'VEL' and flag == True:g = g + geom_line(pd_m,aes(x='Frequency [Hz]', y='VelModel'),size=2.0,color='b')
        if config.gmparam == 'DISP': g = g + geom_vline(xintercept = fmin) + annotate(geom="label",x=fmin, y=pd_m['DispModel'].tolist()[1], label=str(fmin) + 'Hz', fill = 'white',size=9, colour='k')
        if config.gmparam == 'DISP': g = g + geom_vline(xintercept = fmax) + annotate(geom="label",x=fmax, y=pd_m['DispModel'].tolist()[1], label=str(fmax) + 'Hz', fill = 'white',size=9, colour='k')
        if config.gmparam == 'ACC': g = g + geom_vline(xintercept = fmin) + annotate(geom="label",x=fmin, y=pd_m['AccModel'].tolist()[1], label=str(fmin) + 'Hz', fill = 'white',size=9, colour='k')
        if config.gmparam == 'ACC': g = g + geom_vline(xintercept = fmax) + annotate(geom="label",x=fmax, y=pd_m['AccModel'].tolist()[1], label=str(fmax) + 'Hz', fill = 'white',size=9, colour='k')
        if config.gmparam == 'VEL': g = g + geom_vline(xintercept = fmin) + annotate(geom="label",x=fmin, y=pd_m['VelModel'].tolist()[1], label=str(fmin) + 'Hz', fill = 'white',size=9, colour='k')
        if config.gmparam == 'VEL': g = g + geom_vline(xintercept = fmax) + annotate(geom="label",x=fmax, y=pd_m['VelModel'].tolist()[1], label=str(fmax) + 'Hz', fill = 'white',size=9, colour='k')

        index_fc = abs(pd_m['Frequency [Hz]'] - fc).to_list().index(min(abs(pd_m['Frequency [Hz]'] - fc)))

        if config.gmparam == 'DISP': g = g + geom_point(aes(pd_m['Frequency [Hz]'][index_fc],pd_m['DispModel'][index_fc]),colour="red",shape='H',size=3) + annotate(geom="label",x=pd_m['Frequency [Hz]'][index_fc], y=pd_m['DispModel'][index_fc] + 0.2, label='fc = ' + str(np.around(float(fc),2)) + 'Hz', fill = 'w',size=9, colour='r')
        if config.gmparam == 'VEL': g = g + geom_point(aes(pd_m['Frequency [Hz]'][index_fc],pd_m['VelModel'][index_fc]),colour="red",shape='H',size=3) + annotate(geom="label",x=pd_m['Frequency [Hz]'][index_fc], y=pd_m['VelModel'][index_fc] + 0.2, label='fc = ' + str(np.around(float(fc),2)) + 'Hz', fill = 'w',size=9, colour='r')
        if config.gmparam == 'ACC': g = g + geom_point(aes(pd_m['Frequency [Hz]'][index_fc],pd_m['AccModel'][index_fc]),colour="red",shape='H',size=3) + annotate(geom="label",x=pd_m['Frequency [Hz]'][index_fc], y=pd_m['AccModel'][index_fc] - 0.5, label='fc = ' + str(np.around(float(fc),2)) + 'Hz', fill = 'w',size=9, colour='r')

        g = g + scale_x_log10()
        g = g + ggtitle('EventID: ' + eveid + '\nlog10(M0): ' + str(np.around(float(logM0),2)) + '[Nm]'+ '\nMw: ' + str(np.around(mag,2))) 
        g = g + theme_bw()

        if float(eveid) in config.plot_eve_list and config.plot_graph: print(g)
        if not os.path.exists(config.out_dir + '/fitmod' + str(config.type) + '/PLOT/'):
            os.makedirs(config.out_dir + '/fitmod' + str(config.type) + '/PLOT/')
        if float(eveid) in config.save_eve_list and config.save_graph: 
            g.save(filename=config.out_dir + '/fitmod' + str(config.type) + '/PLOT/' + eveid + '_' + config.gmparam, dpi=300,width = 5, height = 5)
            
    def outsource(self,M0_val,M0_error,fc,fc_error,kappa,kappa_error,fminl,fmaxh,stressd,stressd_error,ideve,Evt,Mag,log10_Er_mod_,App_stress_,log_10_Er_oss_,log_10_Er_oss_fit_,App_stress_oss_,log_10_Er_tot_,log_10_Er_tot_fit_,ηSW_,κv_,κv_fmin_fmax_fit_,M0_fc_corr,Mcat_,Mwflag_,Mfas_,result):        
        M0_val.append(result['M0'])
        M0_error.append(result['M0_error'])
        fc.append(result['fc'])
        fc_error.append(result['fc_error'])
        M0_fc_corr.append(result['M0_fc_corr'])
        kappa.append(result['kappa'])
        kappa_error.append(result['kappa_error'])
        ideve.append(Evt.Id0)
        Mfas_.append(result['M'])
        Mag.append(Evt.Ml)
        Mcat_.append(result['Mcat'])
        Mwflag_.append(result['MWflag'])
        fminl.append(result['fminl'])
        fmaxh.append(result['fmaxh'])
        stressd.append(result['stressd'])
        stressd_error.append(result['stressd_error'])
        log10_Er_mod_.append(result['log_10_Er_mod [J]'])
        App_stress_.append(result['AppStress [Pa]'])
        log_10_Er_oss_.append(result['log_10_Er_oss [J]'])
        log_10_Er_oss_fit_.append(result['log_10_Er_oss_fmin_fmax_fit [J]'])
        App_stress_oss_.append(result['AppStress_oss [Pa]'])    
        log_10_Er_tot_.append(result['log_10_Er_tot [J]'])
        log_10_Er_tot_fit_.append(result['log_10_Er_tot_fmin_fmax_fit [J]'])   
        ηSW_.append(result['ηSW'])
        κv_.append(result['κv'])
        κv_fmin_fmax_fit_.append(result['κv_fmin_fmax_fit'])

        return {'EveId': ideve,'M': Mfas_,'MWcat': Mcat_,'fmin [Hz]':fminl,'fmax [Hz]':fmaxh,'M0 [Nm]': M0_val, 'M0_err': M0_error,'fc [Hz]': fc, 'fc_error': fc_error,'M0_fc_corr':M0_fc_corr,'kappa [s]':kappa, 'k_err': kappa_error,'stress_drop [MPa]': stressd,'stress_drop_err': stressd_error,  'log10_Er_mod [J]': log10_Er_mod_, 'AppStress [Pa]': App_stress_,'log_10_Er_oss [J]': log_10_Er_oss_,'log_10_Er_oss_fit [J]': log_10_Er_oss_fit_, 'AppStress_oss [Pa]':App_stress_oss_,'log_10_Er_tot [J]':log_10_Er_tot_,'log_10_Er_tot_fmin_fmax_fit [J]':log_10_Er_tot_fit_,'ηSW':ηSW_,'κv':κv_,'κv_fmin_fmax_fit':κv_fmin_fmax_fit_}