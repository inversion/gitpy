#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def dict_fmt(exit_status, exit_message=None, in_file=None, out_string=None, out_object=None, out_file=None, out_format=None, out_file_content=None):
    """
    Return a feedback message 
    Args:
        exit_status(int)
        exit_message(str)
        in_file(str)
        out_string(str)
        out_object(PythonObject): can be used to return a list, dict or any other python object
        out_file(str)
        out_format(str)
        out_file_content(str)
    Returns:
        python dictionary with feedback message
    """
    return { \
    'exit_status': exit_status, \
    'exit_message': exit_message, \
    'in_file': in_file, \
    'out_string': out_string, \
    'out_object': out_object, \
    'out_file': out_file, \
    'out_format': out_format, \
    'out_file_content': out_file_content
    }



