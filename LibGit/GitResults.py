#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import shutil
import numpy as np
import matplotlib
from LibGit import Attenuation
from LibGit import EvtGit
from LibGit import Sources
from LibGit import StatGit
from LibGit import Stations

class GitResults:          
    def __init__(self,prog): 
        self.evtGit=list()       
        self.staGit=list() 
        self.STA_ID=list()
        self.EVT_ID=list()
        self.LSQR_RES=list()
        self.LSQR_FREQ=list()
        self.progname=prog
        
    def make_list_freq(self, freqlist):
        for i in range(len(freqlist)):
            self.LSQR_FREQ.append(freqlist[i].freq)

    def make_list_evt(self, evtframe, cm):
        for i in range(len(evtframe)):
            id0 = evtframe.iloc[i][0]
            if len(id0) == 11:
                id0 = "0" + id0
            lst = evtframe.iloc[i].values.tolist()
            lst.pop(0)
            lst.insert(0, id0)
            lst.append(cm)
            self.evtGit.append(EvtGit(lst))
        self.evtGit.sort(key=lambda x: x.Id0, reverse=False)
        for i in range(len(self.evtGit)):
            self.EVT_ID.append(self.evtGit[i].Id0)

    def make_list_sta(self, staframe, cm):
        for i in range(len(staframe)):
            lst = staframe.iloc[i].values.tolist()
            self.staGit.append(StatGit(lst, cm))
        self.staGit.sort(key=lambda x: x.FDSN, reverse=False)
        for i in range(len(self.staGit)):
            self.STA_ID.append(self.staGit[i].FDSN)

    def set_inc(self):
        sizeevt=len(self.evtGit)
        self.nfreq=len(self.LSQR_FREQ)
        for x in range(sizeevt):
            self.evtGit[x].set_val(self.nfreq)
        sizesta=len(self.staGit)   
        for x in range(sizesta):
            self.staGit[x].set_val(self.nfreq)
            
    def add_sta(self, Sta,cmp): 
        size=len(self.staGit)
        find=False
        for x in range(size):
            if Sta.FDSN == self.staGit[x].FDSN:
                find=True
        if find == False:  
            self.staGit.append(StatGit.StatGit(Sta,cmp))
            
    def add_evt(self, Evt):   
       size=len(self.evtGit)
       find=False
       for x in range(size):
          if Evt.Id0 == self.evtGit[x].Id0:
              find=True
       if find == False:
           p1=EvtGit.EvtGit(Evt)
           self.evtGit.append(p1)
          
    def add_Freq(self, Freq):
        self.LSQR_FREQ.append(Freq)
        
    def add_ResultsLsqr(self, X):
        self.LSQR_RES.append(X)

    def WriteResults(self,cfg,logf):
        s1="......WRITE GIT RESULTS......................"
        path_gr=os.path.join(cfg.out_dir,"GRAPH")
        path_gr=os.path.normpath(path_gr)
        s1="  Directory for graphic files: {:s}".format(path_gr)
        val=os.path.isdir(path_gr)
        if val == True:
            s1="  WARNING Out Dir:  {:s} Exist! ".format(path_gr)
            logf.write(s1)
            logf.write("\n")  
            print(s1)
        else:
            flag=os.mkdir(path_gr)
            if flag == False:
                msg=f" Cannot create Dir {path_gr} \n Aborting"
                logf.write(msg)
                logf.write("\n")  
                print(msg)    
                sys.exit()
        logf.write(s1)
        logf.write("\n") 
        print(s1)
        nfr=len(self.LSQR_FREQ)
        nrow=cfg.nbin_dist
        initial_val = 0.0
        att = [[initial_val] * nfr for _ in range(nrow)]
        path=os.path.join(cfg.out_dir,"Attenuation.txt")
        path=os.path.normpath(path)
        s1="  File for attenuation:  {:s}".format(path)
        logf.write(s1)
        logf.write("\n") 
        print(s1)
        val = os.path.isfile(path)
        if val == True:
            s1="  WARNING Out File:  {:s} Exist .... Overwrite ".format(path)
            logf.write(s1)
            logf.write("\n")  
            print(s1)
        for i in range(nfr):
            sol=self.LSQR_RES[i]
            for j in range(nrow):
                att[j][i]=sol[j]
        Att = Attenuation(cfg.dist_bin, self.LSQR_FREQ, att, cfg.dref, self.progname, cfg.cmp,cfg.job_name)
        if np.isnan(att).all() == True:
            print("WARNING! No data .... check nrow_min in the configuration file")
            sys.exit()
        Att.WriteAttenuation(path)
        s1 = "  Plot Non Parametric attenuation curves....."
        logf.write(s1)
        logf.write("\n")
        print(s1)
        path1 = os.path.join(path_gr)
        path1=os.path.normpath(path1)
        gui_env = cfg.GUI
        if gui_env == 'Na':
            s1 = "  Graphic file for attenuation:  {:s}".format(path1)
            logf.write(s1)
            logf.write("\n")
            print(s1)
            Att.plotAttenuation(path1, cfg.plot_graph, cfg.save_graph,False)            
        else:
            for gui in gui_env:
                try:
                    matplotlib.use(gui)
                    from matplotlib import pyplot as plt                
                    print("Using ..... ",matplotlib.get_backend())
                    print("")
                    s1 = "  Graphic file for attenuation:  {:s}".format(path1)
                    logf.write(s1)
                    logf.write("\n")
                    print(s1)
                    Att.plotAttenuation(path1, cfg.plot_graph, cfg.save_graph,False)
                    continue
                except:
                    print ("    ",gui, "Not found")        
        path = os.path.join(cfg.out_dir, "SOURCES")
        path=os.path.normpath(path)
        s1 = "  Directory for sources: {:s}".format(path)
        val = os.path.isdir(path)
        if val == True:
            s1 = "  WARNING Out Dir:  {:s} Exist! ".format(path)
            logf.write(s1)
            logf.write("\n")
            print(s1)
        else:
            flag = os.mkdir(path)
            if flag == False:
                msg = f" Cannot create Dir {path} \n Aborting"
                logf.write(msg)
                logf.write("\n")
                print(msg)
                sys.exit()
        Sou = Sources(self.LSQR_FREQ, self.evtGit, path, self.progname)
        Sou.WriteSources(cfg)
        path1 = os.path.join(path_gr, "Sources.jpg")
        path1=os.path.normpath(path1)
        s1 = "  Graphic file for sources:  {:s}".format(path1)
        logf.write(s1)
        logf.write("\n")
        print(s1)
        Sou.plotSources(path1, cfg.plot_graph, cfg.save_graph,"ACC")
        path=os.path.join(cfg.out_dir,"SITES")
        path=os.path.normpath(path)

        s1="  Directory for sites: {:s}".format(path)
        val=os.path.isdir(path)
        if val == True:
            s1="  WARNING Out Dir:  {:s} Exist! ".format(path)
            logf.write(s1)
            logf.write("\n")  
            print(s1)
        else:
            flag=os.mkdir(path)
            if flag == False:
                msg=f" Cannot create Dir {path} \n Aborting"
                logf.write(msg)
                logf.write("\n")  
                print(msg)    
                sys.exit()
        Sites=Stations(self.LSQR_FREQ,self.staGit,path,self.progname)
        Sites.toAmp()
        Sites.WriteStations()
        path1 = os.path.join(path_gr, "Sites.jpg")
        path1=os.path.normpath(path1)

        s1 = "  Graphic file for sites:  {:s}".format(path1)
        logf.write(s1)
        logf.write("\n")
        print(s1)
        Sites.plotStations(path1, cfg.plot_graph, cfg.save_graph)

    def copy_log_conf(self,path2conf,path2conf_d,path2log,path2log_d):
        shutil.copyfile(path2conf,path2conf_d)
        shutil.move(path2log,path2log_d)
