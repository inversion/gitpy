# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import numpy as np
import pandas as pd
import math
from plotnine import aes, ggplot, geom_line, ggtitle
from plotnine import theme_bw, xlab, ylab, annotation_logticks, \
    scale_x_log10

class EvtGit:
    def __init__(self, f):
        results = isinstance(f, str)
        resultl = isinstance(f, list)
        if results:
            self.name = f
            self.mean = None
            self.nsamp = 0
            self.freq = None
            self.val = None
            self.nfreq = None
            self.std = None
            self.nn = None
            self.app_source = None
            fr = list()
            amp = list()
            std = list()
            nn = list()
            filename = f
            try:
                f = open(self.name, "r")
            except FileNotFoundError:
                print(f"File {self.name} not found.  Aborting")
                sys.exit(1)
            except OSError:
                print(f"OS error occurred trying to open {self.name}")
                sys.exit(1)
            except Exception as err:
                print(f"Unexpected error opening {self.name} is", repr(err))
                sys.exit(1)
            line = f.readline()
            p1 = line.split()
            #GITeps H VECTORIAL 180129190239  3.20   42.931   13.114     8.70
            self.programName = p1[0]
            self.cmp = p1[1]
            self.mean = p1[2]
            self.Id0 = p1[3]
            self.Ml = float(p1[4])
            self.Lat = float(p1[5])
            self.Lon = float(p1[6])
            self.Dpt = float(p1[7])
            Lines = f.readlines()
            basename = os.path.basename(self.name)
            if basename.split('.')[1] == 'app_source':
                head = Lines[0]
                table = Lines[1:]
                self.app_source = pd.DataFrame(columns=head.split(), 
                      data=[row.split() for row in table])
            else:
                ncount = 0
                for line1 in Lines:
                    ncount += 1
                    if ncount > 1:
                        line1 = line1.rstrip()
                        p1 = line1.split()
                        fr.append(float(p1[0]))
                        amp.append(float(p1[1]))
                        if self.programName == "CorrectFas":
                            std.append(float(p1[2]))
                            nn.append(float(p1[3]))
                        else:
                            std.append(0.0)
                            nn.append(0)                      
                self.freq = np.asarray(fr)
                self.val = np.asarray(amp)
                self.std = np.asarray(std)
                self.nn = np.asarray(nn,dtype=np.int32)
                self.nfreq = len(self.freq)
            f.close()
            del fr
            del amp
        elif resultl == True:
            self.Id0 = f[0]
            self.Ml = float(f[1])
            self.Lat = float(f[2])
            self.Lon = float(f[3])
            self.Dpt = float(f[4])
            self.cmp = f[5]
            self.nsamp = 0
            self.name = ""
            self.nfreq = 0
        else:
            self.Id0 = f.Id0
            self.Ml = f.Ml
            self.Lat = f.Lat
            self.Lon = f.Lon
            self.Dpt = f.Dpt
            self.nsamp = 0
            self.name = ""
            self.nfreq = 0

    def set_val(self, nf):
        self.nfreq = nf
        self.val = np.empty(self.nfreq)
        self.val[:] = np.NaN

    def toString(self, name: object, cm: object, cfg: object) -> object:
        if name:
            s1 = "{:s} {:s} {:s} {:12s} {:5.2f} {:8.3f} {:8.3f} {:8.2f}".format(name, cm, cfg.mean, self.Id0, self.Ml, self.Lat,
                                                                           self.Lon, self.Dpt)
        else:
            s1 = "{:12s} {:5.2f} {:8.3f} {:8.3f} {:8.2f}".format(self.Id0, self.Ml, self.Lat, self.Lon, self.Dpt)
        return s1

    def plotSource(self, name_out, v1, v2,typ):
        f1 = np.asarray(self.freq)
        if typ == "ACC":
            s1 = "Inv: {:s}, Cmp: {:s} Id: {:s}, Ml: {:5.1f} (Acc) ".format(self.programName, self.cmp,self.Id0,self.Ml)
        elif typ == "VEL":
            s1 = "Inv: {:s}, Cmp: {:s} Id: {:s}, Ml: {:5.1f} (Vel) ".format(self.programName, self.cmp,self.Id0,self.Ml)
        else:
            s1 = "Inv: {:s}, Cmp: {:s} Id: {:s}, Ml: {:5.1f} (Dsp) ".format(self.programName, self.cmp,self.Id0,self.Ml)
        if typ == "VEL":
            for ifr in range(self.nfreq):
                v1 = math.pow(10., self.val[ifr])
                v1 = v1 / (2 * math.pi * f1[ifr])
                self.val[ifr] = math.log10(v1)
        elif typ == "DSP":
            for ifr in range(self.nfreq):
                v1 = math.pow(10., self.val[ifr])
                v1 = v1 / ((2 * math.pi * f1[ifr]) * (2 * math.pi * f1[ifr]))
                self.val[ifr] = math.log10(v1)
        d1 = {'x': f1, 'y': self.val}
        d1 = pd.DataFrame(d1)
        d1 = d1.reset_index()
        if v1 != False or v2 != False:
            g = ggplot(d1, aes(x='x', y='y')) + geom_line(size=0.6,color='dodgerblue')
            g = g + scale_x_log10(breaks=[0.3, 1.0, 3.0, 10.0, 30.0])
            g = g + annotation_logticks(sides="b")
            g = g + xlab("Frequency [Hz]") + ylab("Log(Source)")
            g = g + ggtitle(s1)
            g = g + theme_bw()
            if v1:
                print(g)
            if v2:
                if name_out:
                    g.save(filename=name_out, dpi=300)
