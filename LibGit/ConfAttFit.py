#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
import sys
import numpy as np
from yaml.loader import SafeLoader
from pathlib import Path
from LibGit.Attenuation import Attenuation

class ConfAttFit:
    
    def __init__(self,fname,logf,verbose):
        
        self.log = logf
        self.verbose = verbose
        self.job_name = None
        self.plot_graph = False
        self.save_graph = False
        self.f_single = 1
        self.f_step = 0.2
        self.Vs = None
        self.hingfreq = None
        self.k_known = None
        self.gs_model = 1
        self.hd = None
        self.hd1 = None
        self.hd2 = None
        self.hd3 = None
        self.corr_fact_flag = 0
        self.Q0_corr = None
        self.d1 = 20
        self.d2 = 80
        self.Q_type = 1
        self.HF1 = 0.5
        self.HF2 = 15
        self.outformat = "all"
        
        message = {}
        dict_GR = {1:"Rref/R",2:"bilinear",3:"trilinear",4:"4-linear"}
        
        if Path(fname).suffix != '.yaml':
            msg = f" Config File {fname} not yaml extension\n Aborting"
            print(msg)
            sys.exit(1)
        try:
            with open(fname) as f:
                   data = yaml.load(f, Loader=SafeLoader)
        except FileNotFoundError:
            print(f"File {fname} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {fname}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {fname} is", repr(err))
            sys.exit(1)
        s1 = "....... START reading Ymal Configuration File: {:s} ".format(fname) 
        self.log.write(s1)
        self.log.write("\n")
        if verbose: print(s1)
        s1 = "-- Section GLOBAL --"
        self.log.write("\n")
        self.log.write(s1)
        self.log.write("\n")
        if verbose: print(s1)
        self.att_file = data["GLOBAL"]["att_file"]
        s1 = "> File Name: {:s} ".format(self.att_file)
        self.log.write(s1)
        self.log.write("\n")
        if verbose: print(s1)
        self.att_dir = data["GLOBAL"]["att_dir"]
        s1 = "> ATT  dir: {:s} ".format(self.att_dir) 
        self.log.write(s1)
        self.log.write("\n")
        if verbose: print(s1)
        Att = Attenuation(self.att_dir + self.att_file)
        self.job_name = Att.job_name
        self.out_dir = data["GLOBAL"]["out_dir"]
        s1 = "> OUT  dir: {:s} ".format(self.out_dir)
        self.log.write(s1)
        self.log.write("\n")
        if verbose: print(s1) 

        s1 = "-- Section PLOT --"
        self.log.write("\n")
        self.log.write(s1)
        self.log.write("\n")
        self.plot_graph = str(data["PLOT"]["plot_graph"])            
        self.plot_graph = self.plot_graph.upper()
        if self.plot_graph != 'TRUE' and self.plot_graph != 'FALSE':
            self.plot_graph = False
            s1 = "  WARNING INVALID plot_graph: {:s} (options: TRUE or FALSE') reset to default falue: FALSE".format(
                str(self.plot_graph))
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
        else:
            s1 = "> Plot_graph: {:s} ".format(str(self.plot_graph))
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
        if self.plot_graph == 'FALSE':
            self.plot_graph = False
        if self.plot_graph == 'TRUE':
            self.plot_graph = True
        self.save_graph = False
        self.save_graph = str(data["PLOT"]["save_graph"])
        self.save_graph = self.save_graph.upper()
        if self.save_graph != 'TRUE' and self.save_graph != 'FALSE':
            self.save_graph = False
            s1 = "  WARNING INVALID save_graph: {:s} (options: TRUE or FALSE') reset to default falue: FALSE".format(
                str(self.save_graph))
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
        else:
            s1 = "> Save_graph: {:s} ".format(str(self.save_graph))
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
        if self.save_graph == 'FALSE':
            self.save_graph = False
        if self.save_graph == 'TRUE':
            self.save_graph = True
        s1 = "-- Section FREQUENCY GS --"
        self.log.write("\n")
        self.log.write(s1)
        self.log.write("\n")        
        #self.f_single = np.array(data["FREQUENCY GS"]["f_single"])
        #if isinstance(self.f_single, np.ndarray):
        #    pass
        #else:
        #    print("ERROR!: f_single in the configuration file must be a list of frequencies")
        #    sys.exit()
        self.f_step = data["FREQUENCY GS"]["f_step"]
        if type(self.f_step) is float or type(self.f_step) is int:
            pass
        else:
            print('ERROR: unespected value for f_step, check the configuration file')
            sys.exit()
        #self.f_step = np.array(data["FREQUENCY GS"]["f_step"])
        #if isinstance(self.f_step, np.ndarray):
        #    pass
        #else:
        #    print("ERROR!: f_step in the configuration file must be a list of frequencies intervals around each f_single value")
        #    sys.exit()
        #num_freq = len(self.f_single)
        #num_val = len(self.f_step)
        #if  num_freq != num_val: 
        #    print('ERROR! f_single and f_step must be numpy array with the same lenght')    
        #    sys.exit()
        self.freq = data["FREQUENCY GS"]["freq"]
        if type(self.freq) is float or type(self.freq) is int:
            pass
        else:
            print('ERROR: unespected value for freq, check the configuration file')
            sys.exit()
        #if self.freq in self.f_single:
        #    pass
        #else:
        #    print('ERROR! freq: ' + str(self.freq) + ' Hz is not in the f_single array ' + str(self.f_single) + '; check the configuration file')
        message.update({"fsingle": self.freq})
        s1 = "> Frequency used to fit the Geometrical spreading: {:s} ".format(str(self.freq))
        self.log.write(s1)
        self.log.write("\n")  
        if verbose: print(s1)        
        
        s1 = "-- Section GEOMETRICAL SPREADING --"
        self.log.write("\n")
        self.log.write(s1)
        self.log.write("\n")                         
        self.gs_model = data["GEOMETRICAL SPREADING"]["gs_model"]
        if self.gs_model == 1:
            s1 = "> Geometrical Spreading model: {:s}".format(dict_GR[self.gs_model])
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
        elif self.gs_model == 2:
            self.hd = data["GEOMETRICAL SPREADING"]["HD1"]
            s1 = "> Hinge distance [km] for the Bilinear G(R) model: {:s}".format(str(self.hd))
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
        elif  self.gs_model == 3:
            self.hd1 = data["GEOMETRICAL SPREADING"]["HD1"]
            s1 = "> First Hinge distance [km] for the Trilinear G(R) model: {:s}".format(str(self.hd1))
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
            self.hd2 = data["GEOMETRICAL SPREADING"]["HD2"]
            s1 = "> Second Hinge distance [km] for the Trilinear G(R) model: {:s}".format(str(self.hd2))
            self.log.write(s1) 
            self.log.write("\n")
            if verbose: print(s1)
        
        elif  self.gs_model == 4:
            
            self.hd1 = data["GEOMETRICAL SPREADING"]["HD1"]
            s1 = "> First Hinge distance [km] for the 4-linear G(R) model: {:s}".format(str(self.hd1))
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
            self.hd2 = data["GEOMETRICAL SPREADING"]["HD2"]
            s1 = "> Second Hinge distance [km] for the 4-linear G(R) model: {:s}".format(str(self.hd2))
            self.log.write(s1) 
            self.log.write("\n")
            if verbose: print(s1)
            self.hd3 = data["GEOMETRICAL SPREADING"]["HD3"]
            s1 = "> Third Hinge distance [km] for the 4-linear G(R) model: {:s}".format(str(self.hd3))
            self.log.write(s1) 
            self.log.write("\n")
            if verbose: print(s1)                
        else:
            msg = f" Improper Config File {fname}: undefined model for the Geometrical Spreading\n Aborting"
            print(msg)
            sys.exit(1)            

        s1 = "-- Section ANELASTIC ATTENUATION --"
        self.log.write("\n")
        self.log.write(s1)
        self.log.write("\n")            
        self.case = data["ANELASTIC ATTENUATION"]["case"]
        dict_case = {1: "Fitting the non-parametric Attenuation by considering a  '" +  dict_GR[self.gs_model]   + "' term for the Geometrical Spreading multiplied by an exponential term accounting for the anelastic attenuation", 2: "Fitting the non-parametric Attenuation by considering a  '" +  dict_GR[self.gs_model]   + "' term for the Geometrical Spreading multiplied by an exponential term accounting for the anelastic attenuation and an exponential term accounting for kappa",3: "Fitting the non-parametric Attenuation by considering a  '" +  dict_GR[self.gs_model]   + "' term for the Geometrical Spreading multiplied by an exponential term accounting for the anelastic attenuation and an exponential term accounting for known kappa"}

        if self.case == 1:
            self.log.write("> " + dict_case[self.case]) 
            self.log.write("\n")
            if verbose: print("> " + dict_case[self.case])
            s1 = "> No kappa correction is applied"
            self.log.write(s1)
            self.log.write("\n")
            if verbose:print(s1)
            self.hingfreq = None
        elif self.case == 2:
            self.log.write("> " + dict_case[self.case]) 
            self.log.write("\n")
            if verbose: print("> " + dict_case[self.case])
            self.hingfreq = data["KAPPA"]["hingfreq"]
            s1 = "> Lower frequeny bound [Hz] to use above which to estimate kappa: {:s} ".format(str(self.hingfreq))
            self.log.write(s1)
            self.log.write("\n")  
            if verbose: print(s1)
        elif self.case == 3:
            self.log.write("> " + dict_case[self.case]) 
            self.log.write("\n")
            if verbose: print("> " + dict_case[self.case])
            self.hingfreq = data["KAPPA"]["hingfreq"]
            if type(self.hingfreq) is float or type(self.hingfreq) is int:
                pass
            else:
                print('ERROR! hingfreq must be a number >0, check the configuration file')
                sys.exit()
            if self.hingfreq> 0:
                pass
            else:
                print('ERROR! hingfreq must be a number >0, check the configuration file')
                sys.exit()
            s1 = "> Lower frequeny bound [Hz] to use above which to correct kappa: {:s} ".format(str(self.hingfreq))        
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
            self.k_known =  data["KAPPA"]["k_known"]
            if type(self.k_known) is float or type(self.k_known) is int:
                pass
            else:
                print('ERROR! k_known must be a number >0, check the configuration file')
                sys.exit()
            if self.k_known > 0:
                pass
            else:
                print('ERROR! k_known must be a number >0, check the configuration file')
                sys.exit()  
            s1 = "> Kappa value to correct the high frequency attenuation: {:s} ".format(str(self.k_known))
            self.log.write(s1)
            self.log.write("\n")  
            if verbose: print(s1)
        else:
            msg = f" Improper Config File {fname}: undefined model for the Anelastic Attenuation\n Aborting"
            print(msg)
            sys.exit(1)            

        self.corr_fact_flag = data["ANELASTIC ATTENUATION"]["corr_fact_flag"]
        
        if self.corr_fact_flag == 1:
            s1 = "> G(R) corrected for the anelastic attenuation at 1Hz"
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
            self.Q0_corr = data["ANELASTIC ATTENUATION"]["Q0_corr"]
            s1 = "> Q0 value at 1 Hz: {:s} ".format(str(self.Q0_corr))
            self.log.write(s1)
            self.log.write("\n")
            if verbose: print(s1)
        else:
            s1 = "> G(R) NOT corrected for the anelastic attenuation at 1Hz"
            self.log.write(s1)  
            self.log.write("\n")
            if verbose: print(s1)
        
        self.Q_type = data["ANELASTIC ATTENUATION"]["Q_type"]
        
        if self.Q_type == 1:
            s1 = "> linear model for Q(f)"
            self.log.write(s1)  
            self.log.write("\n")
            if verbose: print(s1)
        else:
            s1 = "> trilinear model for Q(f)"
            self.log.write(s1)  
            self.log.write("\n")
            if verbose: print(s1)            
                          
        
        self.HF1 = data["ANELASTIC ATTENUATION"]["HF1"]
        if type(self.HF1) is float or type(self.HF1) is int:
            pass
        else:
            print('ERROR! HF1 must be a number >0, check the configuration file')
            sys.exit()
        if self.HF1 > 0:
            pass
        else:
            print('ERROR! HF1 must be a number >0, check the configuration file')
            sys.exit()
        s1 = "> First Hinge Distance for Q(f)"
        self.log.write(s1)  
        self.log.write("\n")
        if verbose: print(s1)         
        
        self.HF2 = data["ANELASTIC ATTENUATION"]["HF2"]
        if type(self.HF2) is float or type(self.HF2) is int:
            pass
        else:
            print('ERROR! HF2 must be a number >0, check the configuration file')
            sys.exit()
        if self.HF2 > 0:
            pass
        else:
            print('ERROR! HF2 must be a number >0, check the configuration file')
            sys.exit()
        s1 = "> Second Hinge Distance for Q(f)"
        self.log.write(s1)  
        self.log.write("\n")
        if verbose: print(s1)       
        
        self.d1 = data["ANELASTIC ATTENUATION"]["dist_min_Q"]
        s1 = "> Minimum distance [km] to fit the Q parameter : {:s} ".format(str(self.d1))
        self.log.write(s1)
        self.log.write("\n")
        if verbose: print(s1)
        self.d2 = data["ANELASTIC ATTENUATION"]["dist_max_Q"]
        s1 = "> Maximum distance [km] to fit the Q parameter : {:s} ".format(str(self.d2))
        self.log.write(s1)
        self.log.write("\n") 
        if verbose: print(s1)     
        self.Vs = data["ANELASTIC ATTENUATION"]["Vs"]
        if type(self.Vs) is float or type(self.Vs) is int:
            pass
        else:
            print('ERROR! Vs must be a float or an integer number, check the configuration file')
            sys.exit()
        if self.Vs >0:
            pass
        else:
            print('ERROR! Vs must be a number > 0, check the configuration file')
        s1 = "> Shear wave velocity [km/s] in source region, used for source spectral fits: {:s} ".format(str(self.Vs))
        self.log.write(s1)
        self.log.write("\n")  
        if verbose: print(s1)
