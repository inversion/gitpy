#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gc
import math
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import lsqr
from numpy import count_nonzero
from LibGit.utilities import get_site_val

class FASInv:
    
    def __init__(self, log_file, cfg, GitRes, inp):
        self.logf = log_file
        self.cfg = cfg
        self.GitRes = GitRes
        self.Inp = inp
        
    def DoInversion(self,nf):
        gc.collect()
        row = []
        col = []
        dtmat = []
        bb = []
        s1 = "....... START Inversion Freq: {:7.2f} Hz  Nf: {:d} ".format(self.GitRes.LSQR_FREQ[nf], nf + 1)
        self.logf.write(s1)
        self.logf.write("\n")
        print(s1)
        Tab = self.Inp.Tables[nf]
        
        evt = Tab[['Id']]
        evt = evt.drop_duplicates('Id', keep='last')
        evt = evt.drop_duplicates()
        evt = evt['Id'].astype(str)
        nevt = len(evt)
        ind_evt = np.zeros(shape=(nevt), dtype=int)
        ind_evt[:] = -1
        for i in range(len(evt)):
            id0 = evt.iloc[i]
            if len(id0) != 11:
                pass
            else:
                id0 = "0" + id0
            ns = self.GitRes.EVT_ID.index(id0)
            ind_evt[i] = ns
        sta = Tab[['FDSN']]
        sta = sta.drop_duplicates('FDSN', keep='last')
        sta = sta.drop_duplicates()
        sta = sta['FDSN'].astype(str)
        nsta = len(sta)
        ind_sta = np.zeros(shape=(nsta), dtype=int)
        ind_sta[:] = -1
        for i in range(len(sta)):
            stat = sta.iloc[i]
            ns = self.GitRes.STA_ID.index(stat)
            ind_sta[i] = ns
        ninc = self.cfg.nbin_dist + nevt + nsta
        s1 = "        Nstat: {:d} Nevt: {:d} Nbin_Dist: {:d} Tot Un: {:d}".format(nsta, nevt, self.cfg.nbin_dist, ninc)
        self.logf.write(s1)
        self.logf.write("\n")
        print(s1)
        # # DATA KERNEL
        nrow = 0
        sta = sta.reset_index()
        evt = evt.reset_index()
        for i in range(len(Tab)):
            val = 0
            if self.cfg.cmp == 'Z':
                val = Tab.iloc[i].FAS_Z * self.cfg.scale
            elif self.cfg.cmp == 'NS':
                val = Tab.iloc[i].FAS_NS * self.cfg.scale
            elif self.cfg.cmp == 'EW':
                val = Tab.iloc[i].FAS_EW * self.cfg.scale
            else:
                if self.cfg.mean == 'VECTORIAL':
                    EW = (Tab.iloc[i].FAS_EW * self.cfg.scale) * (Tab.iloc[i].FAS_EW * self.cfg.scale)
                    NS = (Tab.iloc[i].FAS_NS * self.cfg.scale) * (Tab.iloc[i].FAS_NS * self.cfg.scale)
                    val = math.sqrt((EW + NS))
                else:
                    val = math.sqrt((Tab.iloc[i].FAS_EW * self.cfg.scale) * (Tab.iloc[i].FAS_NS * self.cfg.scale))
    
            d = Tab.iloc[i].Dipo
            bb.append(math.log10(val))
            p1 = next(x[0] for x in enumerate(self.cfg.dist_bin) if x[1] > d)
            d0 = self.cfg.dist_bin[p1 - 1]
            dd = self.cfg.dist_bin[p1] - self.cfg.dist_bin[p1 - 1]
            w0 = 1 - (d - d0) / dd
            w1 = 1 - w0
            #      Kernel Distance
            row.append(nrow)
            col.append(p1 - 1)
            dtmat.append(w0)
            row.append(nrow)
            col.append(p1)
            dtmat.append(w1)
            Id0 = Tab.iloc[i].Id
            Id0 = str(Id0)
            indevt = evt[evt['Id'] == Id0].index
            nevt0 = indevt.values[0]
            Sta = Tab.iloc[i].FDSN
            indsta = sta[sta['FDSN'] == Sta].index
            nsta0 = indsta.values[0]
            if nsta0 < 0:
                print(str(nsta0) + ' <0')
            #      Kernel nevt    
            coln = self.cfg.nbin_dist + nevt0
            row.append(nrow)
            col.append(coln)
            dtmat.append(1)
            #      Kernel stat
            coln = self.cfg.nbin_dist + nevt + nsta0
            row.append(nrow)
            col.append(coln)
            dtmat.append(1)
            nrow = nrow + 1
        # END DATA KERNEL
        s1 = "        End data kernel nrow: {:d}  len(b): {:d} ".format(nrow, len(bb))
        nrow_data_kernel = nrow
        self.logf.write(s1)
        self.logf.write("\n")
        print(s1)
        nref_site = 0
        weight = 0
        for i in range(self.cfg.nsites):
            staz = self.cfg.REF_SITE[i]
            val = staz in sta.values
            if val == True:
                nref_site = nref_site + 1
                weight = 1. / nref_site
        s1 = "        Site constraints, Iref: {:d}  Num ref sites: {:d}, Total sites:  {:d}".format(round(self.cfg.iref),
                                                                                                           nref_site,
                                                                                                           self.cfg.nsites)
        self.logf.write(s1)
        self.logf.write("\n")
        print(s1)
        value = get_site_val(self.cfg, self.GitRes.LSQR_FREQ[nf])
        bb.append(value * self.cfg.weight_site)
        for i in range(self.cfg.nsites):
            staz = self.cfg.REF_SITE[i]
            val = staz in sta.values
            if val == True:
                indsta = sta[sta['FDSN'] == self.cfg.REF_SITE[i]].index
                nsta0 = indsta.values[0]
                coln = self.cfg.nbin_dist + nevt + nsta0
                row.append(nrow)
                col.append(coln)
                dtmat.append(weight * self.cfg.weight_site)
        #      print(" row ",weight*cfg.weight_site)
        nrow = nrow + 1
        s1 = "        End site constraints nrow: {:d}  len(b): {:d} ".format(nrow, len(bb))
        self.logf.write(s1)
        self.logf.write("\n")
        print(s1)
        # END SITE CONSTRAINT
        # REFERENCE DISTANCE CONSTRAINT
        bb.append(self.cfg.vdref * self.cfg.weight_dref)
        p1 = next(x[0] for x in enumerate(self.cfg.dist_bin) if x[1] > self.cfg.dref)
        d0 = self.cfg.dist_bin[p1 - 1]
        delta = self.cfg.dist_bin[p1] - self.cfg.dist_bin[p1 - 1]
        diff = self.cfg.dist_bin[p1] - self.cfg.dref
        v1 = (diff / delta) * self.cfg.weight_dref
        dtmat.append(v1)
        row.append(nrow)
        col.append(p1 - 1)
        v1 = (1 - (diff / delta)) * self.cfg.weight_dref
        dtmat.append(v1)
        row.append(nrow)
        col.append(p1)
        nrow = nrow + 1
        s1 = '        End  dref constraint nrow: {:d}  len(b): {:d} '.format(nrow, len(bb))
        self.logf.write(s1)
        self.logf.write("\n")
        print(s1)
        # END REFERENCE DISTANCE CONSTRAINT
        cnt = 1
        while cnt <= self.cfg.nbin_dist - 2:
            indx = cnt
            diff21 = self.cfg.dist_bin[indx] - self.cfg.dist_bin[indx - 1]
            diff32 = self.cfg.dist_bin[indx + 1] - self.cfg.dist_bin[indx]
            diff31 = self.cfg.dist_bin[indx + 1] - self.cfg.dist_bin[indx - 1]
            v1 = (-diff32 / diff31) * self.cfg.weight_smo
            v2 = 1 * self.cfg.weight_smo
            v3 = (-diff21 / diff31) * self.cfg.weight_smo
            col.append(cnt - 1)
            dtmat.append(v1)
            row.append(nrow)
            col.append(cnt)
            dtmat.append(v2)
            row.append(nrow)
            col.append(cnt + 1)
            dtmat.append(v3)
            row.append(nrow)
            nrow = nrow + 1
            bb.append(0 * self.cfg.weight_smo)
            cnt = cnt + 1
        s1 = "        End  Bin  Distance Smooth: {:d}  len(b): {:d} ".format(nrow, len(bb))
        self.logf.write(s1)
        self.logf.write("\n")
        print(s1)
        dtmat = np.array(dtmat)
        row = np.array(row)
        col = np.array(col)
        bb = np.array(bb)
        A = csr_matrix((dtmat, (row, col)), shape=(nrow, ninc)).toarray()
        if A.size > 0:
            sparsity = 1.0 - (float(count_nonzero(A)) / float(A.size))
            sparsity = sparsity * 100
        else:
            sparsity = 0
        s1 = "        End Sparse Matrix construction, nrow: {:10d} ncol: {:10d} Sparsity: {:6.1f} % len(b): {:d}".format(
            nrow, ninc, sparsity, len(bb))
        self.logf.write(s1)
        self.logf.write("\n")
        print(s1)
        if nrow_data_kernel > self.cfg.nrow_min:
            s1 = "        LSQR INVERSION...."
            self.logf.write(s1)
            self.logf.write("\n")
            print(s1)
            x, istop, itn, r1norm = lsqr(A, bb, damp=1e-10, atol=1e-08, btol=1e-08, conlim=100000000.0,
                                                             iter_lim=self.cfg.iter_lim, show=False, calc_var=False, x0=None)[:4]
            s1 = "        End LSQR INVERSION Stop Cond: {:d} Niter: {:d} R1norm: {:7.3f}".format(istop, itn, r1norm)
            # `     x, residuals, rank, s = np.linalg.lstsq(A,bb)
            self.logf.write(s1)
            self.logf.write("\n")
            print(s1)
            inv = True
            i = 0
            while i < nevt:
                nv = ind_evt[i]
                Id1 = self.GitRes.EVT_ID[nv]
                Id2 = self.GitRes.evtGit[nv].Id0
                self.GitRes.evtGit[nv].val[nf] = x[i + self.cfg.nbin_dist]
                Id0 = evt.iloc[i]['Id']
                i = i + 1
            i = 0
            while i < nsta:
                ns = ind_sta[i]
                St1 = self.GitRes.STA_ID[ns]
                St2 = self.GitRes.staGit[ns].FDSN
                self.GitRes.staGit[ns].val[nf] = x[i + self.cfg.nbin_dist + nevt]
                St0 = sta.iloc[i]['FDSN']
                i = i + 1
            self.GitRes.add_ResultsLsqr(x)
        else:
            s1 = "        WARNING  nrow_data_kernel: {:d} <  nrow_min (cfg): {:d}  Inversion = False ".format(
                nrow_data_kernel, self.cfg.nrow_min)
            self.logf.write(s1)
            self.logf.write("\n")
            print(s1)
            x = np.zeros(shape=(ninc))
            x[:] = np.NaN
            self.GitRes.add_ResultsLsqr(x)
            inv = False
        s1 = "....... END Inversion Freq: {:7.2f} Hz".format(self.GitRes.LSQR_FREQ[nf])
        self.logf.write(s1)
        self.logf.write("\n")
        print(s1)
        del ind_evt, ind_sta, evt, sta
        del row
        del col
        del bb
        del A
        del dtmat