#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit,OptimizeWarning
from plotnine import aes, ggplot, annotate,ggtitle,geom_line, theme_bw, xlab, ylab, scale_color_distiller,geom_vline
   
class GeomSpread:
    
    def __init__(self,Att_single_f,d_ref,dist_bin):
        
        self.Att = Att_single_f
        self.rref=d_ref
        self.dist_bin = dist_bin
        self.h1=None
        self.h2=None
        self.h3=None

    def setGRRrefSuR(self):
        
        #return np.log10(self.rref/np.array(self.dist_bin))
        dict = {"coeff":{"Rref":float(self.rref), "n1": None, "n2": None, "n3": None},
                "GR1": {"X": self.dist_bin,"Y": np.log10(self.rref/np.array(self.dist_bin))},
                "GR2" : {"X": self.dist_bin,"Y": np.log10(np.power(self.rref,2)/np.power(np.array(self.dist_bin),2))}}
        return dict
    
    def getGRRrefSuR(self,Att,GR):
        
        d1 = {'x': Att.dist, 'y': GR, 'TY': 1,'FR': 1,'Color':'k', 'Type': "Rref/R"}
        GR_df = pd.DataFrame(d1)
        return GR_df

    def getGRRrefSuR2(self,Att,GR):
        
        d1 = {'x': Att.dist, 'y': GR, 'TY': 1,'FR': 1,'Color':'k', "Type": "(Rref/R)^2"}
        GR_df = pd.DataFrame(d1)
        return GR_df

    def getGRRrefSuRn(self,Att,GR):
        
        d1 = {'x': Att.dist, 'y': GR, 'TY': 1,'FR': 1,'Color':'k', "Type": "(Rref/R)^n"}
        GR_df = pd.DataFrame(d1)
        return GR_df
        
    def getGRdf (self,GR,freq,type):
        
        X = GR.get("GR").get("X")
        Y = GR.get("GR").get("Y")
        for i in range(len(X)):
            d1 = {'x': X[i], 'y': Y[i], 'TY': str(freq), 'FR': float(freq),"Type": type}
            d1 = pd.DataFrame(d1, index=[i])
            if i == 0:
                GRmod = d1
            else:
                 #   GRmod = GRmod.append(d1)
                GRmod=pd.concat([GRmod,d1])
            del d1    
        return GRmod
    
    def setGRtrilinear (self,h1,h2):
        self.h1=h1
        self.h2=h2
        new_ATT = self.Att
        # to select three distance ranges whith two hinge distances HD1 and HD2
        below_HD1 = new_ATT[new_ATT["x"] <= self.h1]
        between_HD1_HD2 = new_ATT[new_ATT["x"].between(self.h1,self.h2)]
        above_HD2 = new_ATT[new_ATT["x"] >= self.h2] 
 
        #fit below HD1 
        xdata_1 = np.unique(below_HD1[["x"]].to_numpy(dtype=np.dtype).reshape(-1)) 
        ydata_1 = list()
        for d in xdata_1:
            ydata_1.append(np.mean(below_HD1.loc[below_HD1['x']==d]['y'].to_numpy(dtype=np.dtype).reshape(-1)))

        def fit_fun_1(x,n1,rref):
            rref=float(self.rref)
            y = pow((rref/x),n1)
            yy = []
            for i in range(len(y)):
                yy.append(math.log10(y[i]))
            return yy
        try:
            popt_1, pcov_1 = curve_fit(fit_fun_1, xdata_1, ydata_1, method='trf', maxfev=2000)
            error_1 = np.sqrt(np.diag(pcov_1))
        except OptimizeWarning as e:
            print (str(e))
        
        Y_1 = fit_fun_1(xdata_1,*popt_1)
        X_1 = xdata_1
        
        #fit between HD1 and HD2
        xdata_2 = np.unique(between_HD1_HD2[["x"]].to_numpy(dtype=np.dtype).reshape(-1))
        ydata_2 = list()
        for d in xdata_2:
            ydata_2.append(np.mean(between_HD1_HD2.loc[between_HD1_HD2['x']==d]['y'].to_numpy(dtype=np.dtype).reshape(-1)))
            
        def fit_fun_2 (x,n1,n2,rref,HD1):
            rref = float(self.rref)
            n1 = popt_1[0]
            HD1 = self.h1

            exp_1 = pow(rref/HD1,n1)
            exp_2 = pow(HD1/x,n2)
            y = exp_1*exp_2       
            yy = []
            for i in range(len(y)):
                yy.append(math.log10(y[i]))
            return yy          

        popt_2, pcov_2 = curve_fit(fit_fun_2, xdata_2, ydata_2, method='trf', maxfev=2000)
        error_2 = np.sqrt(np.diag(pcov_2))
        Y_2 = fit_fun_2(xdata_2,*popt_2)
        X_2 = xdata_2 
                            
        #fit above HD2
        xdata_3 = np.unique(above_HD2[["x"]].to_numpy(dtype=np.dtype).reshape(-1))
        ydata_3 = list()
        for d in xdata_3:
            ydata_3.append(np.mean(above_HD2.loc[above_HD2['x']==d]['y'].to_numpy(dtype=np.dtype).reshape(-1)))
            
        def fit_fun_3 (x,n1,n2,n3,rref,HD1,HD2):
            rref = float(self.rref)
            n1 = popt_1[0]
            n2 = popt_2[1]
            HD1 = self.h1
            HD2 = self.h2
            
            exp_1 = pow(rref/HD1,n1)
            exp_2 = pow(HD1/HD2,n2)
            exp_3 = pow(HD2/x,n3)            
            y = exp_1*exp_2*exp_3          
            yy = []
            for i in range(len(y)):
                yy.append(math.log10(y[i]))
            return yy            
 
        popt_3, pcov_3 = curve_fit(fit_fun_3, xdata_3, ydata_3, method='trf', maxfev=2000)
        error_3 = np.sqrt(np.diag(pcov_3))
        Y_3 = fit_fun_3(xdata_3,*popt_3)
        X_3 = xdata_3                             

        X = np.append(X_1, X_2); X = np.append(X,X_3)
        Y = np.append(Y_1, Y_2); Y = np.append(Y,Y_3)
        dict = {"coeff":{"Rref":float(self.rref), "n1": float(popt_1[0]), "n2": float(popt_2[1]), "n3": float(popt_3[2]), "err_n1": float(error_1[0]), "err_n2": float(error_2[1]), "err_n3": float(error_3[2]),"h1": float(h1),"h2": float(h2)},
                "GR": {"X": X,"Y": Y}}
        return dict        
                                             
    def setGRbilinear(self,h):
        
        self.h=h
        new_ATT = self.Att#[self.Att["FR"]==1]
        new_ATT.loc[new_ATT["x"] == 0, "x"] = 0.01
        above_HD = new_ATT[new_ATT["x"] > self.h]
        below_HD = new_ATT[new_ATT["x"] <= self.h]
        #fit below HD
        xdata_1 = np.unique(below_HD[["x"]].to_numpy(dtype=np.dtype).reshape(-1))
        #ydata_1 = below_HD[["y"]].to_numpy(dtype=np.dtype).reshape(-1)
        ydata_1 = list()
        for d in xdata_1:
            ydata_1.append(np.mean(below_HD.loc[below_HD['x']==d]['y'].to_numpy(dtype=np.dtype).reshape(-1)))
        def fit_fun_1(x,n1,rref):
            rref=float(self.rref)
            y = pow((rref/x),n1)
            yy = []
            for i in range(len(y)):
                yy.append(math.log10(y[i]))
            return yy
        

        popt_1, pcov_1 = curve_fit(fit_fun_1, xdata_1, ydata_1, method='trf', maxfev=2000)
        error_1 = np.sqrt(np.diag(pcov_1))
        Y_1 = fit_fun_1(xdata_1,*popt_1)
        X_1 = xdata_1
        
        #fit above HD
        xdata_2 = np.unique(above_HD[["x"]].to_numpy(dtype=np.dtype).reshape(-1))
        #ydata_2 = above_HD[["y"]].to_numpy(dtype=np.dtype).reshape(-1)
        ydata_2 = list()
        for d in xdata_2:
            ydata_2.append(np.mean(above_HD.loc[above_HD['x']==d]['y'].to_numpy(dtype=np.dtype).reshape(-1)))

        def fit_fun_2 (x,n1,n2,rref,HD):
            rref = float(self.rref)
            n1 = popt_1[0]
            HD = self.h=h
            exp_1 = pow(rref/HD,n1)
            exp_2 = pow(HD/x,n2)            
            y = exp_1*exp_2      
            yy = []
            for i in range(len(y)):
                yy.append(math.log10(y[i]))
                
            return yy            
 
        popt_2, pcov_2 = curve_fit(fit_fun_2, xdata_2, ydata_2, method='trf', maxfev=2000)
        error_2 = np.sqrt(np.diag(pcov_2))
        Y_2 = fit_fun_2(xdata_2,*popt_2)
        X_2 = xdata_2
        
        X = []; Y = []
        X = np.concatenate((X_1,X_2),axis=None)  
        Y = np.concatenate((Y_1,Y_2),axis=None)  
              
        #return [popt_1[0],popt_2[1]]
        dict = {"coeff":{"Rref":float(self.rref), "n1": float(popt_1[0]), "n2": float(popt_2[1]), "err_n1": float(error_1[0]), "err_n2": float(error_2[1]),"h": float(h)},
                "GR": {"X": X,"Y": Y}}
        return dict

    def setGR4linear(self,h1,h2,h3):

        self.h1=h1
        self.h2=h2
        self.h3=h3
        new_ATT = self.Att#[self.Att["FR"]==1]
        print(new_ATT)
        below_HD1 = new_ATT[new_ATT["x"] <= self.h1]
        between_HD1_HD2 = new_ATT[new_ATT["x"].between(self.h1,self.h2)]
        between_HD2_HD3 = new_ATT[new_ATT["x"].between(self.h2,self.h3)]
        above_HD3 = new_ATT[new_ATT["x"] >= self.h3] 
         
        #fit below HD1 
        xdata_1 = np.unique(below_HD1[["x"]].to_numpy(dtype=np.dtype).reshape(-1))  
        ydata_1 = list()
        for d in xdata_1:
            ydata_1.append(np.mean(below_HD1.loc[below_HD1['x']==d]['y'].to_numpy(dtype=np.dtype).reshape(-1)))
        
        def fit_fun_1(x,n1,rref):
            rref=float(self.rref)
            y = pow((rref/x),n1)
            yy = []
            for i in range(len(y)):
                yy.append(math.log10(y[i]))
            return yy
        try:
            popt_1, pcov_1 = curve_fit(fit_fun_1, xdata_1, ydata_1, method='trf', maxfev=2000)
            error_1 = np.sqrt(np.diag(pcov_1))
        except OptimizeWarning as e:
            print (str(e))
        
        Y_1 = fit_fun_1(xdata_1,*popt_1)
        X_1 = xdata_1
        
        #fit between HD1 and HD2
        xdata_2 = np.unique(between_HD1_HD2[["x"]].to_numpy(dtype=np.dtype).reshape(-1))
        ydata_2 = list()
        for d in xdata_2:
            ydata_2.append(np.mean(between_HD1_HD2.loc[between_HD1_HD2['x']==d]['y'].to_numpy(dtype=np.dtype).reshape(-1)))

        def fit_fun_2 (x,n1,n2,rref,HD1):
            rref = float(self.rref)
            n1 = popt_1[0]
            HD1 = self.h1

            exp_1 = pow(rref/HD1,n1)
            exp_2 = pow(HD1/x,n2)
            y = exp_1*exp_2       
            yy = []
            for i in range(len(y)):
                yy.append(math.log10(y[i]))
            return yy          

        popt_2, pcov_2 = curve_fit(fit_fun_2, xdata_2, ydata_2, method='trf', maxfev=2000)
        error_2 = np.sqrt(np.diag(pcov_2))
        Y_2 = fit_fun_2(xdata_2,*popt_2)
        X_2 = xdata_2 

        #fit between HD2 and HD3
        xdata_3 = np.unique(between_HD2_HD3[["x"]].to_numpy(dtype=np.dtype).reshape(-1))
        ydata_3 = list()
        for d in xdata_3:
            ydata_3.append(np.mean(between_HD2_HD3.loc[between_HD2_HD3['x']==d]['y'].to_numpy(dtype=np.dtype).reshape(-1)))

        def fit_fun_3 (x,n1,n2,n3,rref,HD1,HD2):
            rref = float(self.rref)
            n1 = popt_1[0]
            n2 = popt_2[1]
            HD1 = self.h1
            HD1 = self.h2
            exp_1 = pow(rref/HD1,n1)
            exp_2 = pow(HD1/HD2,n2)
            exp_3 = pow(HD2/x,n3)            
            y = exp_1*exp_2*exp_3 
            yy = []
            for i in range(len(y)):
                yy.append(math.log10(y[i]))
            return yy          

        popt_3, pcov_3 = curve_fit(fit_fun_3, xdata_3, ydata_3, method='trf', maxfev=2000)
        error_3 = np.sqrt(np.diag(pcov_3))
        Y_3 = fit_fun_3(xdata_3,*popt_3)
        X_3 = xdata_3 

        #fit above HD3
        xdata_4 = np.unique(above_HD3[["x"]].to_numpy(dtype=np.dtype).reshape(-1))
        ydata_4 = list()
        for d in xdata_4:
            ydata_4.append(np.mean(above_HD3.loc[above_HD3['x']==d]['y'].to_numpy(dtype=np.dtype).reshape(-1)))

        def fit_fun_4 (x,n1,n2,n3,n4,rref,HD1,HD2,HD3):
            rref = float(self.rref)
            n1 = popt_1[0]
            n2 = popt_2[1]
            n3 = popt_3[2]
            HD1 = self.h1
            HD2 = self.h2
            HD3 = self.h3
            exp_1 = pow(rref/HD1,n1)
            exp_2 = pow(HD1/HD2,n2)
            exp_3 = pow(HD2/HD3,n3)
            exp_4 = pow(HD3/x,n4)            
            y = exp_1*exp_2*exp_3*exp_4       
            yy = []
            for i in range(len(y)):
                yy.append(math.log10(y[i]))
            return yy            
 
        popt_4, pcov_4 = curve_fit(fit_fun_4, xdata_4, ydata_4, method='trf', maxfev=2000)
        error_4 = np.sqrt(np.diag(pcov_4))
        Y_4 = fit_fun_4(xdata_4,*popt_4)
        X_4 = xdata_4                             

        X = np.append(X_1, X_2); X = np.append(X,X_3); X = np.append(X,X_4)
        Y = np.append(Y_1, Y_2); Y = np.append(Y,Y_3); Y = np.append(Y,Y_4)

        dict = {"coeff":{"Rref":float(self.rref), "n1": float(popt_1[0]), "n2": float(popt_2[1]), "n3": float(popt_3[2]), "n4": float(popt_4[3]), "err_n1": float(error_1[0]), "err_n2": float(error_2[1]), "err_n3": float(error_3[2]), "err_n4": float(error_4[3]),"h1": float(h1),"h2": float(h2),"h3": float(h3)},
                "GR": {"X": X,"Y": Y}}
        return dict        

    def plotGRmodel (self,GR,GR_,Att,Att_single_f,freq,name_out,v1,v2,opts):
        fmin=Att.freq[0]
        fmax=Att.freq[Att.nfreq-1]
        lm = []
        lm = [fmin,fmax]

        stit = "Git: {:s}, Job Name: {:s}, CMP: {:s} Freq: {:5.1f} ".format(Att.progname,Att.job_name,Att.cmp,freq)
        ndist=len(Att.dist)
        xmax= Att.dist[ndist-1]
        xmax=math.log10(xmax)

        g = ggplot()
        g=  g +  geom_line(Att_single_f, aes(x='x', y='y', group='TY',color='FR'),size=1.0,linetype="dotted")
        g = g +  geom_line(GR, aes(x='x', y='y', group='TY',color='FR',linetype="Type"),size=1)
        if 'h' in GR_['coeff']: g = g + geom_vline(xintercept = GR_['coeff']['h'], linetype="dotted", color = "grey", size=1.0) + annotate(geom="label",x=GR_['coeff']['h'], y=0, label=GR_['coeff']['h'], fill = 'white')
        if 'h1' in GR_['coeff']: g = g + geom_vline(xintercept = GR_['coeff']['h1'], linetype="dotted", color = "grey", size=1.0)\
            + annotate(geom="label",x=GR_['coeff']['h1'], y=0, label=GR_['coeff']['h1'], fill = 'white')
        if 'h2' in GR_['coeff']: g = g + geom_vline(xintercept = GR_['coeff']['h2'], linetype="dotted", color = "grey", size=1.0)+ annotate(geom="label",x=GR_['coeff']['h2'], y=0, label=GR_['coeff']['h2'], fill = 'white')
        if 'h3' in GR_['coeff']: g = g + geom_vline(xintercept = GR_['coeff']['h3'], linetype="dotted", color = "grey", size=1.0)+ annotate(geom="label",x=GR_['coeff']['h3'], y=0, label=GR_['coeff']['h3'], fill = 'white')
        g=  g + scale_color_distiller(name='Freq  [Hz]',palette=9,type='div',limits=lm)
        g=  g + xlab("Hypocentral Distance [km]")  +  ylab("Log(Attenuation)")
        g=  g + ggtitle(stit)
        g=  g+theme_bw()
        if v1 != False and opts.configFile != None:
            print("\n")
            print("*********************************")
            print('Please close the figure to go on')
            print("*********************************")
            print("\n") 
            print(g)
        if v1 != False and opts.configFile == None:
            print(g)
        if v2 != False:
            if name_out:
                msg = f" saving graphic file: {name_out} \n"
                verbose = opts.verb
                if verbose: print(msg)
                g.save(filename=name_out, dpi=300,verbose = False)