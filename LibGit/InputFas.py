#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import gc
import glob
import os
import sys
import pandas as pd
from pathlib import Path

def check_Eve(ConfigFile,opts,logID):
    if ConfigFile.eve_red == 'NONE':
        EvID = []
    else:    
        if Path(ConfigFile.eve_red).suffix != '.txt':
            msg = f" Config File {ConfigFile.eve_red} not txt extension\n Aborting"
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")
            sys.exit(1)
        try:
            with open(ConfigFile.eve_red) as f: 
                lines = f.readlines()[1:]
                EvID = []
                if len(lines) >=1:
                    for row in lines:
                        row = row.rstrip('\n')
                        row = row.split(';')
                        EvID.append(row[1])
            f.close()

        except FileNotFoundError:
            msg = f"File {ConfigFile.eve_red} not found.  Aborting"
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")        
            return None,None
        except OSError:
            msg = f"OS error occurred trying to open {ConfigFile.eve_red}"
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")        
            sys.exit(1)
        except Exception as err:
            msg = f"Unexpected error opening {ConfigFile.eve_red} is", repr(err)
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")
            sys.exit(1)
    return EvID

def check_Sta(ConfigFile,opts,logID):
    if ConfigFile.sta_red == 'NONE':
        StaID = []
    else:
        if Path(ConfigFile.sta_red).suffix != '.txt':
            msg = f" Config File {ConfigFile.sta_red} not txt extension\n Aborting"
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")
            sys.exit(1)
        try:
            with open(ConfigFile.sta_red) as f: 
                lines = f.readlines()[1:]
                StaID = []
                if len(lines)>=1:
                    for row in lines:
                        row = row.rstrip('\n')
                        row = row.split(';')
                        StaID.append(row[1])
            f.close()

        except FileNotFoundError:
            msg = f"File {ConfigFile.sta_red} not found.  Aborting"
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")        
            return None,None
        except OSError:
            msg = f"OS error occurred trying to open {ConfigFile.sta_red}"
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")        
            sys.exit(1)
        except Exception as err:
            msg = f"Unexpected error opening {ConfigFile.sta_red} is", repr(err)
            if opts.verb == True: print(msg)
            logID.write("\n")
            logID.write(msg)
            logID.write("\n")
            sys.exit(1)
    return StaID

class FasName:
    def __init__(self, nm, f0):
        self.freq = float(f0)
        self.name = nm

class InputFas:
    def __init__(self, cfg, logf, GitR, progname):
        """

        :type logf: object
        """
        self.log = logf
        self.GitRes = GitR
        fas = list()
        s1 = "....... START reading FAS Dir: {:s} ".format(cfg.fas_dir)
        logf.write(s1)
        logf.write("\n")
        print(s1)
        self.Tables = list()
        os.chdir(cfg.fas_dir)
        fil = "*.fas"
        print("READING FAS: ")
        nskip = 0
        dmax = cfg.dist_bin[cfg.nbin_dist - 1]
        dmin = cfg.dist_bin[0]
        msg = " TOTAL NUMBER OF FAS files (*.fas):  {:10d}".format(len(glob.glob(fil)))
        print(msg)
        logf.write(msg)
        logf.write("\n")
        for file in glob.glob(fil):
            gc.collect()
            name = os.path.join(cfg.fas_dir, file)
            data = pd.read_table(name, header=0, delim_whitespace=True, nrows=1)
            if len(data) >= 1:
                f1 = data.iloc[0].Freq
                if f1 > cfg.fmin and f1 < cfg.fmax:
                    fas.append(FasName(name, f1))
                else:
                    nskip = nskip + 1
            else:
                nskip = nskip + 1
        self.nf = len(fas)
        if self.nf == 0:
            msg = f" ERROR SELECTED FAS files = 0 (*.fas), FAS Dir {cfg.fas_dir} Aborting"
            logf.write(msg)
            logf.write("\n")
            print(msg)
            sys.exit()
        fas.sort(key=lambda x: x.freq, reverse=False)
        self.GitRes.make_list_freq(fas)
        nread = 0
        for i in range(len(fas)):
            name = fas[i].name
            f1 = fas[i].freq
            data = pd.read_table(name, header=0, delim_whitespace=True)
            data = data.loc[(data['Dipo'] > dmin) & (data['Dipo'] < dmax)]
            #   data=data.loc[(data['MLDib'] > cfg.mmin) & (data['MLDib'] < cfg.mmax)]
            data = data.loc[(data['Ml'] > cfg.mmin) & (data['Ml'] < cfg.mmax)]
            if cfg.type_sel == 'FILE': 
                EveID = check_Eve(cfg,True,logf)
                s1 = "removed events: " + str(EveID)
                logf.write(s1)
                logf.write("\n")
                print(s1)
                StaID = check_Sta(cfg,True,logf)
                print(StaID)
                s1 = "removed stations: " + str(StaID)
                logf.write(s1)
                logf.write("\n")
                print(s1)            
                data = data[~data['Id'].isin([float(i) for i in EveID])]
                data = data[~data['FDSN'].isin([i for i in StaID])]

            ndat = len(data)
            s1 = "FAS: {:5d} {:s} {:6.3f} Hz Ndat: {:10d}".format(nread + 1, name, f1, ndat)
            logf.write(s1)
            logf.write("\n")
            print(s1)
            self.Tables.append(data)
            if nread == 0:
                evt = data[['Id', 'Ml', 'EvLat', 'EvLon', 'EvtDpt']]
#                evt = data[['Id0', 'Ml', 'Lat', 'Lon', 'Dpt']]
                #sta = data[['Stat', 'CMP', 'Net', 'StLat', 'StLon', 'StElev']]
                sta = data[['FDSN', 'CMP','StLat', 'StLon', 'StElev']] # modificato 
            else:
#                evt=pd.concat([evt,data[['Id0', 'Ml', 'Lat', 'Lon', 'Dpt']]])
                evt=pd.concat([evt,data[['Id', 'Ml', 'EvLat', 'EvLon', 'EvtDpt']]])
           #     evt = evt.append(data[['Id0', 'Ml', 'Lat', 'Lon', 'Dpt']])
                #sta = sta.append(data[['Stat', 'CMP', 'Net', 'StLat', 'StLon', 'StElev']])
                sta=pd.concat([sta,data[['FDSN', 'CMP','StLat', 'StLon', 'StElev']]])
             #   sta = sta.append(data[['FDSN', 'CMP','StLat', 'StLon', 'StElev']]) # modificato
            nread = nread + 1
#        evt = evt.drop_duplicates('Id0', keep='last')
        evt = evt.drop_duplicates('Id', keep='last')
        ev3 = evt.drop_duplicates()
#        ev3 = ev3[["Id0", "Ml", "Lat", "Lon", "Dpt"]].astype(str, float)
        ev3 = ev3[["Id", "Ml", "EvLat", "EvLon", "EvtDpt"]].astype(str, float)
        ind = []
        for i in range(len(cfg.REF_SITE)):
            if cfg.REF_SITE[i] in StaID:
                ind.append(i)
            #cfg.REF_SITE.remove(cfg.REF_SITE[ind])
        print(len(cfg.REF_SITE))
        for i in range(len(ind)):
            cfg.REF_SITE.remove(cfg.REF_SITE[i])
        print(len(cfg.REF_SITE))
        self.GitRes.make_list_evt(ev3, cfg.cmp)
        s1 = "....... TOTAL NUMBER OF EVENTS: {:d}".format(len(self.GitRes.evtGit))
        logf.write(s1)
        logf.write("\n")
        print(s1)
        s1 = "    Id        Ml     Lat      Lon      Dpt"
        logf.write(s1)
        logf.write("\n")
        print(s1)
        for i in range(len(self.GitRes.evtGit)):
            s1 = self.GitRes.evtGit[i].toString(None, None, None)
            logf.write(s1)
            logf.write("\n")
            print(s1)
        s1 = "...................................................................."
        logf.write(s1)
        logf.write("\n")
        print(s1)
        sta = sta.drop_duplicates('FDSN', keep='last')
        sta3 = sta.drop_duplicates()
        # sta3 = sta3[['Stat', 'CMP', 'Net', 'StLat', 'StLon', 'StElev']].astype(str, float)
        sta3 = sta3[['FDSN', 'CMP','StLat', 'StLon', 'StElev']].astype(str, float)
        self.GitRes.make_list_sta(sta3, cfg.cmp)
        s1 = "....... TOTAL NUMBER OF STATIONS: {:d} -----------".format(len(self.GitRes.staGit))
        logf.write(s1)
        logf.write("\n")
        print(s1)
        s1 = "FDSN  Channel   Net   Lat      Lon      Elev   RefSite"
        logf.write(s1)
        logf.write("\n")
        print(s1)
        count = 0
        numref = len(cfg.REF_SITE)
        for i in range(len(self.GitRes.staGit)):
            if (cfg.iref == 0):
                self.GitRes.staGit[i].ref = True
                count = count + 1
            else:
                val = (self.GitRes.staGit[i].FDSN in cfg.REF_SITE)
                if val == True:
                    self.GitRes.staGit[i].ref = True
                    count = count + 1
                else:
                    self.GitRes.staGit[i].ref = False
            s1 = self.GitRes.staGit[i].toString0(None)
            # s1 = self.GitRes.staGit[i].toString0()
            logf.write(s1)
            logf.write("\n")
            if 0 != cfg.iref:
                pass
            else:
                cfg.REF_SITE.append(self.GitRes.staGit[i].Stat)
            print(s1)
        s1 = "..................................................................."
        print(s1)       
        if numref != 0:
            if count ==0 and cfg.iref != 0:
                s1 = "WARNING: no reference sites were found, check the configuration file"
                print(s1)
                logf.write(s1)
                logf.write("\n")
                sys.exit()
            else:
                s1 = " Number of reference stations: {:4.0f}".format(numref)
                print(s1)
        if (cfg.iref == 0):
            cfg.nsites = len(self.GitRes.staGit)
        logf.write(s1)
        logf.write("\n")
        self.GitRes.set_inc()
        del evt
        del fas
        del sta
