#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import sys
import os
import numpy as np
import pandas as pd
from plotnine import aes, ggplot, annotation_logticks, ggtitle
from plotnine import geom_line
from plotnine import theme_bw,xlab, ylab, scale_color_distiller, scale_x_continuous

class Attenuation:
    
    def __init__(self, *args):
        self.ndist = None
        self.progname = None
        self.dist = None
        self.freq = None
        self.dref = -1
        if len(args) == 1:
            self.fname = str(args[0])
            self.FromFile(args[0])
        elif len(args) == 7:
            self.FromData(args[0], args[1], args[2], args[3], args[4], args[5],args[6])

    def checkFreq(self,f,fvect,fstep):
        self.selectedf = f
        self.fvect = fvect
        self.fstep = fstep
        
        try:
            i_f_step = np.where(self.fvect == self.selectedf)[0][0]
            self.fstep= self.fstep[i_f_step]
            message = {"f_step": self.fstep, "error": None}

        except Exception as e:
            str_ = "Attenzione, digitare una frequenza tra: fr = " + str(self.fvect)
            message = {"f_step": None, "error": str_}     
        return message
    
    def WriteAttenuation(self, fname):
        try:
            att_file = open(fname, "w")
        except FileNotFoundError:
            print(f"File {fname} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {fname}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {fname} is", repr(err))
            sys.exit(1)
        s1 = "{:s} {:s} {:f} {:s}".format(self.progname,self.cmp,self.dref,self.job_name)
        att_file.write(s1)
        att_file.write("\n")
        s1 = " Dist_[km]"
        att_file.write(s1)
        for i in range(self.nfreq):
            freq = self.freq[i]
            s1 = "  {:7.2f} ".format(freq)
            att_file.write(s1)
        att_file.write("\n")
        for j in range(self.ndist):
            s1 = " {:7.2f} ".format(self.dist[j])
            att_file.write(s1)
            for i in range(self.nfreq):
                s1 = " {:8.4f} ".format(self.att[j][i])
                att_file.write(s1)
            att_file.write("\n")
        att_file.close()

    def FromData(self, dist, freq, at, dr, name, cm,jb):
        self.fname = ""
        self.dist = dist
        self.freq = freq
        self.nfreq = len(self.freq)
        self.ndist = len(self.dist)
        self.att = at
        self.dref = dr
        self.progname = name
        self.cmp = cm
        self.job_name = jb

    def FromFile(self, fn):
        self.fname = fn
        self.freq = list()
        self.dist = list()
        self.dref = math.nan
        str0 = list()
        try:
            f = open(fn, "r")
        except FileNotFoundError:
            print(f"File {fn} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {fn}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {fn} is", repr(err))
            sys.exit(1)
        line = f.readline()
        line = line.rstrip('\n')
        p1 = line.split()
        self.progname = p1[0]
        self.cmp = p1[1]
        self.dref = float(p1[2])
        self.job_name = p1[3]
        line = f.readline()
        ncount = 0
        self.ndist = 0
        while line:
            ncount = ncount + 1
            if ncount == 1:
                p1 = line.split()
                self.nfreq = len(p1) - 1
                cnt = 1
                while cnt <= self.nfreq:
                    self.freq.append(float(p1[cnt]))
                    cnt = cnt + 1
            else:
                p1 = line.split()
                self.dist.append(float(p1[0]))
                str0.append(p1)
            line = f.readline()
        f.close()
        initial_val = 0.0
        self.ndist = len(str0)
        self.att = [[initial_val] * self.nfreq for _ in range(self.ndist)]
        for j in range(self.ndist):
            p1 = str0[j]
            v = 0;
            for i in range(self.nfreq):
                self.att[j][i] = float(p1[i + 1])
                v = v + float(p1[i + 1])
            if abs(v) < 0.1:
                self.dref = self.dist[j]

    def getAnelasticAttenuation (self,AttDF,GRDF,flag,rref,Q0,Vs):
        
        if np.unique(GRDF["Type"])[0] == 'Rref/R':
            val_to_sub_1 = np.log(pow(10,GRDF['y']))
            dist = GRDF['x']
        else:
            val_to_sub_1 = np.log(pow(10,GRDF['y']))
            dist = AttDF.loc[AttDF['FR'] ==1]['x']
        val_to_sub_2 = 0
        
        if flag==1:
            val_to_sub_2 = np.log(pow(10,(-np.pi*(dist-rref))/(Q0*Vs*np.log(10))))
            val_to_sub_1 = val_to_sub_1 - val_to_sub_2

        AnelasticAttDF=None

        freq_vect = np.sort(np.unique(AttDF['FR'].to_numpy()))
        for fr in  freq_vect:
            val = AttDF.loc[AttDF['FR']==fr]
            AttDF_ = val.assign(AminusGR=np.log(pow(10,val['y']))-val_to_sub_1)
            if fr == freq_vect[0]:
                AnelasticAttDF = AttDF_
            else:
                AnelasticAttDF=pd.concat([AnelasticAttDF,AttDF_])
            #    AnelasticAttDF = AnelasticAttDF.append(AttDF_)
            del AttDF_
                
        return AnelasticAttDF     

    def AnelasticAttDFredD(self,AttDF,D1,D2):
        self.D1 = D1
        self.D2 = D2
        above_D1 = AttDF[AttDF["x"] >= self.D1]
        belove_D2 = above_D1[above_D1["x"] <= self.D2]
        df = belove_D2
        
        return df
    
    def AnelasticAttDFredF(self,AttDF,f_range_index):

        cond = AttDF["FR"] < f_range_index[-1]
        dtot2 = AttDF.loc[cond]   
        cond2 = dtot2["FR"] > f_range_index[0]
        Att_single_f = dtot2.loc[cond2]     
        
        return Att_single_f
    
    def AnelasticAttDFsingleF(self,AttDF,f):
        self.f = f
        df = AttDF.loc[AttDF['FR'] == self.f]
        
        return df
    
    def getAttenuationDF (self):
        
        dtot=None

        for i in range(self.nfreq):
            y1 = np.empty(self.ndist, dtype=float)
            for j in range(self.ndist):
                y1[j] = self.att[j][i]
            fr = round(self.freq[i], 2)
            f1 = []
            f1 = [fr for i in range(self.ndist)]
            frr = str(fr)
            s1 = []
            s1 = [frr for i in range(self.ndist)]
            d1 = {'x': self.dist, 'y': y1, 'TY': s1, 'FR': f1, 'TYPE': "FROM_FILE"}
            d1 = pd.DataFrame(d1)
            if d1['x'][0] ==0:
                d1 = d1.iloc[1:, :]
            if i == 0:
                dtot = d1
            else:
                dtot=pd.concat([dtot,d1])
      #          dtot = dtot.append(d1)
            del d1
                
        return dtot
    
    def plotAttenuation(self,name_out, v1, v2,verbose):
        
        dtot = self.getAttenuationDF()
        st = "Inv: {:s}, Cmp: {:s}, Nfreq: {:d} ".format(self.progname, self.cmp, self.nfreq)
        xmax = self.dist[self.ndist - 1]
        xmax = math.log10(xmax)

        br = []
        br = [5.0, 10, 20, 50, 100]
        c1 = []
        c1 = [0.6, xmax]
        vdref = math.isnan(self.dref)
        if vdref == False:
            dtot = dtot[(dtot["x"] >= self.dref)]
            unox = [[self.dref, 0, 'T'], [10 + self.dref, -1, 'T'], [100 + self.dref, -2, 'T']]
        else:
            unox = [[1, 0], [10, -1], [100, -2]]
        unox = pd.DataFrame(unox, columns=['X', 'Y', 'TY'])
        dtot = dtot.reset_index()
        if v1 == True or v2 == True:
            g = ggplot(dtot, aes(x='x', y='y', group='TY', color='FR')) + geom_line(size=0.9)
            #     g=g+geom_line(unox, aes(x='X', y='Y',group='TY'),color="red")
            g = g + scale_color_distiller(name='Freq  [Hz]', palette=9, type='div')
            #g= g + geom_line(GR1R, aes(x='x', y='y',linetype="Type"),size=1.0)
            #g= g + geom_line(GR1R2, aes(x='x', y='y',linetype="Type"),size=1.0)
            #  g=g+xlim(-100, 200)
            #g = g + coord_cartesian(xlim=c1)
            g = g + scale_x_continuous(trans='log10', breaks=br)
            #   g=g+scale_x_continuous(trans='log10')
            g = g + annotation_logticks(sides="b")
            g = g + xlab("Hypocentral Distance [km]") + ylab("Log(Attenuation)")
            g = g + ggtitle(st)
            g = g + theme_bw()
            if v1 == True:
                print(g)
            if v2 == True:
                if not os.path.exists(name_out):
                    os.makedirs(name_out)
                if name_out:
                    msg = f" saving graphic file: {name_out} \n"
                    if verbose: print(msg)
                    g.save(filename=name_out + '/Attenuation.jpg', dpi=300,verbose=False)
            
    def plotAllAttenuation(self,GR1R,GR1R2, name_out, v1, v2, verbose):

        dtot = self.getAttenuationDF()
        st = "Inv: {:s}, Cmp: {:s}, Nfreq: {:d} ".format(self.progname, self.cmp, self.nfreq)
        xmax = self.dist[self.ndist - 1]
        xmax = math.log10(xmax)

        br = []
        br = [5.0, 10, 20, 50, 100]
        c1 = []
        c1 = [5, xmax]
        vdref = math.isnan(self.dref)
        if vdref == False:
            dtot = dtot[(dtot["x"] >= self.dref)]
            unox = [[self.dref, 0, 'T'], [10 + self.dref, -1, 'T'], [100 + self.dref, -2, 'T']]
        else:
            unox = [[1, 0], [10, -1], [100, -2]]
        unox = pd.DataFrame(unox, columns=['X', 'Y', 'TY'])
        dtot = dtot.reset_index()
        if v1 != False or v2 != False:
            g = ggplot()
            g=  g +  geom_line(dtot, aes(x='x', y='y',group = 'TY', colour='FR'),size=1.0,linetype="solid")
            g=  g + scale_color_distiller(name='Freq  [Hz]',palette=9,type='div')
            g= g + geom_line(GR1R, aes(x='x', y='y',linetype="Type"),size=1.0)
            g= g + geom_line(GR1R2, aes(x='x', y='y',linetype="Type"),size=1.0)
            #g = g + xlim(10,200)
            g = g + scale_x_continuous(trans='log10', breaks=br)
            #g=g+scale_x_continuous(trans='log10')
            g = g + annotation_logticks(sides="b")
            g = g + xlab("Hypocentral Distance [km]") + ylab("Log Attenuation")
            g = g + ggtitle(st)
            g = g + theme_bw()
            if v1 != False:
                print("\n")
                print("*********************************")
                print('Please close the figure to go on')
                print("*********************************")
                print("\n") 
                print(g)
            if v2 != False:
                if not os.path.exists(name_out):
                    os.makedirs(name_out)
                if name_out:
                    msg = f" saving graphic file: {name_out} \n"
                    if verbose: print(msg)
                    g.save(filename=name_out + '/Attenuation.png', height=5, width=5,dpi=300,verbose=False)

    def plotAnelasticAttenuation(self,Att,name_out,df, v1, v2, opts):

        fmin=Att.freq[0]
        fmax=Att.freq[Att.nfreq-1]
        lm = []
        lm = [fmin,fmax]
            
        stit = "Git: {:s}, Job Name: {:s}, CMP: {:s}".format(Att.progname,Att.job_name,Att.cmp)
        ndist=len(Att.dist)
        xmax= Att.dist[ndist-1]
        xmax=math.log10(xmax)
        g = ggplot()
        g=  g +  geom_line(df, aes(x='x', y='AminusGR', group='TY',color='FR'),size=1.0,linetype="solid")
        g=  g + scale_color_distiller(name='Freq  [Hz]',palette=9,type='div',limits=lm)
        g=g + xlab("Hypocentral Distance [km]")  +  ylab("Log Attenuation")
        g = g + ggtitle(stit)
        g=g+theme_bw()
        


        if v1 != False and opts.configFile != None:
            print("\n")
            print("*********************************")
            print('Please close the figure to go on')
            print("*********************************")
            print("\n") 
            print(g)
        if v1 != False and opts.configFile == None:
            print(g)
              
        if v2 != False:
            if name_out:
                msg = f" saving graphic file: {name_out} \n"
                verbose = opts.verb
                if verbose: print(msg)
                g.save(filename=name_out, height=5, width=5, dpi=300,verbose=False)

    def plotSingleAttenuation(self,Att,f_single,dtot3,name_out,v1,v2,verbose):

        fmin=Att.freq[0]
        fmax=Att.freq[Att.nfreq-1]
        lm = []
        lm = [fmin,fmax]
        stit = "Git: {:s}, Job Name: {:s}, CMP: {:s} Freq: {:5.1f} ".format(Att.progname,Att.job_name,Att.cmp,f_single)
        ndist=len(Att.dist)
        xmax= Att.dist[ndist-1]
        xmax=math.log10(xmax)
        g = ggplot()
        g=  g +  geom_line(dtot3, aes(x='x', y='y', group='TY',color='FR'),size=1.0,linetype="dotted")
        g=  g + scale_color_distiller(name='Freq  [Hz]',palette=9,type='div',limits=lm)
        g=g + xlab("Hypocentral Distance [km]")  +  ylab("Log Attenuation")
        g = g + ggtitle(stit)
        g=g+theme_bw()
        if v1 != False:
            print("\n")
            print("*********************************")
            print('Please close the figure to go on')
            print("*********************************")
            print("\n")
            print(g)
        if v2 != False:
            if name_out:
                msg = f" saving graphic file: {name_out} \n"
                if verbose: print(msg)
                g.save(filename=name_out, dpi=300,verbose=False)

