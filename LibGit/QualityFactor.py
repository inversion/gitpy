#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
plt.matplotlib.use('TkAgg')
from plotnine import aes, ggplot,scale_x_log10, scale_y_log10,ggtitle,geom_line, geom_point, theme_bw,geom_errorbar 
from scipy.optimize import curve_fit
from LibGit.utilities import linfunc1

class QualityFactor:
    
    def __init__(self,D1,D2,Vs,dist_vect):
        self.D1 = D1
        self.D2 = D2
        self.Vs = Vs
        self.dist_vect = dist_vect
        self.kappa_flag = None
        self.freq_hinge = None
        self.kappa_val = None
    
    def checkQF(self):
        excs = []
        try:
            if isinstance(self.Vs, (int, float)) == False:
                raise TypeError("Only integers or float are allowed for Vs [Km/s]")
        except Exception as e:
            excs.append(e)
        try:
            if isinstance(self.D1, (int, float)) == False:
                raise TypeError("Only integers or float are allowed for D1 [km]")
        except Exception as e:
            excs.append(e)
        try:
            if self.D1 <0:
                raise TypeError("Only positive integers or float are allowed for D1 [km]")
        except Exception as e:
            excs.append(e)
        try:
            if self.D2 <0:
                raise TypeError("Only positive integers or float are allowed for D2 [km]")
        except Exception as e:
            excs.append(e)
        try:
            if isinstance(self.D2, (int, float)) == False:
                raise TypeError("Only integers or float are allowed for D2 [km]")
        except Exception as e:
            excs.append(e)
        try:
            if self.D2 <= self.D1:
                raise  Exception("ERROR!!!! D2 distance must be greater that D1")
        except Exception as e:
            excs.append(e)
        try:
            A = np.where(np.array(self.dist_vect) <= self.D1)
            if len(A) == 0:
                A[0]=1
                raise Exception("Using first bin for lowest distance for Q evaluation")
            B = np.where(np.array(self.dist_vect) <= self.D2)
            if len(B) == 0:
                #Q0 = 1 #da gestire nel costruttore
                #N = 1 #da gestire nel costruttore
                raise Exception("Could not evaluate Q due to bad distance range given by user")
        except Exception as e:
            excs.append(e)        
        return excs
             
    def todefineQ0(self,dd,rr,aa,Vs):
        X = dd - rr
        Y = aa
        def fit_fun(x,b):
            y = -np.pi*x/Vs*b*np.log(10)
            return y    
        popt, pcov = curve_fit(fit_fun, X, Y) 
        std_ = np.sqrt(np.diagonal(pcov))
        Q0 = popt[0]
        return Q0         

    def Case1(self,dd,rr,aa,f,index,Vs): 

        X = dd -rr
        Y = aa
        def fit_fun(x,b):
            y = b*x
            return y
    
        popt, pcov = curve_fit(fit_fun, X, Y) 
        std_BETA = np.sqrt(np.diagonal(pcov))
        Q = (-np.pi*f)/(popt[0]*Vs)
        Q_plus = np.pi*f/(Vs*abs(popt[0]+std_BETA*2))
        Q_minus = np.pi*f/(Vs*abs(popt[0]-std_BETA*2))
        std_Q = Q_plus - Q
        dict = {int(index):{"Frequency [Hz]":f, "bcoeff": float(popt[0]), "bcov": float(pcov[0]), "Q(f)": float(Q), "Q": Q,"yerr": float(std_Q[0]),"err_high": Q_plus[0], "err_low": Q_minus[0]}}
        return dict    
    
    def fitCase1(self,dfQF):
        
        X = np.log10(dfQF["Frequency [Hz]"].to_numpy()) 
        Y = np.log10(dfQF["Q(f)"].to_numpy())         
     
        popt, pcov = curve_fit(linfunc1, X, Y)
             
        Q0 = np.power(10,popt[0])
        N = popt[1]
        std = np.sqrt(np.diag(pcov))
        
        dict = {"Q0": float(Q0), "N": float(N), "err_Q0": std[0], "err_N": std[1]}
        
        return dict
    
    def plotQf(self,dfQF,dfQF1,Q0,N,ERQ0,ERN,Q01,N1,Q02,N2,Q03,N3,name_out,v1,v2,verbose):
        
        Y_fit = linfunc1(np.log10(dfQF["Frequency [Hz]"].to_numpy()),np.log10(Q0),N)    
        dfQF["Y_fit"] = np.power(10,Y_fit)
        #dfQF1["Y_fit"] = np.power(10,dfQF1['fit'])
        g = ggplot()
        stit = "Q0 = " + "{:3.2f}".format(Q0) + ' +/- ' +  "{:3.2f}".format(ERQ0) + '  N = ' + "{:3.2f}".format(N)+ ' +/- ' +  "{:3.2f}".format(ERN)
        g = ggplot(dfQF) + geom_point(aes("Frequency [Hz]", "Q(f)"), size=2) + geom_errorbar(aes(x="Frequency [Hz]", ymin="err_low",ymax="err_high"), width=.1)
        g= g + geom_line(dfQF,aes(x='Frequency [Hz]', y='Y_fit', color="Model"),size=1.0)
        if dfQF1 is not None:
            g= g + geom_line(dfQF1,aes(x='Frequency [Hz]', y='fit', color="Model"),size=1.5)

        g = g + scale_x_log10()
        g = g + scale_y_log10()
        g = g + ggtitle(stit)
        g=g+theme_bw() 
        if v1 != False:
            print("\n")
            print("*********************************")
            print('Please close the figure to go on')
            print("*********************************")
            print("\n") 
            print(g)
        if v2 != False:
            if name_out:
                msg = f" saving graphic file: {name_out} \n"
                if verbose: print(msg)
                g.save(filename=name_out, dpi=300,verbose=False)        
    