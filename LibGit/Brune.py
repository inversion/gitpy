#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

class Brune:
    
    def __init__(self, *args):
        self.f = args[0]
        self.smom = args[1]
        self.cfreq = args[2]
        self.hfreq = args[3]
        self.ksource = args[4]
        
    def fun_brune_model_disp(self):
        y = np.log10(self.smom) +  np.log10(np.power(self.cfreq,2)/(np.power(self.cfreq,2)+np.power(self.f,2))) + np.log10(np.exp(-np.pi*self.ksource*(self.f-self.hfreq)))
        return y
    
    def fun_brune_model_vel(self):
        y = np.log10(2*np.pi*np.array(self.f)) + np.log10(np.power(self.cfreq,2)/(np.power(self.cfreq,2)+np.power(self.f,2))) + np.log10(np.exp(-np.pi*self.ksource*(self.f-self.hfreq)))
        return y 
    
    def fun_brune_model_acc(self):
        y = 2*np.log10(2*np.pi*np.array(self.f)) + np.log10(np.power(self.cfreq,2)/(np.power(self.cfreq,2)+np.power(self.f,2))) + np.log10(np.exp(-np.pi*self.ksource*(self.f-self.hfreq)))
        return y
    
    def fun_brune_model_kappa(self):
        y =  -np.log10(np.exp(np.pi*self.ksource*(self.f-self.hfreq)))
        return y
    
    