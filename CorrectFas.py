import math
import os
import sys
import argparse
from datetime import datetime
import numpy as np
import shutil
import warnings
warnings.filterwarnings("ignore")
from datetime import datetime
from scipy import interpolate
from LibGit.ConfCorrectFas import ConfCorrectFas
from LibGit.utilities import valid_true_false
from LibGit.format_utils import dict_fmt
from obspy.geodetics.base import gps2dist_azimuth

class ArgumentParserError(Exception): pass
class CustomArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ArgumentParserError(message)
    
def toEvtString(Inp,Evt):
    id0 =Evt.Id
    if len(id0) != 11:
       pass
    else:
       id0 = "0" + id0
    s1 = "{:10s} {:s} {:12s} {:s} {:s} {:s} {:s}".format('CorrectFas', Inp.cmp, id0, Evt.Ml, Evt.EvLat, Evt.EvLon, Evt.EvtDpt)
    return s1

def WriteFullOutPut(Inp):
    for i in range(len(Inp.Map_Evt_Sta)):
        
        IntEvt=toEvtString(Inp,Inp.evt.iloc[i])
        id1 = Inp.evt.iloc[i].Id
        ml= Inp.evt.iloc[i].Ml
        lat=Inp.evt.iloc[i].EvLat
        lon=Inp.evt.iloc[i].EvLon
        dpt=Inp.evt.iloc[i].EvtDpt
        if len(id1) != 11:
           pass
        else:
           id1 = "0" + id1
        name = os.path.join(Inp.out_dir, id1)
        name = name + ".app_source"
        try:
            evt = open(name, "w")
        except FileNotFoundError:
            print(f"File {name} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {name}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {name} is", repr(err))
            sys.exit(1)
        evt.write(IntEvt)
        evt.write("\n")
        s1 = "    FDSN       Dist   Bz    Az  "
        for iy in range(len(Inp.freq)):
            v1="  {:5.2f}  ".format(Inp.freq[iy])
            s1 = s1+ v1
        evt.write(s1)
        evt.write("\n")
        Inp.Map_Evt_Sta[i].mat = [[np.nan] * len(Inp.Map_Evt_Sta[i].listaStaz) for _ in range(Inp.nf)]
        for j in range(len(Inp.Map_Evt_Sta[i].listaStaz)):
            sta=Inp.Map_Evt_Sta[i].listaStaz[j]
            s1 = "{:12s} {:6.2f} {:5.1f} {:5.1f} ".format(sta.FDSN,sta.Dipo,sta.Azim,sta.Baz)
            for k in range(Inp.nf):
                v1="{:7.2e} ".format(sta.val[k])
                s1=s1+v1
                Inp.Map_Evt_Sta[i].mat[k][j]=math.log10(sta.val[k])
            evt.write(s1)
            evt.write("\n")
        evt.close() 
        name = os.path.join(Inp.out_dir, id1)
        name = name + ".source"
        try:
            evt = open(name, "w")
        except FileNotFoundError:
            print(f"File {name} not found.  Aborting")
            sys.exit(1)
        except OSError:
            print(f"OS error occurred trying to open {name}")
            sys.exit(1)
        except Exception as err:
            print(f"Unexpected error opening {name} is", repr(err))
            sys.exit(1)
        s1 = "{:s} {:s} {:12s} {:s} {:s} {:s} {:s}".format("CorrectFas", Inp.cmp, id1, ml, lat,lon,dpt)
        evt.write(s1)
        evt.write("\n")
        for j in range(Inp.nf):
             median=np.nanmedian(Inp.Map_Evt_Sta[i].mat[j])
             std=np.nanstd(Inp.Map_Evt_Sta[i].mat[j])
             n1= np.count_nonzero(~np.isnan(Inp.Map_Evt_Sta[i].mat[j]))
             s1 = "  {:7.2f} {:e}  {:e} {:d}".format(Inp.freq[j], median,std,n1)
             evt.write(s1)
             evt.write("\n")
        evt.close()
        
def CorrectFas(logf, Inp, nf):
    map_Id_stat = []
    s1 = "....... Fas Correction Freq: {:7.2f} Hz  Nf: {:d} ".format(Inp.fas[nf].freq, nf + 1)
    logf.write(s1)
    logf.write("\n")
    print(s1)
    Tab = Inp.Tables[nf]
    nfreq=len(Inp.Tables)
    f0=Tab.iloc[1].Freq
    nd=Inp.Att.ndist
    dmin=Inp.Att.dist[0]
    dmax=Inp.Att.dist[nd-1]
    for i in range(len(Tab)):
        staz=Tab.iloc[i].FDSN
        dist=Tab.iloc[i].Dipo
        value=np.nan
        d=(dist >= dmin and dist <= dmax)
        val=False
        for j in range(len(Inp.Sites.staGit)):
            if staz == Inp.Sites.staGit[j].FDSN:
               index_sta=j
               val=True        
        if (val == True) and (d == True):
            IdS=str(Tab.iloc[i].Id)
            index_evt=  Inp.evt[Inp.evt['Id'] == IdS]
           
            if Inp.cmp == 'Z':
                val = Tab.iloc[i].FAS_Z * Inp.scale
            elif Inp.cmp == 'NS':
                val = Tab.iloc[i].FAS_NS * Inp.scale
            elif Inp.cmp == 'EW':
                val = Tab.iloc[i].FAS_EW * Inp.scale
            else:
                if Inp.mean == 'VECTORIAL':
                   EW = (Tab.iloc[i].FAS_EW * Inp.scale) * (Tab.iloc[i].FAS_EW * Inp.scale)
                   NS = (Tab.iloc[i].FAS_NS * Inp.scale) * (Tab.iloc[i].FAS_NS * Inp.scale)
                   val = math.sqrt((EW + NS))
                else:
                   val = math.sqrt((Tab.iloc[i].FAS_EW * Inp.scale) * (Tab.iloc[i].FAS_NS * Inp.scale))
            valOld=val
            val=math.log10(val)
            AttCorr=Inp.mesh_att(dist,f0)
            index_sta=index_sta-1
            SiteCorr=GetSiteCorr(Inp,index_sta,f0)
            v1=val-AttCorr-SiteCorr
           
            d1,az,baz= gps2dist_azimuth(Tab.iloc[i].StLat,Tab.iloc[i].StLon, Tab.iloc[i].EvLat,Tab.iloc[i].EvLon)
            valCor=pow(10,v1)
            MapSta = Inp.Map_Evt_Sta[index_evt.index[0]]
            MapSta.addSta(staz,nfreq,dist,az,baz,valCor,nf)
   
def usageGit(msg):
    print(msg)
    sys.exit()
    
def GetSiteCorr(Inp,index_sta,f0):

    f = interpolate.interp1d(Inp.Sites.staGit[index_sta].freq,Inp.Sites.staGit[index_sta].val)
    nf=len(Inp.Sites.staGit[index_sta].freq)
    fmin=Inp.Sites.staGit[index_sta].freq[0]
    fmax=Inp.Sites.staGit[index_sta].freq[nf-1]
    if f0 >= fmin and f0 <= fmax:
        val = f(f0)
        if val ==0: return np.NaN
        return math.log10(val)
    return np.NaN

def correctfas(opts_str):

    synopsis = 'Provides the apparent source spectra correcting the recorded spectra (FAS) for the attenuation and site effects, using the GIT spectral decomposition outputs'
    usage = "example: %prog --config /path/to/configuration_file.yaml"
    p = argparse.ArgumentParser(description=synopsis + '\n' + usage)
    p.add_argument("--config", "--configuration_file", action="store", dest="configFile",required=True, help="configuration file in YAML format to execute the CorrectFas.py module")
    p.add_argument("--log", "--log_file", action="store", dest="LogFile", required=True,help="log file in TXT format")
    p.add_argument("--v", "--verbose", action="store", dest="verb", type=valid_true_false, default='True', help="Verbose mode (prints summary and other details). Default: True")
    opts_str_lst = opts_str.replace("'", "").replace('=',' ').split(' ')
    try: opts = p.parse_args(opts_str_lst) 
    except ArgumentParserError as ap_err: return dict_fmt(exit_status=2, exit_message=str(ap_err))       
    
    now = datetime.now()
    dt_string_start = now.strftime("%d/%m/%Y %H:%M:%S")
    s1 = " ---------------------- CorrectFas, Starting computation: {:s} ---------".format(dt_string_start)
    print(s1)
    
    configFile = opts.configFile
    logFile = opts.LogFile
    print("-----  CorrectFas: earthquake,path and sites   -----")
    print("         Configuration file: ", configFile)
    print("                   Log file: ", logFile)
             
    log_file = open(logFile, "w")  
    val = os.path.isfile(logFile)
    if (val):         
        write = os.access(logFile, os.W_OK)
        path_log = os.path.abspath(logFile)
        if write == False:
            msg = f" Cannot write Log File {logFile} \n Aborting"
            usageGit(msg)
        else:
            msg = f" Warning Log File {logFile} exist !"
            print(msg)
    else:
        pa = os.path.dirname(logFile)
        write = os.access(pa, os.W_OK)
        if write == False:
            msg = f" Cannot write File {logFile} \n Aborting"
            usageGit(msg)

    val = os.path.isfile(configFile)
    path_conf = os.path.abspath(configFile)
    if val == False:
        msg = f" Config File {configFile} not found\n Aborting"
        usageGit(msg)
    
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    s1 = " ---------------------- CorrectFas, Starting computation: {:s} ---------".format(dt_string)
    log_file.write(s1)
    log_file.write("\n")
    print(s1)
    s1 = "> Configuration File: {:s} ".format(configFile)
    log_file.write(s1)
    log_file.write("\n")
    s1 = "> Log File: {:s} ".format(logFile)
    log_file.write(s1)
    log_file.write("\n")
    cfg = ConfCorrectFas(configFile, log_file)
    for i in range(cfg.nf):
        CorrectFas(log_file, cfg, i)

    WriteFullOutPut(cfg)
    log_file.close()
    print(os.path.commonprefix(cfg.out_dir))

    shutil.move(path_log, cfg.out_dir + os.path.basename(path_log))
    shutil.copyfile(path_conf, cfg.out_dir + os.path.basename(path_conf))
        
    dt_string_end = now.strftime("%d/%m/%Y %H:%M:%S")
    s1 = " ---------------------- CorrectFas, End computation: {:s} ---------".format(dt_string_end)
    print('')
    print(s1)
    return dict_fmt(exit_status=0, exit_message='',out_string='')            

def main():
    
    ret_code = correctfas(' '.join(sys.argv[1:]))
    
    if ret_code['exit_status'] == 0: print('DONE!!') 
    if ret_code['exit_message']: print(ret_code['exit_message'])
    sys.exit(ret_code['exit_status'])


if __name__ == "__main__": main()