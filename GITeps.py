#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import argparse
from datetime import datetime
import warnings
warnings.filterwarnings("ignore")
from LibGit.ConfGITeps import Conf
from LibGit import GitResults
from LibGit import InputFas
from LibGit.FASInv import FASInv
from LibGit.format_utils import dict_fmt
from LibGit.utilities import valid_true_false,check_log

class ArgumentParserError(Exception): pass
class CustomArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ArgumentParserError(message)

def GITeps(opts_str):

    synopsis = 'GITeps implements the Generalized Inversion Technique (GIT) to isolate the source, propagation and site contributions to ground shaking in the spectral domain.'
    usage = "example: python GITeps.py --config /path/to/configfile.txt --log /path/to/logfile.txt"
    p = argparse.ArgumentParser(description=synopsis + '\n' + usage)
    p.add_argument("--config", "--configuration_file", action="store", dest="configFile",  required=True, help="configuration file in YAML format to set the Generalized Inversion of seismological records")
    p.add_argument("--log", "--log_file", action="store", dest="LogFile",  required=True, help="log file in TXT format")
    p.add_argument("--v", "--verbose", action="store", dest="verb", type=valid_true_false, default='True', help="Verbose mode (prints summary and other details). Default: True") 
    opts_str_lst = opts_str.replace("'", "").replace('=',' ').split(' ')
    try: opts = p.parse_args(opts_str_lst) 
    except ArgumentParserError as ap_err: return dict_fmt(exit_status=2, exit_message=str(ap_err)) 
    
    progname = "GITeps.py"
    progname = progname.replace(".py", "")
    configFile = opts.configFile
    logFile = opts.LogFile
    print("-----  GITeps: earthquake,path and sites   -----")
    print("         Configuration file: ", configFile)
    print("                   Log file: ", logFile)
    log_file = open(logFile, "w")
    confF,confL = check_log(logFile,configFile)
    now = datetime.now()
    dt_string_start = now.strftime("%d/%m/%Y %H:%M:%S")
    s1 = " ---------------------- GITeps, Starting computation: {:s} ---------".format(dt_string_start)
    log_file.write(s1)
    log_file.write("\n")
    print(s1)
    s1 = "> Configuration File: {:s} ".format(configFile)
    log_file.write(s1)
    log_file.write("\n")
    s1 = "> Log File: {:s} ".format(logFile)
    log_file.write(s1)
    log_file.write("\n")
    cfg = Conf(configFile, log_file)
    GitRes = GitResults(progname)    
    inp = InputFas(cfg, log_file, GitRes, progname)
    inve = FASInv(log_file, cfg, GitRes, inp)
    for i in range(inp.nf):        
        inve.DoInversion(i)
    GitRes.WriteResults(cfg, log_file)
    dt_string_end = now.strftime("%d/%m/%Y %H:%M:%S")
    s1 = " ---------------------- GITeps, End computation: {:s} ---------".format(dt_string_end)
    log_file.write(s1)
    log_file.write("\n")
    print(s1)
    log_file.close()    
    GitRes.copy_log_conf(confF,cfg.out_dir + os.path.basename(configFile),confL,cfg.out_dir + os.path.basename(logFile))
    return dict_fmt(exit_status=0, exit_message='')

def main():
    
    ret_code = GITeps(' '.join(sys.argv[1:]))
    
    if ret_code['exit_status'] == 0 and ret_code['out_string'] : print('DONE!!')
    if ret_code['exit_message']: print(ret_code['exit_message'])
    sys.exit(ret_code['exit_status'])


if __name__ == "__main__": main()
