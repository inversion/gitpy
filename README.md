
<img src="images/Logo_GITpy.png"  width="1000">

GITpy is an open-source object-oriented Python software that implements the Generalized Inversion Technique (GIT) to isolate the source, propagation and site contributions to ground shaking in the spectral domain (Fourier Amplitude Spectra, FAS). GITpy includes modules to perform both the non-parametric spectral decomposition and to fit the resulting source spectra and spectral attenuation to standard seismological models. 

## Engine versions
Version 1.0.0 (05/08/2024) - Beta version, the first release of code and documentation.

## Documentation
- <h3 style="font-size:16px;">[Overview](docs/Overwiev.md)
- <h3 style="font-size:20px;">[Installation](docs/Installation.md)
- <h3 style="font-size:20px;">[Getting Started](docs/Getting-Started.md)
- <h3 style="font-size:20px;">[Tutorial](docs/Tutorial.md)

## License  
GITpy is licensed under the GNU Lesser General Public License (LGPL) v3.0.

## Citation
Permission to use, copy or reproduce parts of the GITpy code and/or related outputs is granted, provided that GITpy is properly referenced as:

*D’Amico M., Morasca P., Spallarossa D. (2024). GITpy, a python framework for the Generalized Inversion of Fourier Amplitude Spectra and the physical parametrization of Attenuation and Source Spectra, Istituto Nazionale di Geofisica e Vulcanologia and Genova University Software Release.* 

## Getting Help  
If you need help, please browse and search this documentation. To stay updated on the latest code developments and updates, please refer to the GitLab project https://gitlab.rm.ingv.it/inversion/gitpy and the related README. Additionally, you can submit questions, bug reports, and feature requests on the issue tracker on GitLab (https://gitlab.rm.ingv.it/inversion/gitpy/-/issues). All feedback and suggestions at maria.damico@ingv.it or paola.morasca@ingv.it are welcome.

## Disclaimer
This software is preliminary or provisional and is subject to revision.  
GITpy is provided on an As Is, with all faults, and As Available basis, without any warranties of any kind. We specifically do not warrant that GITpy is complete, accurate, reliable, or secure in any way, suitable for, or compatible with any of your contemplated activities, devices, operating systems, browsers, software and tools, or that their operation will be free of any viruses, bugs or program limitations.  
The use of GITpy for the inversion of seismic records and the parametrization of related spectra are devoted to qualified users. No warranty, implicit or explicit, is attached to GITpy data and metadata. Every risk due to the improper use of data or related information is assumed by the User.

## Acknowledgements
GITpy has been developed by the INGV and the Genova University. The development of the python modules devoted to the inversion of Fourier Amplitude Spectra (_GITeps.py_), the physical parameterization of source spectra (_FitSource.py_) and path attenuation (_FitAttenuation.py_), and the computation of apparent source spectra (_CorrectFas.py_) benefited from the scientific support of Dino Bindi and Matteo Picozzi. Thanks for testing to: Lorenzo Vitrano and Laura Cataldi. Thanks to Francesca Pacor for suggesting the idea of developing GITpy.

## References

Aki K. and Chouet B., (1975). *Origin of Coda Waves: Source, Attenuation and Scattering Effects*. Journal of Geophysical Research, 80, 3322-3342. http://dx.doi.org/10.1029/jb080i023p03322

Andrews D. J., (1986). *Objective determination of source parameters and similarity of earthquakes of different size, in Earthquake Source Mechanics*, S. Das, J. Boatwright, and C. H. Scholz (Editors), Geophysical Monograph Series, 259–267, doi: 10.1029/GM037p0259.

Bindi D. and Kotha S., (2020). *Spectral decomposition of the Engineering Strong Motion (ESM) flat file: regional attenuation, source scaling and Arias stress drop*. Bull Earthquake Eng 18(6):2581–2606

Bindi D., Spallarossa D., Picozzi M., and Morasca P., (2020). *Reliability of Source Parameters for Small Events in Central Italy: Insights from Spectral Decomposition Analysis Applied to Both Synthetic and Real Data*. Bulletin of the Seismological Society of America 2020; 110 (6): 3139–3157. doi: [https://doi.org/10.1785/0120200126](https://doi.org/10.1785/0120200126)

Bindi D., Spallarossa D., Picozzi M., Oth A., Morasca P. and Mayeda K., (2023a). *The Community Stress‐Drop Validation Study—Part I: Source, Propagation, and Site Decomposition of Fourier Spectra*. Seismological Research Letters 2023; 94 (4): 1980–1991. doi: [https://doi.org/10.1785/0220230019](https://doi.org/10.1785/0220230019)

Bindi D., Spallarossa D., Picozzi M., Oth A., Morasca P. and Mayeda K., (2023b). *The Community Stress‐Drop Validation Study—Part II: Uncertainties of the Source Parameters and Stress Drop Analysis*. Seismological Research Letters 2023; 94 (4): 1992–2002. doi: [https://doi.org/10.1785/0220230020](https://doi.org/10.1785/0220230020)

Boatwright J., Fletcher J.B. and Fumal T.E., (1991). *A general inversion scheme for source, site, and propagation characteristics using multiple recorded sets of moderate-sized earthquakes*. Bull. Seismol. Soc. Am. 81, no. 5, 1754–1782.

Castro R. R., Anderson J. G.  and Singh S. K., (1990). *Site response, attenuation and source spectra of S waves along the Guerrero,Mexico, subduction zone*. Bull. Seismol. Soc. Am. 80, no. 6A, 1481–1503.

Drouet S., Chevrot S., Cotton F. and Souriau A., (2008). *Simultaneous inversion of source spectra, attenuation parameters, and site responses: Application to the data of the French accelerometric network*. Bull. Seismol. Soc. Am. 98, 198–219.

Ide S. and Beroza G. C., (2001). *Does apparent stress vary with earthquake size?* Geophysical Research Letters, 28, no. 17, 3349 \- 3352\. https://doi.org/10.1029/2001GL013106

Oth A., Bindi D., Parolai S. and Di Giacomo D., (2011). *Spectral analysis of K-NET and KiK-net data in Japan, Part II: On attenuation characteristics, source spectra, and site response of borehole and surface stations*. Bull. Seismol. Soc. Am. 101, no. 2, 667–687.

Wyss M., (1970). *Apparent Stresses of Earthquakes on Ridges compared to Apparent Stresses of Earthquakes in Trenches*. Geophysical Journal International, Volume 19, Issue 5, June 1970, Pages 479–484, [https://doi.org/10.1111/j.1365-246X.1970.tb00153.x](https://doi.org/10.1111/j.1365-246X.1970.tb00153.x).